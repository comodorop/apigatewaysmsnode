//obtiene el nuevo nombre del archivo si existe el anterior
function getNewFileName(file) {
    var f1 = getFileName(file) + "-" + getIntRandom(10, 100000) + "." + getExtension(file);
    return getFileName(file) + "-" + getIntRandom(10, 100000) + getBetweenSeparators(f1) + "." + getExtension(file);
}
//crea un nombre para la imagen a subir
function getFirstFileName(file) {
    return getFileName(file) + "-" + getIntRandom(10, 1000) + "." + getExtension(file);
}
//obtenemos la extensión de la imagen
function getExtension(file) {
    return file.split('.').pop();
}
//obtenemos el nombre de la imagen
function getFileName(file) {
    return file.substr(0, file.lastIndexOf('.')) || file;
}
//obtenemos un número entero aleatorio para el nombre de la imagen
function getIntRandom(min, max) {
    return Math.floor((Math.random() * ((max + 1) - min)) + min);
}
//separamos entre el - y el .
function getBetweenSeparators(str) {
    return str.substring(str.lastIndexOf("-") + 1, str.lastIndexOf("."));
}
//comprobamos si está permitida la extensión del archivo
function checkExtension(file) {
    //extensiones permitidas
    var allowedExtensions = ["jpg", "jpeg", "gif", "png", "rar", "pdf"];
    //extension del archivo
    var extension = file.split('.').pop();
    //hacemos la comprobación
    return in_array(extension, allowedExtensions) === true ? true : false;
}
//funcion para comprobar valores en un array
function in_array(needle, haystack) {
    var key = '';
    for (key in haystack) {
        if (haystack[key] == needle) {
            return true;
        }
    }
    return false;
}

module.exports = {
    getNewFileName,
    getFirstFileName,
    getExtension,
    getFileName,
    getIntRandom,
    getBetweenSeparators,
    checkExtension,
    in_array
}