'use strict'

function createMessage(status, message, data = []) {
  var msg = {
    status: status,
    message: message,
    data: data
  }
  return msg
}

function successMessage(resp, msg) {
  resp.status(200).send({ data: msg })
}

function errorMessage(resp, msg) {
  return resp.status(401).send({ data: {msg: msg} })
}



module.exports = {
  createMessage,
  successMessage,
  errorMessage
}