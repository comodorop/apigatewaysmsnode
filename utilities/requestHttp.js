var requestify = require('requestify');



function requestMethod(method, body, url, service_id, authorization, companyId) {
    var promise = new Promise((res, rej) => {
        requestify.request(`${url}`, {
            method: method,
            body: body,
            headers: {
                "Service-ID": service_id,
                "Authorization": authorization,
                "X-COMPANY-ID": companyId
            }
        }).then(function (response) {
            res(response)
        }).fail(function (response) {
            console.log(response)
            rej(response)
        });
    })
    return promise;
}
function requestGet(url, service_id, authorization, companyId) {
    var promise = new Promise((res, rej) => {
        requestify.request(`${url}`, {
            method: 'GET',
            headers: {
                "Service-Id": service_id,
                "Authorization": authorization,
                "X-COMPANY-ID": companyId
            }
        }).then(function (response) {
            res(response.body)
        }).fail(function (response) {
            console.log(response)
            rej(response)
        });
    })
    return promise;
}

function requestGet2(url, service_id, authorization, companyId) {
    console.log(url)
    var promise = new Promise((res, rej) => {
        requestify.request(`${url}`, {
            method: 'GET',
            headers: {
                "Service-Id": service_id,
                "Authorization": authorization,
                "X-COMPANY-ID": companyId
            }
        }).then(function (response) {
            res(response)
        }).fail(function (response) {
            console.log(response)
            rej(response)
        });
    })
    return promise;
}

function requestPatch(url, body, service_id, authorization, companyId) {
    console.log(url)
    if (!body) {
        body = {}
    }
    console.log(body)
    var promise = new Promise((res, rej) => {
        requestify.request(`${url}`, {
            method: 'PATCH',
            body: body,
            headers: {
                "Service-Id": service_id,
                "Authorization": authorization,
                "X-COMPANY-ID": companyId
            }
        }).then(function (response) {
            console.log(response.body)
            res(response)
        }).fail(function (response) {
            console.log(response)
            rej(response)
        });
    })
    return promise;
}

function requestDelete(url, service_id, authorization, companyId) {
    console.log(url)
    var promise = new Promise((res, rej) => {
        requestify.request(`${url}`, {
            method: 'DELETE',
            headers: {
                "Service-Id": service_id,
                "Authorization": authorization,
                "X-COMPANY-ID": companyId
            }
        }).then(function (response) {
            console.log(response.body)
            res(response.body)
        }).fail(function (response) {
            console.log(response)
            rej(response)
        });
    })
    return promise;
}

module.exports = {
    requestMethod,
    requestGet,
    requestDelete,
    requestGet2,
    requestPatch
}