const obj = require('config-yml');
var HTTPError = require('node-http-error');

function validate(req, resp, next){
    if (req.headers.serviceid != obj.config.MSSENDER.serviceid) {
        return resp.send(new HTTPError(412, 'You dont have permits'))
    }
    else if (req.headers.authorization != obj.config.MSSENDER.authorization) {
        return resp.send(new HTTPError(412, 'You dont have authorization'))
    } else {
        next()
    }
}

module.exports={
    validate
}