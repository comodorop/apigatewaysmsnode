function verifyAdmin(req, resp, next) {
    if (req.token !== 'undefined' && (req.token.role === 'admin' || req.token.role === 'superadmin')) {
        next()
    } else {
        console.log(`${req.token.role} no tiene autorización`)
        return resp.status(403).send({data :{ status : 403, msg: 'No tiene autorización para este recurso.'}})
    }
}

function verifySuperAdmin(req, resp, next) {
    console.log(req.token)
    if (req.token !== 'undefined' && req.token.role === 'superadmin') {
        next()
    } else {
        console.log(`${req.token.role} no tiene autorización`)
        return resp.status(403).send({data :{ status : 403, msg: 'No tiene autorización para este recurso.'}})
    }
}

function verifyNormal(req, resp, next) {
    if (req.token !== 'undefined' && (req.token.role === 'normal' || req.token.role === 'admin' || req.token.role === 'superadmin')) {
        next()
    } else {
        console.log(`${req.token.role} no tiene autorización`)
        return resp.status(403).send({data :{ status : 403, msg: 'No tiene autorización para este recurso.'}})
    }
}


module.exports = {
    verifyAdmin,
    verifySuperAdmin,
    verifyNormal
}