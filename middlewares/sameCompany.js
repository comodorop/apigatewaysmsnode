function verifySameRoleCompany (nameId) {
    return function (req, resp, next) {
        console.log(req.params[nameId])
        if (req.token.role !== 'superadmin' && (req.params[nameId] === 'undefined' || req.token.company_id !== req.params[nameId])) {
            return resp.status(403).send({data :{ status : 403, msg: 'No puedes cambiar o ver datos de otra empresa. No tiene autorización para este recurso.'}})
        }
        next()
    }
}

module.exports = {
    verifySameRoleCompany
}