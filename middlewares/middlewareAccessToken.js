var HTTPError = require('node-http-error')
var verifyToken = require('../service/token/token')
var message = require('../utilities/messages')

//verifica que el token aun este vigente  y si tiene en la cabecera la propiedad access_token
function verifyAccessToken(req, res, next) {
    
    if(req.url == '/auth/token' || req.url == '/auth/refresh' || req.url =='/superadmins/auth/token' || req.url =='/superadmins/auth/refresh') next()
    else {
        var token  = req.headers['authorization']
        verifyToken.validateToken(token.replace("Bearer ","").trim())
            .then(tokenParams => {
                req.token = tokenParams
                next()
            })
            .catch(err => {
                message.errorMessage(res, 'The Toke has been expired')
            })
    }
}
module.exports = {
    verifyAccessToken
}