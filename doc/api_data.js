define({ "api": [
  {
    "type": "delete",
    "url": "http://206.81.204:3000/v1/categories/:id",
    "title": "Eliminar una Categoria",
    "name": "DeleteCategory",
    "group": "Category",
    "description": "<p>Nos permite elimiinar una categoria.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "content-type",
            "description": "<p>Del tipo application/json que se envia por la cabecera.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "authorization",
            "description": "<p>Token que se envia por la cabecera y es: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJ2YWx1ZSJ9.FG-8UppwHaFp1LgRYQQeS6EDQF7_6-bMFegNucHjmWg</p>"
          },
          {
            "group": "Parameter",
            "type": "uuid",
            "optional": false,
            "field": "category_id",
            "description": "<p>id de categoria que se envia por el url</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "regresa un status 204",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-sin_cabecera:",
          "content": "En el caso de no tener serviceid en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property serviceid\"\n}\nEn el caso de no tener content-type en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property content-type\"\n}\nEn el caso de no tener authorization en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property of authorization\"\n}",
          "type": "json"
        },
        {
          "title": "Error-serviceid:",
          "content": "En el caso de tener un serviceid incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have permits\"\n}",
          "type": "json"
        },
        {
          "title": "Error-content-type:",
          "content": "En el caso de tener un content-type diferente a application/json:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"you dont have the format correct\"\n}",
          "type": "json"
        },
        {
          "title": "Error-authorization:",
          "content": "En el caso de tener authorization incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have authorization\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/categories/Category.js",
    "groupTitle": "Category"
  },
  {
    "type": "get",
    "url": "http://206.81.14.204:3000/v1/categories/:id",
    "title": "mostrar categorias",
    "name": "GetCategory",
    "group": "Category",
    "description": "<p>Nos permite mostrar informacion de las categorias. No requiere parámetros.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "content-type",
            "description": "<p>Del tipo application/json que se envia por la cabecera.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "authorization",
            "description": "<p>Token que se envia por la cabecera y es: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJ2YWx1ZSJ9.FG-8UppwHaFp1LgRYQQeS6EDQF7_6-bMFegNucHjmWg</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n    \"response_id\": \"80c41c46-67df-413b-97a2-fa9e8041f742\",\n    \"path\": \"/v1/categories/ba3f8c72-8f1b-49db-95e5-5a6b4f880455\",\n    \"method\": \"get\",\n    \"request\": 1533317841,\n    \"msg\": \"category\",\n    \"api-version\": \"v1\",\n    \"status\": \"ok\",\n    \"service\": \"MS-SMS\",\n    \"data\": {\n        \"company_id\": \"6cf86cef-a3ad-4250-93f6-1b1b68a5d10a\",\n        \"name\": \"test1\",\n        \"id\": \"ba3f8c72-8f1b-49db-95e5-5a6b4f880455\",\n        \"description\": \"esto es un catalogo\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-sin_cabecera:",
          "content": "En el caso de no tener serviceid en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property serviceid\"\n}\nEn el caso de no tener content-type en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property content-type\"\n}\nEn el caso de no tener authorization en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property of authorization\"\n}",
          "type": "json"
        },
        {
          "title": "Error-serviceid:",
          "content": "En el caso de tener un serviceid incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have permits\"\n}",
          "type": "json"
        },
        {
          "title": "Error-content-type:",
          "content": "En el caso de tener un content-type diferente a application/json:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"you dont have the format correct\"\n}",
          "type": "json"
        },
        {
          "title": "Error-authorization:",
          "content": "En el caso de tener authorization incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have authorization\"\n}",
          "type": "json"
        },
        {
          "title": "Error-token:",
          "content": "El access_token expira en cierto tiempo y si eso pasa sale este mensaje:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 401,\n\t\"status\": 401,\n\t\"message\": \"el Token ya expiró\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/categories/Category.js",
    "groupTitle": "Category"
  },
  {
    "type": "post",
    "url": "http://206.81.14.204:3000/v1/companies/:id/categories",
    "title": "Agregar una categoria",
    "name": "PostCategory",
    "group": "Category",
    "description": "<p>Esta api nos permite insertar una categoria por compania.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "content-type",
            "description": "<p>Del tipo application/json que se envia por la cabecera.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "authorization",
            "description": "<p>Token que se envia por la cabecera y es: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJ2YWx1ZSJ9.FG-8UppwHaFp1LgRYQQeS6EDQF7_6-bMFegNucHjmWg</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "Nombre",
            "description": "<p>Nombre de la categoria y se envia por el body.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "Description",
            "description": "<p>Descripción de la categoria y se envia por el body.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "\n\t{\n    \"response_id\": \"5d4da980-798c-47b4-9c0c-16522b20d04b\",\n    \"path\": \"/v1/companies/6cf86cef-a3ad-4250-93f6-1b1b68a5d10a/categories\",\n    \"method\": \"post\",\n    \"request\": 1533317173,\n    \"msg\": \"create new category\",\n    \"api-version\": \"v1\",\n    \"status\": \"ok\",\n    \"service\": \"MS-SMS\",\n    \"data\": {\n        \"company_id\": \"6cf86cef-a3ad-4250-93f6-1b1b68a5d10a\",\n        \"id\": \"486fb107-63d8-4125-b2ef-2191098dac28\",\n        \"description\": \"\",\n        \"name\": \"otras\"\n    }\n}\n\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-sin_cabecera:",
          "content": "En el caso de no tener serviceid en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property serviceid\"\n}\nEn el caso de no tener content-type en la cabecera:PostCategory\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property content-type\"\n}\nEn el caso de no tener authorization en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property of authorization\"\n}",
          "type": "json"
        },
        {
          "title": "Error-serviceid:",
          "content": "En el caso de tener un serviceid incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have permits\"\n}",
          "type": "json"
        },
        {
          "title": "Error-content-type:",
          "content": "En el caso de tener un content-type diferente a application/json:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"you dont have the format correct\"\n}",
          "type": "json"
        },
        {
          "title": "Error-authorization:",
          "content": "En el caso de tener authorization incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have authorization\"\n}",
          "type": "json"
        },
        {
          "title": "Error-token:",
          "content": "El access_token expira en cierto tiempo y si eso pasa sale este mensaje:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 401,\n\t\"status\": 401,\n\t\"message\": \"el Token ya expiró\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/companies/company.js",
    "groupTitle": "Category"
  },
  {
    "type": "put",
    "url": "http://206.81.14.204:3000/v1/categories/:id",
    "title": "Actualizar Categoria",
    "name": "PutCategory",
    "group": "Category",
    "description": "<p>Esta api nos permite actualizar información de categoria.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "content-type",
            "description": "<p>Del tipo application/json que se envia por la cabecera.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "authorization",
            "description": "<p>Token que se envia por la cabecera y es: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJ2YWx1ZSJ9.FG-8UppwHaFp1LgRYQQeS6EDQF7_6-bMFegNucHjmWg</p>"
          },
          {
            "group": "Parameter",
            "type": "uuid",
            "optional": false,
            "field": "uuid",
            "description": "<p>uuid de la categoria y se envia por el body.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "nombre",
            "description": "<p>nombre de la categoria y se envia por el body.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "description",
            "description": "<p>Descripcion de la categoria y se envia por el body.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response: ",
          "content": " {\n    \"response_id\": \"fc0ac9b8-a86c-47db-9d06-73af3746c016\",\n    \"path\": \"/v1/categories/486fb107-63d8-4125-b2ef-2191098dac28\",\n    \"method\": \"put\",\n    \"request\": 1533317667,\n    \"msg\": \"update category\",\n    \"api-version\": \"v1\",\n    \"status\": \"ok\",\n    \"service\": \"MS-SMS\",\n    \"data\": {\n        \"company_id\": \"6cf86cef-a3ad-4250-93f6-1b1b68a5d10a\",\n        \"name\": \"otras\",\n        \"id\": \"486fb107-63d8-4125-b2ef-2191098dac28\",\n        \"description\": \"\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-sin_cabecera:",
          "content": "En el caso de no tener serviceid en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property serviceid\"\n}\nEn el caso de no tener content-type en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property content-type\"\n}\nEn el caso de no tener authorization en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property of authorization\"\n}",
          "type": "json"
        },
        {
          "title": "Error-serviceid:",
          "content": "En el caso de tener un serviceid incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have permits\"\n}",
          "type": "json"
        },
        {
          "title": "Error-content-type:",
          "content": "En el caso de tener un content-type diferente a application/json:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"you dont have the format correct\"\n}",
          "type": "json"
        },
        {
          "title": "Error-authorization:",
          "content": "En el caso de tener authorization incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have authorization\"\n}",
          "type": "json"
        },
        {
          "title": "Error-token:",
          "content": "El access_token expira en cierto tiempo y si eso pasa sale este mensaje:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 401,\n\t\"status\": 401,\n\t\"message\": \"el Token ya expiró\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/categories/Category.js",
    "groupTitle": "Category"
  },
  {
    "type": "delete",
    "url": "http://206.81.14.204:3000/companys/company/:id",
    "title": "Borrar compañia",
    "name": "DeleteCompany",
    "group": "Company",
    "description": "<p>Esta api es para borrar una compañia con el siguiente parámetro.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "content-type",
            "description": "<p>Del tipo application/json que se envia por la cabecera.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "authorization",
            "description": "<p>Que inicia con Bearer seguido de un token que se envia por la cabecera.</p>"
          },
          {
            "group": "Parameter",
            "type": "uuid",
            "optional": false,
            "field": "id",
            "description": "<p>Identificador de la compañia y se envia por la url.</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-sin_cabecera:",
          "content": "En el caso de no tener serviceid en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property serviceid\"\n}\nEn el caso de no tener content-type en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property content-type\"\n}\nEn el caso de no tener authorization en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property of authorization\"\n}",
          "type": "json"
        },
        {
          "title": "Error-serviceid:",
          "content": "En el caso de tener un serviceid incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have permits\"\n}",
          "type": "json"
        },
        {
          "title": "Error-content-type:",
          "content": "En el caso de tener un content-type diferente a application/json:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"you dont have the format correct\"\n}",
          "type": "json"
        },
        {
          "title": "Error-authorization:",
          "content": "En el caso de tener authorization incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have authorization\"\n}",
          "type": "json"
        },
        {
          "title": "Error-token:",
          "content": "El access_token expira en cierto tiempo y si eso pasa sale este mensaje:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 401,\n\t\"status\": 401,\n\t\"message\": \"el Token ya expiró\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/companies/company.js",
    "groupTitle": "Company"
  },
  {
    "type": "get",
    "url": "http://206.81.14.204:3000/companys/company/:id",
    "title": "Mostrar informacion de la compañia",
    "name": "GetCompany",
    "group": "Company",
    "description": "<p>Esta api muestra informacion de la compañia solicitando el siguiente parametro.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "content-type",
            "description": "<p>Del tipo application/json que se envia por la cabecera.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "authorization",
            "description": "<p>Que inicia con Bearer seguido de un token que se envia por la cabecera.</p>"
          },
          {
            "group": "Parameter",
            "type": "uuid",
            "optional": false,
            "field": "id",
            "description": "<p>Identificador de la compañia y se envia por la url.</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-sin_cabecera:",
          "content": "En el caso de no tener serviceid en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property serviceid\"\n}\nEn el caso de no tener content-type en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property content-type\"\n}\nEn el caso de no tener authorization en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property of authorization\"\n}",
          "type": "json"
        },
        {
          "title": "Error-serviceid:",
          "content": "En el caso de tener un serviceid incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have permits\"\n}",
          "type": "json"
        },
        {
          "title": "Error-content-type:",
          "content": "En el caso de tener un content-type diferente a application/json:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"you dont have the format correct\"\n}",
          "type": "json"
        },
        {
          "title": "Error-authorization:",
          "content": "En el caso de tener authorization incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have authorization\"\n}",
          "type": "json"
        },
        {
          "title": "Error-id:",
          "content": "Si el id de la compañia no es correcta:\n{\n\t\"code\": 500,\n\t\"headers\": {\n\t\t\"connection\": \"close\",\n\t\t\"content-type\": \"text/html\",\n\t\t\"content-length\": \"141\"\n\t},\n\t\"body\": \"<html>\\n  <head>\\n    <title>Internal Server Error</title>\\n  </head>\\n  <body>\\n    <h1><p>Internal Server Error</p></h1>\\n    \\n  </body>\\n</html>\\n\"\n}",
          "type": "json"
        },
        {
          "title": "Error-token:",
          "content": "El access_token expira en cierto tiempo y si eso pasa sale este mensaje:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 401,\n\t\"status\": 401,\n\t\"message\": \"el Token ya expiró\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/companies/company.js",
    "groupTitle": "Company"
  },
  {
    "type": "post",
    "url": "http://206.81.14.204:3000/companys/company",
    "title": "Crear una compañia.",
    "name": "PostCompany",
    "group": "Company",
    "description": "<p>Estos son los campos que se necesitan son para crear una compañia exitosamente</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "content-type",
            "description": "<p>Del tipo application/json que se envia por la cabecera.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "authorization",
            "description": "<p>Que inicia con Bearer seguido de un token que se envia por la cabecera.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "name",
            "description": "<p>Nombre de la compañia y se envia por el body.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "rfc",
            "description": "<p>rfc de la compañia y se envia por el body.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "email_contact",
            "description": "<p>Correo de contacto de la compañia y se envia por el body.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "phone",
            "description": "<p>El numero teléfonico de la compañia y se envia por el body.</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "credit",
            "description": "<p>Credito de la compañia y se envia por el body.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n\t\"response_id\": \"d268ddfa-b54b-4a9b-b86c-2b0048fec0f5\",\n\t\"path\": \"/v1/companies\",\n\t\"method\": \"post\",\n\t\"request\": 1529057590.0,\n\t\"msg\": \"created\",\n\t\"status\": 201,\n\t\"service\": \"ms-users\",\n\t\"data\": {\n\t\t\"id\": \"5dadb518-c2e8-4caf-8c79-afbc2a057176\",\n\t\t\"name\": \"Byt3Code_2\",\n\t\t\"rfc\": \"asdasdas\",\n\t\t\"email_contact\": \"isc_86@hotmail.com\",\n\t\t\"credits\": 1000,\n\t\t\"phone\": \"9999999\"\n\t}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-sin_cabecera:",
          "content": "En el caso de no tener serviceid en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property serviceid\"\n}\nEn el caso de no tener content-type en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property content-type\"\n}\nEn el caso de no tener authorization en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property of authorization\"\n}",
          "type": "json"
        },
        {
          "title": "Error-serviceid:",
          "content": "En el caso de tener un serviceid incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have permits\"\n}",
          "type": "json"
        },
        {
          "title": "Error-content-type:",
          "content": "En el caso de tener un content-type diferente a application/json:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"you dont have the format correct\"\n}",
          "type": "json"
        },
        {
          "title": "Error-authorization:",
          "content": "En el caso de tener authorization incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have authorization\"\n}",
          "type": "json"
        },
        {
          "title": "Error-token:",
          "content": "El access_token expira en cierto tiempo y si eso pasa sale este mensaje:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 401,\n\t\"status\": 401,\n\t\"message\": \"el Token ya expiró\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/companies/company.js",
    "groupTitle": "Company"
  },
  {
    "type": "put",
    "url": "http://206.81.14.204:3000/companys/company",
    "title": "Actualizar compañia",
    "name": "PutCompany",
    "group": "Company",
    "description": "<p>Esta api requiere de los siguientes parámetros:</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "content-type",
            "description": "<p>Del tipo application/json que se envia por la cabecera.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "authorization",
            "description": "<p>Que inicia con Bearer seguido de un token que se envia por la cabecera.</p>"
          },
          {
            "group": "Parameter",
            "type": "uuid",
            "optional": false,
            "field": "company_id",
            "description": "<p>Identificador de la compañia y se envia por el body.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "name",
            "description": "<p>Nombre de la compañia y se envia por el body.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "rfc",
            "description": "<p>rfc de la compañia y se envia por el body.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "email_contact",
            "description": "<p>Correo de contacto de la compañia y se envia por el body.</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "credits",
            "description": "<p>Credito de la compañia y se envia por el body.</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-sin_cabecera:",
          "content": "En el caso de no tener serviceid en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property serviceid\"\n}\nEn el caso de no tener content-type en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property content-type\"\n}\nEn el caso de no tener authorization en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property of authorization\"\n}",
          "type": "json"
        },
        {
          "title": "Error-serviceid:",
          "content": "En el caso de tener un serviceid incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have permits\"\n}",
          "type": "json"
        },
        {
          "title": "Error-content-type:",
          "content": "En el caso de tener un content-type diferente a application/json:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"you dont have the format correct\"\n}",
          "type": "json"
        },
        {
          "title": "Error-authorization:",
          "content": "En el caso de tener authorization incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have authorization\"\n}",
          "type": "json"
        },
        {
          "title": "Error-token:",
          "content": "El access_token expira en cierto tiempo y si eso pasa sale este mensaje:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 401,\n\t\"status\": 401,\n\t\"message\": \"el Token ya expiró\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/companies/company.js",
    "groupTitle": "Company"
  },
  {
    "type": "delete",
    "url": "http://206.81.204:3000/v1/categories/contacts/idcontact",
    "title": "Eliminar un contacto",
    "name": "DeleteContact",
    "group": "Contact",
    "description": "<p>Nos permite Actualizar informacion de contacto.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "content-type",
            "description": "<p>Del tipo application/json que se envia por la cabecera.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "authorization",
            "description": "<p>Token que se envia por la cabecera y es: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJ2YWx1ZSJ9.FG-8UppwHaFp1LgRYQQeS6EDQF7_6-bMFegNucHjmWg</p>"
          },
          {
            "group": "Parameter",
            "type": "uuid",
            "optional": false,
            "field": "idcontact",
            "description": "<p>id de contacto que se envia por el url</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "No body returned response",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-sin_cabecera:",
          "content": "En el caso de no tener serviceid en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property serviceid\"\n}\nEn el caso de no tener content-type en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property content-type\"\n}\nEn el caso de no tener authorization en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property of authorization\"\n}",
          "type": "json"
        },
        {
          "title": "Error-serviceid:",
          "content": "En el caso de tener un serviceid incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have permits\"\n}",
          "type": "json"
        },
        {
          "title": "Error-content-type:",
          "content": "En el caso de tener un content-type diferente a application/json:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"you dont have the format correct\"\n}",
          "type": "json"
        },
        {
          "title": "Error-authorization:",
          "content": "En el caso de tener authorization incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have authorization\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/categories/Category.js",
    "groupTitle": "Contact"
  },
  {
    "type": "get",
    "url": "http://206.81.14.204:3000/v1/categories/contacts/idcontact",
    "title": "mostrar un contacto",
    "name": "GetContact",
    "group": "Contact",
    "description": "<p>Nos permite mostrar informacion del contacto de una compania.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "content-type",
            "description": "<p>Del tipo application/json que se envia por la cabecera.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "authorization",
            "description": "<p>Token que se envia por la cabecera y es: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJ2YWx1ZSJ9.FG-8UppwHaFp1LgRYQQeS6EDQF7_6-bMFegNucHjmWg</p>"
          },
          {
            "group": "Parameter",
            "type": "uuid",
            "optional": false,
            "field": "idcontact",
            "description": "<p>ID de la compañia que se envia por la url.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n\t\"status\": \"200\",\n\t\"msg\": \"Registers\",\n\t\"data\": [\n\t\t{\n\t\t\t\"uuid\": \"1d0bfb49-ecb2-43c2-85be-943920aa9532\",\n\t\t\t\"first_name\": \"jordan\",\n\t\t\t\"last_name\": \"gongora\",\n\t\t\t\"phone\": \"123456789\",\n\t\t\t\"email\": \"prueba@gmail.com\",\n\t\t\t\"observations\": \"\",\n\t\t\t\"uuidCompany\": \"68a59fbc-ede5-4167-bde5-c3b7b985968e\"\n        }\n      ]\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-sin_cabecera:",
          "content": "En el caso de no tener serviceid en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property serviceid\"\n}\nEn el caso de no tener content-type en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property content-type\"\n}\nEn el caso de no tener authorization en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property of authorization\"\n}",
          "type": "json"
        },
        {
          "title": "Error-serviceid:",
          "content": "En el caso de tener un serviceid incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have permits\"\n}",
          "type": "json"
        },
        {
          "title": "Error-content-type:",
          "content": "En el caso de tener un content-type diferente a application/json:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"you dont have the format correct\"\n}",
          "type": "json"
        },
        {
          "title": "Error-authorization:",
          "content": "En el caso de tener authorization incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have authorization\"\n}",
          "type": "json"
        },
        {
          "title": "Error-token:",
          "content": "El access_token expira en cierto tiempo y si eso pasa sale este mensaje:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 401,\n\t\"status\": 401,\n\t\"message\": \"el Token ya expiró\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/categories/Category.js",
    "groupTitle": "Contact"
  },
  {
    "type": "get",
    "url": "http://206.81.14.204:3000/v1/categories/companies/idcompany/contacts",
    "title": "Mostrar Contactos de una compania",
    "name": "GetContact",
    "group": "Contact",
    "description": "<p>Nos permite mostrar informacion de los contactos de una compañia.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "content-type",
            "description": "<p>Del tipo application/json que se envia por la cabecera.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "authorization",
            "description": "<p>Token que se envia por la cabecera y es: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJ2YWx1ZSJ9.FG-8UppwHaFp1LgRYQQeS6EDQF7_6-bMFegNucHjmWg</p>"
          },
          {
            "group": "Parameter",
            "type": "uuid",
            "optional": false,
            "field": "idcompany",
            "description": "<p>ID de la compañia que se envia por la url.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n    \"response_id\": \"d89a8624-e074-4440-8012-490d068f3bdb\",\n    \"path\": \"/v1/companies/6cf86cef-a3ad-4250-93f6-1b1b68a5d10a/contacts\",\n    \"method\": \"get\",\n    \"request\": 1533316269,\n    \"msg\": \"contact list\",\n    \"api-version\": \"v1\",\n    \"status\": \"ok\",\n    \"service\": \"MS-SMS\",\n    \"data\": [\n        {\n            \"observations\": \"Nothing to say\",\n            \"company_id\": \"6cf86cef-a3ad-4250-93f6-1b1b68a5d10a\",\n            \"email\": \"isc_86@gmail.com\",\n            \"last_name\": \"Sabido\",\n            \"phone\": \"9992193269\",\n            \"id\": \"9ad20734-dfe8-4521-821b-02848b2c4496\",\n            \"first_name\": \"Ivan Israel\"\n        },\n        {\n            \"observations\": \"Nothing to say\",\n            \"company_id\": \"6cf86cef-a3ad-4250-93f6-1b1b68a5d10a\",\n            \"email\": \"isc_@gmail.com\",\n            \"last_name\": \"Sabido\",\n            \"phone\": \"9992199888\",\n            \"id\": \"1f6bc260-f810-4b5b-9332-6afc16394804\",\n            \"first_name\": \"Israel\"\n        },\n        {\n            \"observations\": \"Nothing to say\",\n            \"company_id\": \"6cf86cef-a3ad-4250-93f6-1b1b68a5d10a\",\n            \"email\": \"elian_2014@gmail.com\",\n            \"last_name\": \"Sabido\",\n            \"phone\": \"9992199888\",\n            \"id\": \"d0a50d3c-aed3-4c46-8397-9584537487b5\",\n            \"first_name\": \"Elian\"\n        },\n        {\n            \"observations\": \"Nothing to say\",\n            \"company_id\": \"6cf86cef-a3ad-4250-93f6-1b1b68a5d10a\",\n            \"email\": \"sarah_2012@gmail.com\",\n            \"last_name\": \"Sabido\",\n            \"phone\": \"9992199288\",\n            \"id\": \"154d6eb2-c88b-4fe7-b2ba-edea515fb142\",\n            \"first_name\": \"Sarah\"\n        }\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-sin_cabecera:",
          "content": "En el caso de no tener serviceid en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property serviceid\"\n}\nEn el caso de no tener content-type en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property content-type\"\n}\nEn el caso de no tener authorization en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property of authorization\"\n}",
          "type": "json"
        },
        {
          "title": "Error-serviceid:",
          "content": "En el caso de tener un serviceid incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have permits\"\n}",
          "type": "json"
        },
        {
          "title": "Error-content-type:",
          "content": "En el caso de tener un content-type diferente a application/json:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"you dont have the format correct\"\n}",
          "type": "json"
        },
        {
          "title": "Error-authorization:",
          "content": "En el caso de tener authorization incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have authorization\"\n}",
          "type": "json"
        },
        {
          "title": "Error-token:",
          "content": "El access_token expira en cierto tiempo y si eso pasa sale este mensaje:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 401,\n\t\"status\": 401,\n\t\"message\": \"el Token ya expiró\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/categories/Category.js",
    "groupTitle": "Contact"
  },
  {
    "type": "post",
    "url": "http://206.81.14.204:3000/categories/contact/:id",
    "title": "agregar contacto",
    "name": "PostContact",
    "group": "Contact",
    "description": "<p>Nos permite agregar un contacto.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "content-type",
            "description": "<p>Del tipo application/json que se envia por la cabecera.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "authorization",
            "description": "<p>Token que se envia por la cabecera y es: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJ2YWx1ZSJ9.FG-8UppwHaFp1LgRYQQeS6EDQF7_6-bMFegNucHjmWg</p>"
          },
          {
            "group": "Parameter",
            "type": "uuid",
            "optional": false,
            "field": "uuidCompany",
            "description": "<p>ID de la compañia y se envia por el body.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "first_name",
            "description": "<p>Nombre(s) y se envia por el body.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "last_name",
            "description": "<p>Apellido y se envia por el body.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "phone",
            "description": "<p>Telefono de contacto y se envia por el body.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "email",
            "description": "<p>Email de contacto y se envia por el body.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "observations",
            "description": "<p>observaciones del contacto y se envia por el body.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n    \"response_id\": \"fc0ac9b8-a86c-47db-9d06-73af3746c016\",\n    \"path\": \"/v1/categories/486fb107-63d8-4125-b2ef-2191098dac28\",\n    \"method\": \"put\",\n    \"request\": 1533317667,\n    \"msg\": \"update category\",\n    \"api-version\": \"v1\",\n    \"status\": \"ok\",\n    \"service\": \"MS-SMS\",\n    \"data\": {\n        \"company_id\": \"6cf86cef-a3ad-4250-93f6-1b1b68a5d10a\",\n        \"name\": \"otras\",\n        \"id\": \"486fb107-63d8-4125-b2ef-2191098dac28\",\n        \"description\": \"\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-sin_cabecera:",
          "content": "En el caso de no tener serviceid en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property serviceid\"\n}\nEn el caso de no tener content-type en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property content-type\"\n}\nEn el caso de no tener authorization en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property of authorization\"\n}",
          "type": "json"
        },
        {
          "title": "Error-serviceid:",
          "content": "En el caso de tener un serviceid incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have permits\"\n}",
          "type": "json"
        },
        {
          "title": "Error-content-type:",
          "content": "En el caso de tener un content-type diferente a application/json:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"you dont have the format correct\"\n}",
          "type": "json"
        },
        {
          "title": "Error-authorization:",
          "content": "En el caso de tener authorization incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have authorization\"\n}",
          "type": "json"
        },
        {
          "title": "Error-token:",
          "content": "El access_token expira en cierto tiempo y si eso pasa sale este mensaje:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 401,\n\t\"status\": 401,\n\t\"message\": \"el Token ya expiró\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/categories/Category.js",
    "groupTitle": "Contact"
  },
  {
    "type": "put",
    "url": "http://206.81.14.204:3000/v1/categories/contacts/idcontact",
    "title": "Actualizar contacto",
    "name": "PutContact",
    "group": "Contact",
    "description": "<p>Nos permite Actualizar informacion de contacto.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "content-type",
            "description": "<p>Del tipo application/json que se envia por la cabecera.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "authorization",
            "description": "<p>Token que se envia por la cabecera y es: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJ2YWx1ZSJ9.FG-8UppwHaFp1LgRYQQeS6EDQF7_6-bMFegNucHjmWg</p>"
          },
          {
            "group": "Parameter",
            "type": "uuid",
            "optional": false,
            "field": "idcontact",
            "description": "<p>id del contacto y se envia por la url.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "first_name",
            "description": "<p>Nombre(s) y se envia por el body.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "last_name",
            "description": "<p>Apellido y se envia por el body.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "phone",
            "description": "<p>Telefono de contacto y se envia por el body.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "email",
            "description": "<p>Email de contacto y se envia por el body.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "observations",
            "description": "<p>observaciones del contacto y se envia por el body.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "No body returned response",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-sin_cabecera:",
          "content": "En el caso de no tener serviceid en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property serviceid\"\n}\nEn el caso de no tener content-type en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property content-type\"\n}\nEn el caso de no tener authorization en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property of authorization\"\n}",
          "type": "json"
        },
        {
          "title": "Error-serviceid:",
          "content": "En el caso de tener un serviceid incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have permits\"\n}",
          "type": "json"
        },
        {
          "title": "Error-content-type:",
          "content": "En el caso de tener un content-type diferente a application/json:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"you dont have the format correct\"\n}",
          "type": "json"
        },
        {
          "title": "Error-authorization:",
          "content": "En el caso de tener authorization incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have authorization\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/categories/Category.js",
    "groupTitle": "Contact"
  },
  {
    "type": "post",
    "url": "http://206.81.14.204:8003/v1/categories/idcategory/contacts",
    "title": "Agregar un grupo",
    "name": "DeleteGroup",
    "group": "Group",
    "description": "<p>Nos permite eliminar un grupo.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "content-type",
            "description": "<p>Del tipo application/json que se envia por la cabecera.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "authorization",
            "description": "<p>Token que se envia por la cabecera y es: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJ2YWx1ZSJ9.FG-8UppwHaFp1LgRYQQeS6EDQF7_6-bMFegNucHjmWg</p>"
          },
          {
            "group": "Parameter",
            "type": "uuid",
            "optional": false,
            "field": "idcategory",
            "description": "<p>ID de la categoria y se envia por la url.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "No body returned for response",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-sin_cabecera:",
          "content": "En el caso de no tener serviceid en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property serviceid\"\n}\nEn el caso de no tener content-type en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property content-type\"\n}\nEn el caso de no tener authorization en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property of authorization\"\n}",
          "type": "json"
        },
        {
          "title": "Error-serviceid:",
          "content": "En el caso de tener un serviceid incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have permits\"\n}",
          "type": "json"
        },
        {
          "title": "Error-content-type:",
          "content": "En el caso de tener un content-type diferente a application/json:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"you dont have the format correct\"\n}",
          "type": "json"
        },
        {
          "title": "Error-authorization:",
          "content": "En el caso de tener authorization incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have authorization\"\n}",
          "type": "json"
        },
        {
          "title": "Error-token:",
          "content": "El access_token expira en cierto tiempo y si eso pasa sale este mensaje:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 401,\n\t\"status\": 401,\n\t\"message\": \"el Token ya expiró\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/categories/Category.js",
    "groupTitle": "Group"
  },
  {
    "type": "get",
    "url": "http://206.81.14.204:3000/categories/idcategory/contacts",
    "title": "Mostrar lista de grupos",
    "name": "GetGroup",
    "group": "Group",
    "description": "<p>Nos permite mostrar informacion del listgroup.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "content-type",
            "description": "<p>Del tipo application/json que se envia por la cabecera.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "authorization",
            "description": "<p>Token que se envia por la cabecera y es: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJ2YWx1ZSJ9.FG-8UppwHaFp1LgRYQQeS6EDQF7_6-bMFegNucHjmWg</p>"
          },
          {
            "group": "Parameter",
            "type": "uuid",
            "optional": false,
            "field": "idcategory",
            "description": "<p>ID de la compañia que se envia por la url.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n\t\"response_id\": \"9d6e45bd-8881-4cc2-8525-60fb7a27e149\",\n\t\"path\": \"/v1/categories/46919a29-a93e-429f-af56-412e1e4d37fb/contacts\",\n\t\"method\": \"get\",\n\t\"request\": 1533359498.0,\n\t\"msg\": \"list contacts in category\",\n\t\"api-version\": \"v1\",\n\t\"status\": \"ok\",\n\t\"service\": \"MS-SMS\",\n\t\"data\": [\n\t\t{\n\t\t\t\"observations\": \"Nothing to say\",\n\t\t\t\"first_name\": \"Israel\",\n\t\t\t\"last_name\": \"Sabido\",\n\t\t\t\"id\": \"1f6bc260-f810-4b5b-9332-6afc16394804\",\n\t\t\t\"email\": \"isc_@gmail.com\",\n\t\t\t\"company_id\": \"6cf86cef-a3ad-4250-93f6-1b1b68a5d10a\",\n\t\t\t\"phone\": \"9992199888\"\n\t\t},\n\t\t{\n\t\t\t\"observations\": \"Nothing to say\",\n\t\t\t\"first_name\": \"Elian\",\n\t\t\t\"last_name\": \"Sabido\",\n\t\t\t\"id\": \"d0a50d3c-aed3-4c46-8397-9584537487b5\",\n\t\t\t\"email\": \"elian_2014@gmail.com\",\n\t\t\t\"company_id\": \"6cf86cef-a3ad-4250-93f6-1b1b68a5d10a\",\n\t\t\t\"phone\": \"9992199888\"\n\t\t},\n\t\t{\n\t\t\t\"observations\": \"Nothing to say\",\n\t\t\t\"first_name\": \"Sarah\",\n\t\t\t\"last_name\": \"Sabido\",\n\t\t\t\"id\": \"154d6eb2-c88b-4fe7-b2ba-edea515fb142\",\n\t\t\t\"email\": \"sarah_2012@gmail.com\",\n\t\t\t\"company_id\": \"6cf86cef-a3ad-4250-93f6-1b1b68a5d10a\",\n\t\t\t\"phone\": \"9992199288\"\n\t\t},\n\t\t{\n\t\t\t\"observations\": \"Nothing to say\",\n\t\t\t\"first_name\": \"Ivan Israel\",\n\t\t\t\"last_name\": \"Sabido\",\n\t\t\t\"id\": \"9ad20734-dfe8-4521-821b-02848b2c4496\",\n\t\t\t\"email\": \"isc_86@gmail.com\",\n\t\t\t\"company_id\": \"6cf86cef-a3ad-4250-93f6-1b1b68a5d10a\",\n\t\t\t\"phone\": \"9992193269\"\n\t\t}\n\t]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-sin_cabecera:",
          "content": "En el caso de no tener serviceid en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property serviceid\"\n}\nEn el caso de no tener content-type en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property content-type\"\n}\nEn el caso de no tener authorization en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property of authorization\"\n}",
          "type": "json"
        },
        {
          "title": "Error-serviceid:",
          "content": "En el caso de tener un serviceid incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have permits\"\n}",
          "type": "json"
        },
        {
          "title": "Error-content-type:",
          "content": "En el caso de tener un content-type diferente a application/json:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"you dont have the format correct\"\n}",
          "type": "json"
        },
        {
          "title": "Error-authorization:",
          "content": "En el caso de tener authorization incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have authorization\"\n}",
          "type": "json"
        },
        {
          "title": "Error-token:",
          "content": "El access_token expira en cierto tiempo y si eso pasa sale este mensaje:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 401,\n\t\"status\": 401,\n\t\"message\": \"el Token ya expiró\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/categories/Category.js",
    "groupTitle": "Group"
  },
  {
    "type": "post",
    "url": "http://206.81.14.204:8003/v1/categories/idcategory/contacts",
    "title": "Agregar un grupo",
    "name": "PostGroup",
    "group": "Group",
    "description": "<p>Nos permite agregar un grupo.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "content-type",
            "description": "<p>Del tipo application/json que se envia por la cabecera.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "authorization",
            "description": "<p>Token que se envia por la cabecera y es: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJ2YWx1ZSJ9.FG-8UppwHaFp1LgRYQQeS6EDQF7_6-bMFegNucHjmWg</p>"
          },
          {
            "group": "Parameter",
            "type": "uuid",
            "optional": false,
            "field": "contacts",
            "description": "<p>ID de contactos que se asignan a una categoria y se envia por el body.</p>"
          },
          {
            "group": "Parameter",
            "type": "uuid",
            "optional": false,
            "field": "idcategory",
            "description": "<p>ID de la categoria y se envia por la url.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n    \"response_id\": \"0dcc7ecd-d0f2-4f31-a860-9fd27d83e4c5\",\n    \"path\": \"/v1/categories/46919a29-a93e-429f-af56-412e1e4d37fb/contacts\",\n    \"method\": \"post\",\n    \"request\": 1533316512,\n    \"msg\": \"create new category\",\n    \"api-version\": \"v1\",\n    \"status\": \"ok\",\n    \"service\": \"MS-SMS\",\n    \"data\": {\n        \"contacts\": [\n            \"154d6eb2-c88b-4fe7-b2ba-edea515fb142\",\n            \"1f6bc260-f810-4b5b-9332-6afc16394804\",\n            \"d0a50d3c-aed3-4c46-8397-9584537487b5\",\n            \"9ad20734-dfe8-4521-821b-02848b2c4496\",\n            \"9ad20734-dfe8-4521-821b-02848b2c4496\"\n        ]\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-sin_cabecera:",
          "content": "En el caso de no tener serviceid en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property serviceid\"\n}\nEn el caso de no tener content-type en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property content-type\"\n}\nEn el caso de no tener authorization en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property of authorization\"\n}",
          "type": "json"
        },
        {
          "title": "Error-serviceid:",
          "content": "En el caso de tener un serviceid incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have permits\"\n}",
          "type": "json"
        },
        {
          "title": "Error-content-type:",
          "content": "En el caso de tener un content-type diferente a application/json:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"you dont have the format correct\"\n}",
          "type": "json"
        },
        {
          "title": "Error-authorization:",
          "content": "En el caso de tener authorization incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have authorization\"\n}",
          "type": "json"
        },
        {
          "title": "Error-token:",
          "content": "El access_token expira en cierto tiempo y si eso pasa sale este mensaje:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 401,\n\t\"status\": 401,\n\t\"message\": \"el Token ya expiró\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/categories/Category.js",
    "groupTitle": "Group"
  },
  {
    "type": "put",
    "url": "http://206.81.14.204:8003/categories/listgroup",
    "title": "Actualizar grupo",
    "name": "PutListgroup",
    "group": "Listgroup",
    "description": "<p>Nos permite actualizar un grupo.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "content-type",
            "description": "<p>Del tipo application/json que se envia por la cabecera.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "authorization",
            "description": "<p>Token que se envia por la cabecera y es: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJ2YWx1ZSJ9.FG-8UppwHaFp1LgRYQQeS6EDQF7_6-bMFegNucHjmWg</p>"
          },
          {
            "group": "Parameter",
            "type": "uuid",
            "optional": false,
            "field": "uuid",
            "description": "<p>ID del grupo y se envia por el body.</p>"
          },
          {
            "group": "Parameter",
            "type": "uuid",
            "optional": false,
            "field": "uuid_contact",
            "description": "<p>ID del contacto y se envia por el body.</p>"
          },
          {
            "group": "Parameter",
            "type": "uuid",
            "optional": false,
            "field": "uuid_category",
            "description": "<p>ID de la categoria y se envia por el body.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n\t\"status\": \"200\",\n\t\"msg\": \"Register affect\",\n\t\"data\": []\n   }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-sin_cabecera:",
          "content": "En el caso de no tener serviceid en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property serviceid\"\n}\nEn el caso de no tener content-type en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property content-type\"\n}\nEn el caso de no tener authorization en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property of authorization\"\n}",
          "type": "json"
        },
        {
          "title": "Error-serviceid:",
          "content": "En el caso de tener un serviceid incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have permits\"\n}",
          "type": "json"
        },
        {
          "title": "Error-content-type:",
          "content": "En el caso de tener un content-type diferente a application/json:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"you dont have the format correct\"\n}",
          "type": "json"
        },
        {
          "title": "Error-authorization:",
          "content": "En el caso de tener authorization incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have authorization\"\n}",
          "type": "json"
        },
        {
          "title": "Error-token:",
          "content": "El access_token expira en cierto tiempo y si eso pasa sale este mensaje:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 401,\n\t\"status\": 401,\n\t\"message\": \"el Token ya expiró\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/categories/Category.js",
    "groupTitle": "Listgroup"
  },
  {
    "type": "post",
    "url": "http://206.81.14.204:3000/auth/refresh",
    "title": "refrescar el token",
    "name": "PostRefresh",
    "group": "Login",
    "description": "<p>Nos permite refrescar el token de inicio de sesion.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "content-type",
            "description": "<p>del tipo application/json que se envia por la cabecera</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "authorization",
            "description": "<p>token que se envia por la cabecera y es: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJ2YWx1ZSJ9.FG-8UppwHaFp1LgRYQQeS6EDQF7_6-bMFegNucHjmWg</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "grant_type",
            "description": "<p>tipo de acceso: 'refresh_token'.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "refresh_token",
            "description": "<p>token.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n\t\"response_id\": \"6e8e6f37-5a31-4532-831f-43172e8466a7\",\n\t\"path\": \"/v1/oauth/token/refresh\",\n\t\"method\": \"post\",\n\t\"request\": 1529721319.0,\n\t\"msg\": \"token created\",\n\t\"status\": \"ok\",\n\t\"service\": \"ms-auth\",\n\t\"data\": {\n\t\t\"access_token\": \"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzUxMiJ9.eyJjb21wYW55X2lkIjoiNmNmODZjZWYtYTNhZC00MjUwLTkzZjYtMWIxYjY4YTVkMTBhIiwibWVtYmVyc2hpcF9pZCI6IjJmNWQzMmExLWY0NGUtNDhiMC1hYTU0LWZhZDcyMmFmZWM4MSIsInVzZXJfaWQiOiI2Y2MyZGQwNi04ZDZmLTRmYjktOTJhNy0wMGU1YjI4ZjZkZGYiLCJyb2xlIjoibm9ybWFsIiwiY29tcGFueV9uYW1lIjoiQnl0ZUNvZGUiLCJleHAiOjE1Mjk3MjIyMTksImlhdCI6MTUyOTcyMTMxOSwiYXVkIjoic21hcnRzZW5kIiwiaXNzIjoiIn0.e7dRgpSWYfWaQgXee1I_WYBiEizuzOOA2M2LFyd0Z9PNhnLLqEhRXT7Pu-QG6_ixyBFnsmKixRfIGx6ZRBJOOb97v4QnyTh78uY_j4FKSQ_ph-pS_lMcydXMPjBJ5RWKTbts_uIX860Ersu8XACAwCq3mNyVWK8IcY7aVFbfkgfkmNqUGoN_Dk_QVQvXMfwZheoyKVXjkjtCK3FIIpJVzfxX1XpkjN3KnnP3h5p6cBUZWigNnp_OKwhveBEcluuH-rGhEhrIHeSx7LSEBcnSRcxBOO4X5Otf2-UD4Pu8714kP7Fz1uqz0Wr7bIrp0jYEX8RJ7__GFxB-sIFVc5TXuj1quuWhtwd1yUsZnv-o3TQTgP8N7yldH6puFrQ7-bwl61HBhQJuPsPIo2XSGM9rblvRCuNOzuNKiWkH-AMeT6HrvtBCO7TmjyUO_Kk0vCCoZR-McGM9ap0Owf1MtfLcUXGkmxLkETPeRKpevX4fB29z3_76ZrvkIQ9PBYL3GSOZTDwUQpjR3JVpOiz93f01TO80LAuHwlpZ_CTgjB4k_4-YbLoB1TxXd6oNNUZn3QI5P9xk2roAk932MCxKPLhH9I-V8fERj6_Ijbjv7KQgzZjA5M-oeq59CJHSXhzZo9sOVkQcN_1IDxlZrQIcIIOF2uLdfJF6JrVskCUqE-2kX6M\",\n\t\t\"refresh_token\": \"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzUxMiJ9.eyJtZW1iZXJzaGlwX2lkIjoiMmY1ZDMyYTEtZjQ0ZS00OGIwLWFhNTQtZmFkNzIyYWZlYzgxIiwiZXhwIjoxNTI5ODk0MTE5LCJpYXQiOjE1Mjk3MjEzMTksImF1ZCI6InNtYXJ0c2VuZCIsImlzcyI6IiJ9.Vq52EqNtV7EgosgjsShlz5vlPMNN2Xf-yGVz8XfaAUy8ywBiPiAgoEp9Q86WkrpfARPczjUKNKZSwmBNb0OfJmQw8kADT5678JylIc_57LMbP_urqR0coxpZ8UQgw8sNP8z5TA51Vfi97LD2B3QbdZIOnlnQcvlvd9JuMr6enB5UExGEnFK6L41LBdaJ91-vjWTYvyWSUXamlkQU-d7lCqZI_vSxuhDPR6fY1TJ3VakGziYBtoP_e31tbkV7_C67aWcEIXb3h-u4munLn2RjkTZ7umLYWosxvcTjgshWMAqcGNlYimhJAeJnDDCcosRbLJgCzmAliPocSPlZhIqRclq3DcsimnxsXD8gPAniDUEWm7vAyIvDdMqD7PrqfiJW1XTARMi4YbYEXUfEpmr-60TMaMn8798LJ3W-Q4pymbfsbBVYO0z6iH7_PRIWzGSjlHFUjSjIj70p9Yo7XSMdKxU3rb3FJC4jANJZLlpOi4wU60z0YecbRtMhGOqTUnUuKdULU45fPq5WQ9tUAl3ZxqaOtUsJHYZoUrPRWtIoRGNVzKEWEHZlu9ZDkfRwmudPP9PW1wyX6M52FMoFApoBTqruxWvKNHeT9CcrzEVRpoefRzPmoLaosajlQAvy0kbPt_bdj7UaOvIYZAVdxrxVc2JsYyoF7pFTjxkK28x4Gho\",\n\t\t\"membership_id\": \"2f5d32a1-f44e-48b0-aa54-fad722afec81\",\n\t\t\"company_id\": \"6cf86cef-a3ad-4250-93f6-1b1b68a5d10a\",\n\t\t\"user_id\": \"6cc2dd06-8d6f-4fb9-92a7-00e5b28f6ddf\",\n\t\t\"token_type\": \"JWT TOKEN\",\n\t\t\"issued_at\": 1529721319,\n\t\t\"expires_in\": 1529722219,\n\t\t\"refresh_token_expires_in\": 1529894119,\n\t\t\"company_name\": \"ByteCode\",\n\t\t\"role\": \"normal\",\n\t\t\"email\": \"shotokan@hotmail.com\"\n\t}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-sin_cabecera:",
          "content": "En el caso de no tener serviceid en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property serviceid\"\n}\nEn el caso de no tener content-type en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property content-type\"\n}\nEn el caso de no tener authorization en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property of authorization\"\n}",
          "type": "json"
        },
        {
          "title": "Error-serviceid:",
          "content": "En el caso de tener un serviceid incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have permits\"\n}",
          "type": "json"
        },
        {
          "title": "Error-content-type:",
          "content": "En el caso de tener un content-type diferente a application/json:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"you dont have the format correct\"\n}",
          "type": "json"
        },
        {
          "title": "Error-authorization:",
          "content": "En el caso de tener authorization incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have authorization\"\n}",
          "type": "json"
        },
        {
          "title": "Error-grant_type:",
          "content": "\tsi el grant_type no es refresh_token, se manda el mensaje de error:\n\t{\n\t\"code\": 401,\n\t\"headers\": {\n\t\t\"server\": \"gunicorn/19.8.1\",\n\t\t\"date\": \"Wed, 04 Jul 2018 15:55:13 GMT\",\n\t\t\"connection\": \"close\",\n\t\t\"content-length\": \"242\",\n\t\t\"content-type\": \"application/json; charset=UTF-8\"\n\t},\n\t\"body\": \"{\\\"response_id\\\": \\\"2b719f96-b6b0-4da1-939d-d3779b22e4a5\\\", \\\"path\\\": \\\"/v1/oauth/token\\\", \\\"method\\\": \\\"post\\\", \\\"request\\\": 1530719713.0, \\\"msg\\\": \\\"authentication failed\\\", \\\"status\\\": \\\"error\\\", \\\"service\\\": \\\"ms-auth\\\", \\\"data\\\": {\\\"error\\\": \\\"grant_type incorrecto\\\"}}\"\n}\n\n+ @apiErrorExample {json} Error-token:\n\tSi el token ya se refrescó o ya expiró manda el siguiente mensaje:\n\t{\n\t\"code\": 401,\n\t\"headers\": {\n\t\t\"server\": \"gunicorn/19.8.1\",\n\t\t\"date\": \"Wed, 04 Jul 2018 16:16:27 GMT\",\n\t\t\"connection\": \"close\",\n\t\t\"content-length\": \"301\",\n\t\t\"content-type\": \"application/json; charset=UTF-8\"\n\t},\n\t\"body\": \"{\\\"response_id\\\": \\\"dff62d3c-8e40-4a03-9f31-24aabfd40886\\\", \\\"path\\\": \\\"/v1/oauth/token/refresh\\\", \\\"method\\\": \\\"post\\\", \\\"request\\\": 1530720987.0, \\\"msg\\\": \\\"authentication failed\\\", \\\"status\\\": \\\"error\\\", \\\"service\\\": \\\"ms-auth\\\", \\\"data\\\": {\\\"error\\\": \\\"No se puede actualizar el token. Puede que ya no exista o haya expirado.\\\"}}\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/oauth/auth.js",
    "groupTitle": "Login"
  },
  {
    "type": "post",
    "url": "http://206.81.14.204:3000/auth/token",
    "title": "Iniciar Sesion",
    "name": "PostToken",
    "group": "Login",
    "description": "<p>Nos permite iniciar sesion.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "content-type",
            "description": "<p>del tipo application/json que se envia por la cabecera</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "grant_type",
            "description": "<p>tipo de acceso: 'password'</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "email",
            "description": "<p>Correo de la usuario.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "password",
            "description": "<p>Contraseña del usuario.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n\t\"response_id\": \"e70efa7e-6e10-4bea-8e87-7bd20efbc013\",\n\t\"path\": \"/v1/oauth/token\",\n\t\"method\": \"post\",\n\t\"request\": 1529720833.0,\n\t\"msg\": \"token created\",\n\t\"status\": \"ok\",\n\t\"service\": \"ms-auth\",\n\t\"data\": {\n\t\t\"access_token\": \"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzUxMiJ9.eyJjb21wYW55X2lkIjoiNmNmODZjZWYtYTNhZC00MjUwLTkzZjYtMWIxYjY4YTVkMTBhIiwibWVtYmVyc2hpcF9pZCI6IjJmNWQzMmExLWY0NGUtNDhiMC1hYTU0LWZhZDcyMmFmZWM4MSIsInVzZXJfaWQiOiI2Y2MyZGQwNi04ZDZmLTRmYjktOTJhNy0wMGU1YjI4ZjZkZGYiLCJyb2xlIjoibm9ybWFsIiwiY29tcGFueV9uYW1lIjoiQnl0ZUNvZGUiLCJleHAiOjE1Mjk3MjE3MzMsImlhdCI6MTUyOTcyMDgzMywiYXVkIjoic21hcnRzZW5kIiwiaXNzIjoiIn0.CkVmX-ABTpUAd9KW6TbUCs5Qg0UvNOM1B7pcCM19FiO_81t_5V_PuPvQ5l1e0jldTRpmRCa74pjQOx2NeYhO2yO9QH6vFRWkaFBxV4_JyRbuO5GzuzUMkMi5PLMdjO-pXr0A4e5ie_gSUoefMRuTFDRgY0kiuLqdogxj2r-8uIMl3spceI0jaGg9Hd6JvBMcA4Es1E2zQ4zjIMt4GCqY262zJNw39g0PWDiBtiNFivAfb3HQvPUgv0DPd8lb9h4l7DSsyZtMqGkNd59t9t2Rk4dFeW65OkMevNPMp3UJ4X5QgIgJBJyXHRRPMMqzIVbfr9O8_RSMBpJilfX9VT9NghC2Fu--t86nyK8OcjTzPEb0fBdfUejr6Jv7_Me7mPCkRo-s3rFI_x_nwOTlzjOB49V70QrjlEwUQh5uCubM0kwqzmBUFsHoZv_b0jeEnaLsnT4VjhKTo6NKNjFq8-MzT8_QIoFSl5EULpsYd2i8vKpW4Q49pfiSNr_nXF0bp6xKcKxPd2iUjmGmjBFKadBcqO4fjwcKAY1Jd5oFml3TlHuLMjhgnW8PSHouTvzJN9YA0MV-JruCvJ-RHNAv4OXP3U8-cmIJQ3lfL6-zaq_71PLi1pZz4mQ50NLgOwaekBbNEm0wxgHLcjHbn91SjZNdidYz65zKIiG2pIZ3am2Q94A\",\n\t\t\"refresh_token\": \"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzUxMiJ9.eyJtZW1iZXJzaGlwX2lkIjoiMmY1ZDMyYTEtZjQ0ZS00OGIwLWFhNTQtZmFkNzIyYWZlYzgxIiwiZXhwIjoxNTI5ODkzNjMzLCJpYXQiOjE1Mjk3MjA4MzMsImF1ZCI6InNtYXJ0c2VuZCIsImlzcyI6IiJ9.AmeK0eQxGh_q3kwoCnRmA6_rESSn4o8s2amHc5FlIld7mL3PtmUsB12HS3Yr3F1tTWcSp1TukvzFTogxjLEkx1WOurND8D9CJ0M52qhbvu2dmgpSDipK6c79YnK7NRfDrf2KWtT5MPKzitMDr4v4CKbaI8mZEfh0v6iPmP76Wt81GPgJpeB0_xyWxNxH3sKzEUSMiBxGA4CGz7m2mZcumApiduhgOz3Uehqybl5R94zs4b3ifFs-9XdwzXWXJjeXVvwrs1tFuafGt3AqEKXk9Wic-RnG8UoZc-R2c9wTUQUmVAB8Aucmmp_n39PJUGlL4GCJcLQJHEqOAYmHRm8I7xJ7Y5ss7zkvnuVdP339I7rFzjLdh5-Gj34o7sfN3QpOpxw9CPvPf2RXyIqAwn7Iy5FdDzYSrWF3cV9PXVlOKBEkjBT7KybFM4OAhgSMlJnJC3dD3neT0lj5o2wSwXDG7lyS4_XzzDmmWquHlIRR6b-Vhuk8RmUuU0rGHERecP04Tq6GQr0VV-ejHSBM9wpZPE5mjZNMiQTcuM30aR1-ScmWKGbLKq5N4lFb9jvv2vHjeSecSTaIOcsifvtZvwPO3eBj6w9jxk4Rq1DTjKSgeRBc2TyDmndeYDQhbjHhCq3TaSv0qnZOSGtrmhKaei7Hw4ts2pSlYJvrsrq5-bi5-oQ\",\n\t\t\"membership_id\": \"2f5d32a1-f44e-48b0-aa54-fad722afec81\",\n\t\t\"company_id\": \"6cf86cef-a3ad-4250-93f6-1b1b68a5d10a\",\n\t\t\"user_id\": \"6cc2dd06-8d6f-4fb9-92a7-00e5b28f6ddf\",\n\t\t\"token_type\": \"JWT TOKEN\",\n\t\t\"issued_at\": 1529720833,\n\t\t\"expires_in\": 1529721733,\n\t\t\"refresh_token_expires_in\": 1529893633,\n\t\t\"company_name\": \"ByteCode\",\n\t\t\"role\": \"normal\",\n\t\t\"email\": \"shotokan@hotmail.com\"\n\t}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-sin_cabecera:",
          "content": "En el caso de no tener serviceid en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property serviceid\"\n}\nEn el caso de no tener content-type en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property content-type\"\n}",
          "type": "json"
        },
        {
          "title": "Error-serviceid:",
          "content": "En el caso de tener un serviceid incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have permits\"\n}",
          "type": "json"
        },
        {
          "title": "Error-content-type:",
          "content": "En el caso de tener un content-type diferente a application/json:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"you dont have the format correct\"\n}",
          "type": "json"
        },
        {
          "title": "Error-credenciales:",
          "content": "\tSi el usuario ingresa credenciales incorrectas.\n\t{\n\t\"code\": 401,\n\t\"headers\": {\n\t\t\"server\": \"gunicorn/19.8.1\",\n\t\t\"date\": \"Wed, 04 Jul 2018 15:52:17 GMT\",\n\t\t\"connection\": \"close\",\n\t\t\"content-length\": \"245\",\n\t\t\"content-type\": \"application/json; charset=UTF-8\"\n\t},\n\t\"body\": \"{\\\"response_id\\\": \\\"5b30b357-6cab-4c8b-a1bc-b249bd93cd61\\\", \\\"path\\\": \\\"/v1/oauth/token\\\", \\\"method\\\": \\\"post\\\", \\\"request\\\": 1530719537.0, \\\"msg\\\": \\\"authentication failed\\\", \\\"status\\\": \\\"error\\\", \\\"service\\\": \\\"ms-auth\\\", \\\"data\\\": {\\\"error\\\": \\\"Credenciales incorrectas\\\"}}\"\n}",
          "type": "json"
        },
        {
          "title": "Error-grant_type:",
          "content": "si el grant_type no es password, se manda el siguiente mensaje:\n{\n\t\"code\": 401,\n\t\"headers\": {\n\t\t\"server\": \"gunicorn/19.8.1\",\n\t\t\"date\": \"Wed, 04 Jul 2018 15:52:43 GMT\",\n\t\t\"connection\": \"close\",\n\t\t\"content-length\": \"242\",\n\t\t\"content-type\": \"application/json; charset=UTF-8\"\n\t},\n\t\"body\": \"{\\\"response_id\\\": \\\"b2e1c1df-8380-4ba4-91ac-92822bc677be\\\", \\\"path\\\": \\\"/v1/oauth/token\\\", \\\"method\\\": \\\"post\\\", \\\"request\\\": 1530719563.0, \\\"msg\\\": \\\"authentication failed\\\", \\\"status\\\": \\\"error\\\", \\\"service\\\": \\\"ms-auth\\\", \\\"data\\\": {\\\"error\\\": \\\"grant_type incorrecto\\\"}}\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/oauth/auth.js",
    "groupTitle": "Login"
  },
  {
    "type": "post",
    "url": "http://206.81.14.204:3000/sender/upload",
    "title": "Agregar una categoria",
    "name": "PostUpload",
    "group": "Sender",
    "description": "<p>Nos permite subir un archivo.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "content-type",
            "description": "<p>Del tipo application/json que se envia por la cabecera.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "authorization",
            "description": "<p>Token que se envia por la cabecera y es: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJ2YWx1ZSJ9.FG-8UppwHaFp1LgRYQQeS6EDQF7_6-bMFegNucHjmWg</p>"
          },
          {
            "group": "Parameter",
            "type": "uuid",
            "optional": false,
            "field": "company_id",
            "description": "<p>id de la compañia y se envia por el body.</p>"
          },
          {
            "group": "Parameter",
            "type": "uuid",
            "optional": false,
            "field": "user_id",
            "description": "<p>id del usuario y se envia por el body.</p>"
          },
          {
            "group": "Parameter",
            "type": "image",
            "optional": false,
            "field": "photo",
            "description": "<p>archivo que se subirá y se envia por el body.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "message",
            "description": "<p>Es un mensaje y se envia por el body.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n\t\"status\": \"200\",\n\t\"msg\": \"New register\",\n\t\"data\": []\n   }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-sin_cabecera:",
          "content": "En el caso de no tener serviceid en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property serviceid\"\n}\nEn el caso de no tener content-type en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property content-type\"\n}\nEn el caso de no tener authorization en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property of authorization\"\n}",
          "type": "json"
        },
        {
          "title": "Error-serviceid:",
          "content": "En el caso de tener un serviceid incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have permits\"\n}",
          "type": "json"
        },
        {
          "title": "Error-content-type:",
          "content": "En el caso de tener un content-type diferente a application/json:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"you dont have the format correct\"\n}",
          "type": "json"
        },
        {
          "title": "Error-authorization:",
          "content": "En el caso de tener authorization incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have authorization\"\n}",
          "type": "json"
        },
        {
          "title": "Error-token:",
          "content": "El access_token expira en cierto tiempo y si eso pasa sale este mensaje:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 401,\n\t\"status\": 401,\n\t\"message\": \"el Token ya expiró\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/sender/sender.js",
    "groupTitle": "Sender"
  },
  {
    "type": "post",
    "url": "http://206.81.14.204:8003/v1/categories/companies/idcompany/sms",
    "title": "Obtener sms",
    "group": "Sms",
    "description": "<p>Se obtienen los sms registrados o enviados por una compañia.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "content-type",
            "description": "<p>Del tipo application/json que se envia por la cabecera.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "authorization",
            "description": "<p>Token que se envia por la cabecera y es: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJ2YWx1ZSJ9.FG-8UppwHaFp1LgRYQQeS6EDQF7_6-bMFegNucHjmWg</p>"
          },
          {
            "group": "Parameter",
            "type": "uuid",
            "optional": false,
            "field": "idcompany",
            "description": "<p>ID de la categoria y se envia por la url.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n    \"response_id\": \"e107a984-6d61-4660-bafa-2de98c7d9991\",\n    \"path\": \"/v1/companies/2f5d32a1-f44e-48b0-aa54-fad722afec81/sms\",\n    \"method\": \"get\",\n    \"request\": 1533318209,\n    \"msg\": \"list\",\n    \"api-version\": \"v1\",\n    \"status\": \"ok\",\n    \"service\": \"MS-SMS\",\n    \"data\": []\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-sin_cabecera:",
          "content": "En el caso de no tener serviceid en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property serviceid\"\n}\nEn el caso de no tener content-type en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property content-type\"\n}\nEn el caso de no tener authorization en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property of authorization\"\n}",
          "type": "json"
        },
        {
          "title": "Error-serviceid:",
          "content": "En el caso de tener un serviceid incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have permits\"\n}",
          "type": "json"
        },
        {
          "title": "Error-content-type:",
          "content": "En el caso de tener un content-type diferente a application/json:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"you dont have the format correct\"\n}",
          "type": "json"
        },
        {
          "title": "Error-authorization:",
          "content": "En el caso de tener authorization incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have authorization\"\n}",
          "type": "json"
        },
        {
          "title": "Error-token:",
          "content": "El access_token expira en cierto tiempo y si eso pasa sale este mensaje:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 401,\n\t\"status\": 401,\n\t\"message\": \"el Token ya expiró\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/categories/Category.js",
    "groupTitle": "Sms",
    "name": "PostHttp20681142048003V1CategoriesCompaniesIdcompanySms"
  },
  {
    "type": "delete",
    "url": "http://206.81.14.204:3000/users/company/idcompany/user/iduser",
    "title": "",
    "name": "DeleteUser",
    "group": "User",
    "description": "<p>Nos permite borrar un usuario.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "content-type",
            "description": "<p>del tipo application/json que se envia por la cabecera</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "authorization",
            "description": "<p>Token que se envia por la cabecera y es: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJ2YWx1ZSJ9.FG-8UppwHaFp1LgRYQQeS6EDQF7_6-bMFegNucHjmWg</p>"
          },
          {
            "group": "Parameter",
            "type": "uuid",
            "optional": false,
            "field": "idcompany",
            "description": "<p>Identificador de la compañia, éste se envia por medio de la url.</p>"
          },
          {
            "group": "Parameter",
            "type": "uuid",
            "optional": false,
            "field": "iduser",
            "description": "<p>Identificador del usuario, éste se envia por medio de la url.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "No body returned for response",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-sin_cabecera:",
          "content": "En el caso de no tener serviceid en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property serviceid\"\n}\nEn el caso de no tener content-type en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property content-type\"\n}\nEn el caso de no tener authorization en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property of authorization\"\n}",
          "type": "json"
        },
        {
          "title": "Error-serviceid:",
          "content": "En el caso de tener un serviceid incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have permits\"\n}",
          "type": "json"
        },
        {
          "title": "Error-content-type:",
          "content": "En el caso de tener un content-type diferente a application/json:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"you dont have the format correct\"\n}",
          "type": "json"
        },
        {
          "title": "Error-authorization:",
          "content": "En el caso de tener authorization incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have authorization\"\n}",
          "type": "json"
        },
        {
          "title": "Error-token:",
          "content": "El access_token expira en cierto tiempo y si eso pasa sale este mensaje:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 401,\n\t\"status\": 401,\n\t\"message\": \"el Token ya expiró\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/users/user.js",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "http://206.81.14.204:3000/users/memberships/idcompany",
    "title": "Membresías",
    "name": "GetMemberships",
    "group": "User",
    "description": "<p>nos permite ver las membresías de una compañia.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "content-type",
            "description": "<p>del tipo application/json que se envia por la cabecera</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "authorization",
            "description": "<p>Token que se envia por la cabecera y es: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJ2YWx1ZSJ9.FG-8UppwHaFp1LgRYQQeS6EDQF7_6-bMFegNucHjmWg</p>"
          },
          {
            "group": "Parameter",
            "type": "uuid",
            "optional": false,
            "field": "idcompany",
            "description": "<p>Identificador de la compañia que se envia por la url.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n\t\"response_id\": \"50ae66cb-8c62-44d8-a89a-3c6b32536972\",\n\t\"path\": \"/v1/companies/6cf86cef-a3ad-4250-93f6-1b1b68a5d10a/memberships\",\n\t\"method\": \"get\",\n\t\"request\": 1529058414.0,\n\t\"msg\": \"list\",\n\t\"status\": 200,\n\t\"service\": \"ms-users\",\n\t\"data\": {\n\t\t\"users\": [\n\t\t\t{\n\t\t\t\t\"id\": \"486d6ab2-a9e9-41ed-b3f6-d03f83116293\",\n\t\t\t\t\"name\": \"comodoro_21@hotmail.com\",\n\t\t\t\t\"lastname\": \"gafokebog@mailinator.net\",\n\t\t\t\t\"email\": \"momamaqexu@mailinator.com\",\n\t\t\t\t\"role\": \"normal\",\n\t\t\t\t\"cellphone\": \"kekyd@mailinator.com\",\n\t\t\t\t\"company\": \"ByteCode\",\n\t\t\t\t\"membership_id\": \"0cfbdf8b-cba8-4808-96ae-89620b4712e7\",\n\t\t\t\t\"credit\": 0\n\t\t\t},\n\t\t\t{\n\t\t\t\t\"id\": \"b71196ee-d14a-4000-b741-f7717370deae\",\n\t\t\t\t\"name\": \"fypyh@mailinator.net\",\n\t\t\t\t\"lastname\": \"lagerozyj@mailinator.net\",\n\t\t\t\t\"email\": \"cexat@mailinator.com\",\n\t\t\t\t\"role\": \"normal\",\n\t\t\t\t\"cellphone\": \"jygamolyba@mailinator.net\",\n\t\t\t\t\"company\": \"ByteCode\",\n\t\t\t\t\"membership_id\": \"152ae015-ab69-4888-b3be-e0f501913360\",\n\t\t\t\t\"credit\": 0\n            },\n\t}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-sin_cabecera:",
          "content": "En el caso de no tener serviceid en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property serviceid\"\n}\nEn el caso de no tener content-type en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property content-type\"\n}\nEn el caso de no tener authorization en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property of authorization\"\n}",
          "type": "json"
        },
        {
          "title": "Error-serviceid:",
          "content": "En el caso de tener un serviceid incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have permits\"\n}",
          "type": "json"
        },
        {
          "title": "Error-content-type:",
          "content": "En el caso de tener un content-type diferente a application/json:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"you dont have the format correct\"\n}",
          "type": "json"
        },
        {
          "title": "Error-authorization:",
          "content": "En el caso de tener authorization incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have authorization\"\n}",
          "type": "json"
        },
        {
          "title": "Error-token:",
          "content": "El access_token expira en cierto tiempo y si eso pasa sale este mensaje:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 401,\n\t\"status\": 401,\n\t\"message\": \"el Token ya expiró\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/users/user.js",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "http://206.81.14.204:3000/users/roles",
    "title": "Mostrar roles",
    "name": "GetRoles",
    "group": "User",
    "description": "<p>nos va a permitir obtener los roles de la compañia.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "content-type",
            "description": "<p>del tipo application/json que se envia por la cabecera</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "authorization",
            "description": "<p>Token que se envia por la cabecera y es: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJ2YWx1ZSJ9.FG-8UppwHaFp1LgRYQQeS6EDQF7_6-bMFegNucHjmWg</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n\t\"response_id\": \"1d127649-d5b5-431c-8449-f72a907a68e1\",\n\t\"path\": \"/v1/roles\",\n\t\"method\": \"get\",\n\t\"request\": 1529043706.0,\n\t\"msg\": \"list\",\n\t\"status\": 200,\n\t\"service\": \"ms-users\",\n\t\"data\": [\n\t\t{\n\t\t\t\"id\": \"27166b61-d386-418d-ae23-5050455ab5e8\",\n\t\t\t\"name\": \"admin\"\n\t\t},\n\t\t{\n\t\t\t\"id\": \"7712e1eb-bf99-4ab1-8ca2-4d5918139d63\",\n\t\t\t\"name\": \"normal\"\n\t\t}\n\t]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-sin_cabecera:",
          "content": "En el caso de no tener serviceid en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property serviceid\"\n}\nEn el caso de no tener content-type en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property content-type\"\n}\nEn el caso de no tener authorization en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property of authorization\"\n}",
          "type": "json"
        },
        {
          "title": "Error-serviceid:",
          "content": "En el caso de tener un serviceid incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have permits\"\n}",
          "type": "json"
        },
        {
          "title": "Error-content-type:",
          "content": "En el caso de tener un content-type diferente a application/json:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"you dont have the format correct\"\n}",
          "type": "json"
        },
        {
          "title": "Error-authorization:",
          "content": "En el caso de tener authorization incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have authorization\"\n}",
          "type": "json"
        },
        {
          "title": "Error-token:",
          "content": "El access_token expira en cierto tiempo y si eso pasa sale este mensaje:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 401,\n\t\"status\": 401,\n\t\"message\": \"el Token ya expiró\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/users/user.js",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "http://206.81.14.204:3000/users/company/idcompany/user/iduser",
    "title": "Informacion del usuario",
    "name": "GetUser",
    "group": "User",
    "description": "<p>nos permite ver la informacion de un usuario.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "content-type",
            "description": "<p>del tipo application/json que se envia por la cabecera</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "authorization",
            "description": "<p>Token que se envia por la cabecera y es: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJ2YWx1ZSJ9.FG-8UppwHaFp1LgRYQQeS6EDQF7_6-bMFegNucHjmWg</p>"
          },
          {
            "group": "Parameter",
            "type": "uuid",
            "optional": false,
            "field": "idcompany",
            "description": "<p>Identificador de la compañia, se envia por la url.</p>"
          },
          {
            "group": "Parameter",
            "type": "uuid",
            "optional": false,
            "field": "iduser",
            "description": "<p>Identificador del usuario, se envia por la url.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n\t\"response_id\": \"b914d9d0-50bf-47de-8ae7-d5b8e8629874\",\n\t\"path\": \"/v1/companies/6cf86cef-a3ad-4250-93f6-1b1b68a5d10a/userss/39c7d8ec-e676-46f1-a4d3-bedecec35707\",\n\t\"method\": \"get\",\n\t\"request\": 1529157267.0,\n\t\"msg\": \"found\",\n\t\"status\": \"ok\",\n\t\"service\": \"ms-users\",\n\t\"data\": {\n\t\t\"id\": \"39c7d8ec-e676-46f1-a4d3-bedecec35707\",\n\t\t\"name\": \"jordan\",\n\t\t\"lastname\": \"gongor\",\n\t\t\"email\": \"jordang@hotmail.com\",\n\t\t\"role\": \"normal\",\n\t\t\"cellphone\": \"9999999\",\n\t\t\"company\": \"ByteCode\",\n\t\t\"membership_id\": \"18cfc500-cdc1-41e9-99a0-f1f14fb09482\",\n\t\t\"credit\": 0,\n\t\t\"company_id\": \"6cf86cef-a3ad-4250-93f6-1b1b68a5d10a\",\n\t\t\"company_name\": \"ByteCode\"\n\t}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-sin_cabecera:",
          "content": "En el caso de no tener serviceid en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property serviceid\"\n}\nEn el caso de no tener content-type en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property content-type\"\n}\nEn el caso de no tener authorization en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property of authorization\"\n}",
          "type": "json"
        },
        {
          "title": "Error-serviceid:",
          "content": "En el caso de tener un serviceid incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have permits\"\n}",
          "type": "json"
        },
        {
          "title": "Error-content-type:",
          "content": "En el caso de tener un content-type diferente a application/json:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"you dont have the format correct\"\n}",
          "type": "json"
        },
        {
          "title": "Error-authorization:",
          "content": "En el caso de tener authorization incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have authorization\"\n}",
          "type": "json"
        },
        {
          "title": "Error-token:",
          "content": "El access_token expira en cierto tiempo y si eso pasa sale este mensaje:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 401,\n\t\"status\": 401,\n\t\"message\": \"el Token ya expiró\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/users/user.js",
    "groupTitle": "User"
  },
  {
    "type": "patch",
    "url": "http://206.81.14.204:3000/users/password",
    "title": "Cambiar contreseña.",
    "name": "PatchPassword",
    "group": "User",
    "description": "<p>Nos permite cambiar la contraseña del usuario.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "content-type",
            "description": "<p>del tipo application/json que se envia por la cabecera</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "authorization",
            "description": "<p>Token que se envia por la cabecera y es: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJ2YWx1ZSJ9.FG-8UppwHaFp1LgRYQQeS6EDQF7_6-bMFegNucHjmWg</p>"
          },
          {
            "group": "Parameter",
            "type": "uuid",
            "optional": false,
            "field": "company_id",
            "description": "<p>Identificador de la compañia.</p>"
          },
          {
            "group": "Parameter",
            "type": "uuid",
            "optional": false,
            "field": "id",
            "description": "<p>Identificador del usuario.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "password",
            "description": "<p>contraseña.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "No body returned for response",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-sin_cabecera:",
          "content": "En el caso de no tener serviceid en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property serviceid\"\n}\nEn el caso de no tener content-type en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property content-type\"\n}\nEn el caso de no tener authorization en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property of authorization\"\n}",
          "type": "json"
        },
        {
          "title": "Error-serviceid:",
          "content": "En el caso de tener un serviceid incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have permits\"\n}",
          "type": "json"
        },
        {
          "title": "Error-content-type:",
          "content": "En el caso de tener un content-type diferente a application/json:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"you dont have the format correct\"\n}",
          "type": "json"
        },
        {
          "title": "Error-authorization:",
          "content": "En el caso de tener authorization incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have authorization\"\n}",
          "type": "json"
        },
        {
          "title": "Error-token:",
          "content": "El access_token expira en cierto tiempo y si eso pasa sale este mensaje:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 401,\n\t\"status\": 401,\n\t\"message\": \"el Token ya expiró\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/users/user.js",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "http://206.81.14.204:3000/users/user",
    "title": "Crear usuario",
    "name": "PostUser",
    "group": "User",
    "description": "<p>Nos permite crear un usuario.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "content-type",
            "description": "<p>del tipo application/json que se envia por la cabecera</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "authorization",
            "description": "<p>Token que se envia por la cabecera y es: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJ2YWx1ZSJ9.FG-8UppwHaFp1LgRYQQeS6EDQF7_6-bMFegNucHjmWg</p>"
          },
          {
            "group": "Parameter",
            "type": "uuid",
            "optional": false,
            "field": "Identificador",
            "description": "<p>de la compañia y se envia por el body.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "name",
            "description": "<p>Nombre del usuario y se envia por el body.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "lastname",
            "description": "<p>apellido y se envia por el body.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "Password",
            "description": "<p>contraseña y se envia por el body.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "email",
            "description": "<p>correo del usuario y se envia por el body.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "cellphone",
            "description": "<p>telefono de contacto y se envia por el body.</p>"
          },
          {
            "group": "Parameter",
            "type": "uuid",
            "optional": false,
            "field": "role_id",
            "description": "<p>Identificador del rol y se envia por el body.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n\t\"response_id\": \"0696e21b-93e7-4148-a138-0e676423e86d\",\n\t\"path\": \"/v1/companies/6cf86cef-a3ad-4250-93f6-1b1b68a5d10a/userss\",\n\t\"method\": \"post\",\n\t\"request\": 1529156981.0,\n\t\"msg\": \"created\",\n\t\"status\": \"ok\",\n\t\"service\": \"ms-users\",\n\t\"data\": {\n\t\t\"id\": \"71fe96e5-5425-4d74-9fd1-ad2c3a48192e\",\n\t\t\"name\": \"jordann\",\n\t\t\"lastname\": \"gongora\",\n\t\t\"email\": \"estebann@hotmail.com\",\n\t\t\"role\": \"normal\",\n\t\t\"cellphone\": \"9999999\",\n\t\t\"company\": \"ByteCode\",\n\t\t\"membership_id\": \"3afbd9a0-565c-42c6-8848-d96b75949baa\",\n\t\t\"credit\": 0,\n\t\t\"company_id\": \"6cf86cef-a3ad-4250-93f6-1b1b68a5d10a\",\n\t\t\"company_name\": \"ByteCode\"\n\t}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-sin_cabecera:",
          "content": "En el caso de no tener serviceid en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property serviceid\"\n}\nEn el caso de no tener content-type en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property content-type\"\n}\nEn el caso de no tener authorization en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property of authorization\"\n}",
          "type": "json"
        },
        {
          "title": "Error-serviceid:",
          "content": "En el caso de tener un serviceid incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have permits\"\n}",
          "type": "json"
        },
        {
          "title": "Error-content-type:",
          "content": "En el caso de tener un content-type diferente a application/json:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"you dont have the format correct\"\n}",
          "type": "json"
        },
        {
          "title": "Error-authorization:",
          "content": "En el caso de tener authorization incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have authorization\"\n}",
          "type": "json"
        },
        {
          "title": "Error-token:",
          "content": "El access_token expira en cierto tiempo y si eso pasa sale este mensaje:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 401,\n\t\"status\": 401,\n\t\"message\": \"el Token ya expiró\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/users/user.js",
    "groupTitle": "User"
  },
  {
    "type": "put",
    "url": "http://206.81.14.204:3000/users/roles",
    "title": "Actualizar Roles",
    "name": "PutRoles",
    "group": "User",
    "description": "<p>Permite actualizar el rol de un usuario.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "content-type",
            "description": "<p>del tipo application/json que se envia por la cabecera</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "authorization",
            "description": "<p>Token que se envia por la cabecera y es: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJ2YWx1ZSJ9.FG-8UppwHaFp1LgRYQQeS6EDQF7_6-bMFegNucHjmWg</p>"
          },
          {
            "group": "Parameter",
            "type": "uuid",
            "optional": false,
            "field": "role_id",
            "description": "<p>Identificador del rol y se envia por el body.</p>"
          },
          {
            "group": "Parameter",
            "type": "uuid",
            "optional": false,
            "field": "membership_id",
            "description": "<p>Identificador de la membresia y se envia por el body.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "No body returned for response",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-sin_cabecera:",
          "content": "En el caso de no tener serviceid en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property serviceid\"\n}\nEn el caso de no tener content-type en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property content-type\"\n}\nEn el caso de no tener authorization en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property of authorization\"\n}",
          "type": "json"
        },
        {
          "title": "Error-serviceid:",
          "content": "En el caso de tener un serviceid incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have permits\"\n}",
          "type": "json"
        },
        {
          "title": "Error-content-type:",
          "content": "En el caso de tener un content-type diferente a application/json:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"you dont have the format correct\"\n}",
          "type": "json"
        },
        {
          "title": "Error-authorization:",
          "content": "En el caso de tener authorization incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have authorization\"\n}",
          "type": "json"
        },
        {
          "title": "Error-token:",
          "content": "El access_token expira en cierto tiempo y si eso pasa sale este mensaje:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 401,\n\t\"status\": 401,\n\t\"message\": \"el Token ya expiró\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/users/user.js",
    "groupTitle": "User"
  },
  {
    "type": "put",
    "url": "http://206.81.14.204:3000/users/user",
    "title": "actualizar usuario",
    "name": "PutUser",
    "group": "User",
    "description": "<p>nos permite actualizar informacion del usuario.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "content-type",
            "description": "<p>del tipo application/json que se envia por la cabecera</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "authorization",
            "description": "<p>Token que se envia por la cabecera y es: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJ2YWx1ZSJ9.FG-8UppwHaFp1LgRYQQeS6EDQF7_6-bMFegNucHjmWg</p>"
          },
          {
            "group": "Parameter",
            "type": "uuid",
            "optional": false,
            "field": "company_id",
            "description": "<p>Identificador de la compañia y se envia por el body.</p>"
          },
          {
            "group": "Parameter",
            "type": "uuid",
            "optional": false,
            "field": "id",
            "description": "<p>Identificador del usuario y se envia por el body.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "name",
            "description": "<p>Nombre del usuario y se envia por el body.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "lastname",
            "description": "<p>Apellido y se envia por el body.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "password",
            "description": "<p>Contraseña y se envia por el body.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "email",
            "description": "<p>Correo del usuario y se envia por el body.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "cellphone",
            "description": "<p>Telefono de contacto y se envia por el body.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "role_id",
            "description": "<p>Identificador del rol y se envia por el body.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n\t\"response_id\": \"b0ba2d9a-23d1-4059-8ae2-3268e35c4879\",\n\t\"path\": \"/v1/companies/6cf86cef-a3ad-4250-93f6-1b1b68a5d10a/userss/39c7d8ec-e676-46f1-a4d3-bedecec35707\",\n\t\"method\": \"get\",\n\t\"request\": 1529059948.0,\n\t\"msg\": \"found\",\n\t\"status\": \"ok\",\n\t\"service\": \"ms-users\",\n\t\"data\": {\n\t\t\"id\": \"39c7d8ec-e676-46f1-a4d3-bedecec35707\",\n\t\t\"name\": \"jordan\",\n\t\t\"lastname\": \"gongora\",\n\t\t\"email\": \"jordang@hotmail.com\",\n\t\t\"role\": \"\",\n\t\t\"cellphone\": \"9999999\",\n\t\t\"company\": \"\",\n\t\t\"membership_id\": \"\",\n\t\t\"credit\": 0,\n\t\t\"company_id\": \"\",\n\t\t\"company_name\": \"\"\n\t}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-sin_cabecera:",
          "content": "En el caso de no tener serviceid en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property serviceid\"\n}\nEn el caso de no tener content-type en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property content-type\"\n}\nEn el caso de no tener authorization en la cabecera:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have the property of authorization\"\n}",
          "type": "json"
        },
        {
          "title": "Error-serviceid:",
          "content": "En el caso de tener un serviceid incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have permits\"\n}",
          "type": "json"
        },
        {
          "title": "Error-content-type:",
          "content": "En el caso de tener un content-type diferente a application/json:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"you dont have the format correct\"\n}",
          "type": "json"
        },
        {
          "title": "Error-authorization:",
          "content": "En el caso de tener authorization incorrecto:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 412,\n\t\"status\": 412,\n\t\"message\": \"You dont have authorization\"\n}",
          "type": "json"
        },
        {
          "title": "Error-token:",
          "content": "El access_token expira en cierto tiempo y si eso pasa sale este mensaje:\n{\n\t\"name\": \"HTTPError\",\n\t\"statusCode\": 401,\n\t\"status\": 401,\n\t\"message\": \"el Token ya expiró\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/users/user.js",
    "groupTitle": "User"
  },
  {
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "varname1",
            "description": "<p>No type.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "varname2",
            "description": "<p>With type.</p>"
          }
        ]
      }
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "./doc/main.js",
    "group": "_home_pablo_Documentos_node_projects_sms_apigateway_doc_main_js",
    "groupTitle": "_home_pablo_Documentos_node_projects_sms_apigateway_doc_main_js",
    "name": ""
  }
] });
