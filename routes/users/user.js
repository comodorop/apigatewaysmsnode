var express = require('express');
var router = express.Router();
var request = require('request');
var requestify = require('requestify');
const obj = require('config-yml');
var requestHttp = require('../../utilities/requestHttp')
var validateRole = require('../../middlewares/roleValidation')
var validateSameRoleCompany = require('../../middlewares/sameCompany')

var id = ''
var iduser = ''
var idcompany = ''
/**
 * @api {get} http://206.81.14.204:3000/users/roles Mostrar roles
 * @apiName GetRoles
 * @apiGroup User
 * @apiDescription nos va a permitir obtener los roles de la compañia.
 * @apiParam {string} content-type del tipo application/json que se envia por la cabecera
 * @apiParam {string} authorization Token que se envia por la cabecera y es: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJ2YWx1ZSJ9.FG-8UppwHaFp1LgRYQQeS6EDQF7_6-bMFegNucHjmWg
 * @apiSuccessExample Success-Response:
 * {
	"response_id": "1d127649-d5b5-431c-8449-f72a907a68e1",
	"path": "/v1/roles",
	"method": "get",
	"request": 1529043706.0,
	"msg": "list",
	"status": 200,
	"service": "ms-users",
	"data": [
		{
			"id": "27166b61-d386-418d-ae23-5050455ab5e8",
			"name": "admin"
		},
		{
			"id": "7712e1eb-bf99-4ab1-8ca2-4d5918139d63",
			"name": "normal"
		}
	]
}
 * @apiErrorExample {json} Error-sin_cabecera:
 * En el caso de no tener serviceid en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property serviceid"
}
* En el caso de no tener content-type en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property content-type"
}
* En el caso de no tener authorization en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property of authorization"
}
* @apiErrorExample {json} Error-serviceid:
 * En el caso de tener un serviceid incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have permits"
}
* @apiErrorExample {json} Error-content-type:
 * En el caso de tener un content-type diferente a application/json:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "you dont have the format correct"
}
* @apiErrorExample {json} Error-authorization:
 * En el caso de tener authorization incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have authorization"
}
@apiErrorExample {json} Error-token:
 *El access_token expira en cierto tiempo y si eso pasa sale este mensaje:
 *{
	"name": "HTTPError",
	"statusCode": 401,
	"status": 401,
	"message": "el Token ya expiró"
}
 */
router.get('/roles', validateRole.verifyAdmin, (req, resp) => {
	console.log('entro a los roles')
	var respuesta = requestHttp.requestGet2(`${obj.config.MSUSER.host}/v1/roles`, obj.config.MSUSER.serviceid, obj.config.MSUSER.authorization, req.token.company_id || '').then(msg => {
		var body = JSON.parse(msg.body)
		resp.status(msg.code).send({status: msg.code, data:body.data, pagination: body.pagination})
	}).catch(err => {
		var body = JSON.parse(err.body)
		resp.status(err.code).send({status: err.code, data:body.data })
	})
})

/**
 * @api {put} http://206.81.14.204:3000/users/roles Actualizar Roles
 * @apiName PutRoles
 * @apiGroup User
 * @apiDescription Permite actualizar el rol de un usuario.
 * @apiParam {string} content-type del tipo application/json que se envia por la cabecera
 * @apiParam {string} authorization Token que se envia por la cabecera y es: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJ2YWx1ZSJ9.FG-8UppwHaFp1LgRYQQeS6EDQF7_6-bMFegNucHjmWg
 * @apiParam {uuid} role_id Identificador del rol y se envia por el body.
 * @apiParam {uuid} membership_id Identificador de la membresia y se envia por el body.
 * @apiSuccessExample Success-Response:
 * No body returned for response
 * * @apiErrorExample {json} Error-sin_cabecera:
 * En el caso de no tener serviceid en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property serviceid"
}
* En el caso de no tener content-type en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property content-type"
}
* En el caso de no tener authorization en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property of authorization"
}
* @apiErrorExample {json} Error-serviceid:
 * En el caso de tener un serviceid incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have permits"
}
* @apiErrorExample {json} Error-content-type:
 * En el caso de tener un content-type diferente a application/json:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "you dont have the format correct"
}
* @apiErrorExample {json} Error-authorization:
 * En el caso de tener authorization incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have authorization"
}
@apiErrorExample {json} Error-token:
 *El access_token expira en cierto tiempo y si eso pasa sale este mensaje:
 *{
	"name": "HTTPError",
	"statusCode": 401,
	"status": 401,
	"message": "el Token ya expiró"
}
 */
router.put('/roles', validateRole.verifyAdmin, (req, resp) => {
	var respuesta = requestHttp.requestMethod(req.method, req.body, `${obj.config.MSUSER.host}/v1/memberships/${req.body.membership_id}/roles`, obj.config.MSUSER.serviceid, obj.config.MSUSER.authorization, req.token.company_id).then(msg => {
		resp.send(msg)
	}, err => {
		resp.send(err)
	})
})
/**
 * @api {post} http://206.81.14.204:3000/users/user Crear usuario
 * @apiName PostUser
 * @apiGroup User
 * @apiDescription Nos permite crear un usuario.
 * @apiParam {string} content-type del tipo application/json que se envia por la cabecera
 * @apiParam {string} authorization Token que se envia por la cabecera y es: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJ2YWx1ZSJ9.FG-8UppwHaFp1LgRYQQeS6EDQF7_6-bMFegNucHjmWg
 * @apiParam {uuid} Identificador de la compañia y se envia por el body.
 * @apiParam {string} name Nombre del usuario y se envia por el body.
 * @apiParam {string} lastname apellido y se envia por el body.
 * @apiParam {string} Password contraseña y se envia por el body. 
 * @apiParam {string} email correo del usuario y se envia por el body.
 * @apiParam {string} cellphone telefono de contacto y se envia por el body.
 * @apiParam {uuid} role_id Identificador del rol y se envia por el body.
 * @apiSuccessExample Success-Response:
 * {
	"response_id": "0696e21b-93e7-4148-a138-0e676423e86d",
	"path": "/v1/companies/6cf86cef-a3ad-4250-93f6-1b1b68a5d10a/userss",
	"method": "post",
	"request": 1529156981.0,
	"msg": "created",
	"status": "ok",
	"service": "ms-users",
	"data": {
		"id": "71fe96e5-5425-4d74-9fd1-ad2c3a48192e",
		"name": "jordann",
		"lastname": "gongora",
		"email": "estebann@hotmail.com",
		"role": "normal",
		"cellphone": "9999999",
		"company": "ByteCode",
		"membership_id": "3afbd9a0-565c-42c6-8848-d96b75949baa",
		"credit": 0,
		"company_id": "6cf86cef-a3ad-4250-93f6-1b1b68a5d10a",
		"company_name": "ByteCode"
	}
}
* @apiErrorExample {json} Error-sin_cabecera:
 * En el caso de no tener serviceid en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property serviceid"
}
* En el caso de no tener content-type en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property content-type"
}
* En el caso de no tener authorization en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property of authorization"
}
* @apiErrorExample {json} Error-serviceid:
 * En el caso de tener un serviceid incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have permits"
}
* @apiErrorExample {json} Error-content-type:
 * En el caso de tener un content-type diferente a application/json:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "you dont have the format correct"
}
* @apiErrorExample {json} Error-authorization:
 * En el caso de tener authorization incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have authorization"
}
@apiErrorExample {json} Error-token:
 *El access_token expira en cierto tiempo y si eso pasa sale este mensaje:
 *{
	"name": "HTTPError",
	"statusCode": 401,
	"status": 401,
	"message": "el Token ya expiró"
}
 */
router.post('/user/:company_id/users', validateRole.verifyAdmin, validateSameRoleCompany.verifySameRoleCompany('company_id'), (req, resp) => {
	var respuesta = requestHttp.requestMethod(req.method, req.body, `${obj.config.MSUSER.host}/v1/companies/${req.params.company_id}/users`, obj.config.MSUSER.serviceid, obj.config.MSUSER.authorization, req.token.company_id).then(msg => {
		resp.send(msg)
	}, err => {
		resp.send(err)
	})
})
/**
 * @api {put} http://206.81.14.204:3000/users/user actualizar usuario
 * @apiName PutUser 
 * @apiGroup User
 * @apiDescription nos permite actualizar informacion del usuario.
 * @apiParam {string} content-type del tipo application/json que se envia por la cabecera
 * @apiParam {string} authorization Token que se envia por la cabecera y es: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJ2YWx1ZSJ9.FG-8UppwHaFp1LgRYQQeS6EDQF7_6-bMFegNucHjmWg
 * @apiParam {uuid} company_id Identificador de la compañia y se envia por el body.
 * @apiParam {uuid} id Identificador del usuario y se envia por el body.
 * @apiParam {string} name Nombre del usuario y se envia por el body.
 * @apiParam {string} lastname Apellido y se envia por el body.
 * @apiParam {string} password Contraseña y se envia por el body.
 * @apiParam {string} email Correo del usuario y se envia por el body.
 * @apiParam {string} cellphone Telefono de contacto y se envia por el body.
 * @apiParam {string} role_id Identificador del rol y se envia por el body.
 * @apiSuccessExample Success-Response:
 * {
	"response_id": "b0ba2d9a-23d1-4059-8ae2-3268e35c4879",
	"path": "/v1/companies/6cf86cef-a3ad-4250-93f6-1b1b68a5d10a/userss/39c7d8ec-e676-46f1-a4d3-bedecec35707",
	"method": "get",
	"request": 1529059948.0,
	"msg": "found",
	"status": "ok",
	"service": "ms-users",
	"data": {
		"id": "39c7d8ec-e676-46f1-a4d3-bedecec35707",
		"name": "jordan",
		"lastname": "gongora",
		"email": "jordang@hotmail.com",
		"role": "",
		"cellphone": "9999999",
		"company": "",
		"membership_id": "",
		"credit": 0,
		"company_id": "",
		"company_name": ""
	}
}
* @apiErrorExample {json} Error-sin_cabecera:
 * En el caso de no tener serviceid en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property serviceid"
}
* En el caso de no tener content-type en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property content-type"
}
* En el caso de no tener authorization en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property of authorization"
}
* @apiErrorExample {json} Error-serviceid:
 * En el caso de tener un serviceid incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have permits"
}
* @apiErrorExample {json} Error-content-type:
 * En el caso de tener un content-type diferente a application/json:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "you dont have the format correct"
}
* @apiErrorExample {json} Error-authorization:
 * En el caso de tener authorization incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have authorization"
}
@apiErrorExample {json} Error-token:
 *El access_token expira en cierto tiempo y si eso pasa sale este mensaje:
 *{
	"name": "HTTPError",
	"statusCode": 401,
	"status": 401,
	"message": "el Token ya expiró"
}
 */
router.put('/user', validateRole.verifyAdmin, (req, resp) => {
	console.log(req.method)
	if (req.token.role !== 'superadmin' && req.body.company_id !== req.token.company_id) {
		return resp.status(403).send({data :{ status : 403, msg: 'No puedes cambiar o ver datos de otra empresa. No tiene autorización para este recurso.'}})
	}
	var respuesta = requestHttp.requestMethod(req.method, req.body, `${obj.config.MSUSER.host}/v1/companies/${req.body.company_id}/users/${req.body.id}`, obj.config.MSUSER.serviceid, obj.config.MSUSER.authorization, req.token.company_id).then(msg => {
		resp.send(msg)
	}, err => {
		resp.send(err)
	})
})
/**
 * @api {get} http://206.81.14.204:3000/users/memberships/idcompany Membresías
 * @apiName GetMemberships
 * @apiGroup User
 * @apiDescription nos permite ver las membresías de una compañia.
 * @apiParam {string} content-type del tipo application/json que se envia por la cabecera
 * @apiParam {string} authorization Token que se envia por la cabecera y es: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJ2YWx1ZSJ9.FG-8UppwHaFp1LgRYQQeS6EDQF7_6-bMFegNucHjmWg
 * @apiParam {uuid} idcompany Identificador de la compañia que se envia por la url.
 * @apiSuccessExample Success-Response:
 * {
	"response_id": "50ae66cb-8c62-44d8-a89a-3c6b32536972",
	"path": "/v1/companies/6cf86cef-a3ad-4250-93f6-1b1b68a5d10a/memberships",
	"method": "get",
	"request": 1529058414.0,
	"msg": "list",
	"status": 200,
	"service": "ms-users",
	"data": {
		"users": [
			{
				"id": "486d6ab2-a9e9-41ed-b3f6-d03f83116293",
				"name": "comodoro_21@hotmail.com",
				"lastname": "gafokebog@mailinator.net",
				"email": "momamaqexu@mailinator.com",
				"role": "normal",
				"cellphone": "kekyd@mailinator.com",
				"company": "ByteCode",
				"membership_id": "0cfbdf8b-cba8-4808-96ae-89620b4712e7",
				"credit": 0
			},
			{
				"id": "b71196ee-d14a-4000-b741-f7717370deae",
				"name": "fypyh@mailinator.net",
				"lastname": "lagerozyj@mailinator.net",
				"email": "cexat@mailinator.com",
				"role": "normal",
				"cellphone": "jygamolyba@mailinator.net",
				"company": "ByteCode",
				"membership_id": "152ae015-ab69-4888-b3be-e0f501913360",
				"credit": 0
            },
	}
	* @apiErrorExample {json} Error-sin_cabecera:
 * En el caso de no tener serviceid en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property serviceid"
}
* En el caso de no tener content-type en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property content-type"
}
* En el caso de no tener authorization en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property of authorization"
}
* @apiErrorExample {json} Error-serviceid:
 * En el caso de tener un serviceid incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have permits"
}
* @apiErrorExample {json} Error-content-type:
 * En el caso de tener un content-type diferente a application/json:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "you dont have the format correct"
}
* @apiErrorExample {json} Error-authorization:
 * En el caso de tener authorization incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have authorization"
}
@apiErrorExample {json} Error-token:
 *El access_token expira en cierto tiempo y si eso pasa sale este mensaje:
 *{
	"name": "HTTPError",
	"statusCode": 401,
	"status": 401,
	"message": "el Token ya expiró"
}
 */
router.get('/memberships/:companyId', validateRole.verifyAdmin, (req, resp) => {
	var respuesta = requestHttp.requestGet2(`${obj.config.MSUSER.host}/v1/companies/${req.params.companyId}/memberships`, obj.config.MSUSER.serviceid, obj.config.MSUSER.authorization, req.token.company_id).then(msg => {
		var body = JSON.parse(msg.body)
		 resp.status(msg.code).send({status: msg.code, data:body.data, pagination: body.pagination})
	}, err => {
		var body = JSON.parse(err.body)
		 resp.status(err.code).send({status: err.code, data:body.data })
	})
})
/**
 * @api {delete} http://206.81.14.204:3000/users/company/idcompany/user/iduser
 * @apiName DeleteUser
 * @apiGroup User
 * @apiDescription Nos permite borrar un usuario.
 * @apiParam {string} content-type del tipo application/json que se envia por la cabecera
 * @apiParam {string} authorization Token que se envia por la cabecera y es: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJ2YWx1ZSJ9.FG-8UppwHaFp1LgRYQQeS6EDQF7_6-bMFegNucHjmWg
 * @apiParam {uuid} idcompany Identificador de la compañia, éste se envia por medio de la url.
 * @apiParam {uuid} iduser Identificador del usuario, éste se envia por medio de la url.
 * @apiSuccessExample Success-Response:
 * No body returned for response
 * * @apiErrorExample {json} Error-sin_cabecera:
 * En el caso de no tener serviceid en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property serviceid"
}
* En el caso de no tener content-type en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property content-type"
}
* En el caso de no tener authorization en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property of authorization"
}
* @apiErrorExample {json} Error-serviceid:
 * En el caso de tener un serviceid incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have permits"
}
* @apiErrorExample {json} Error-content-type:
 * En el caso de tener un content-type diferente a application/json:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "you dont have the format correct"
}
* @apiErrorExample {json} Error-authorization:
 * En el caso de tener authorization incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,

	"status": 412,
	"message": "You dont have authorization"
}
@apiErrorExample {json} Error-token:
 *El access_token expira en cierto tiempo y si eso pasa sale este mensaje:
 *{
	"name": "HTTPError",
	"statusCode": 401,
	"status": 401,
	"message": "el Token ya expiró"
}
 */
router.delete('/company/:companyId/user/:userId', validateRole.verifyAdmin, validateSameRoleCompany.verifySameRoleCompany('companyId'), (req, resp) => {
	var respuesta = requestHttp.requestMethod(req.method, iduser, `${obj.config.MSUSER.host}/v1/companies/${req.params.companyId}/users/${req.params.userId}`, obj.config.MSUSER.serviceid, obj.config.MSUSER.authorization, req.token.company_id).then(msg => {
		resp.send(msg)
	}, err => {
		resp.send(err)
	})
})
/**
 * @api {patch} http://206.81.14.204:3000/users/password Cambiar contreseña.
 * @apiName PatchPassword
 * @apiGroup User
 * @apiDescription Nos permite cambiar la contraseña del usuario.
 * @apiParam {string} content-type del tipo application/json que se envia por la cabecera
 * @apiParam {string} authorization Token que se envia por la cabecera y es: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJ2YWx1ZSJ9.FG-8UppwHaFp1LgRYQQeS6EDQF7_6-bMFegNucHjmWg
 * @apiParam {uuid} company_id Identificador de la compañia.
 * @apiParam {uuid} id Identificador del usuario.
 * @apiParam {string} password contraseña.
 * @apiSuccessExample Success-Response:
 * No body returned for response
 * * @apiErrorExample {json} Error-sin_cabecera:
 * En el caso de no tener serviceid en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property serviceid"
}
* En el caso de no tener content-type en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property content-type"
}
* En el caso de no tener authorization en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property of authorization"
}
* @apiErrorExample {json} Error-serviceid:
 * En el caso de tener un serviceid incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have permits"
}
* @apiErrorExample {json} Error-content-type:
 * En el caso de tener un content-type diferente a application/json:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "you dont have the format correct"
}
* @apiErrorExample {json} Error-authorization:
 * En el caso de tener authorization incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have authorization"
}
@apiErrorExample {json} Error-token:
 *El access_token expira en cierto tiempo y si eso pasa sale este mensaje:
 *{
	"name": "HTTPError",
	"statusCode": 401,
	"status": 401,
	"message": "el Token ya expiró"
}
 */
router.patch('/password', validateRole.verifyNormal, (req, resp) => {
	if (req.token.role !== 'superadmin' && req.body.company_id !== req.token.company_id) {
		return resp.status(403).send({data :{ status : 403, msg: 'No puedes cambiar o ver datos de otra empresa. No tiene autorización para este recurso.'}})
	}
	var respuesta = requestHttp.requestMethod(req.method, req.body.data, `${obj.config.MSUSER.host}/v1/companies/${req.body.company_id}/users/${req.body.id}/password`, obj.config.MSUSER.serviceid, obj.config.MSUSER.authorization, req.token.company_id).then(msg => {
		resp.send(msg)
	}, err => {
		resp.send(err)
	})
})

/**
 * @api {get} http://206.81.14.204:3000/users/company/idcompany/user/iduser Informacion del usuario
 * @apiName GetUser
 * @apiGroup User
 * @apiDescription nos permite ver la informacion de un usuario.
 * @apiParam {string} content-type del tipo application/json que se envia por la cabecera
 * @apiParam {string} authorization Token que se envia por la cabecera y es: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJ2YWx1ZSJ9.FG-8UppwHaFp1LgRYQQeS6EDQF7_6-bMFegNucHjmWg
 * @apiParam {uuid} idcompany Identificador de la compañia, se envia por la url.
 * @apiParam {uuid} iduser Identificador del usuario, se envia por la url.
 * @apiSuccessExample Success-Response:
 * {
	"response_id": "b914d9d0-50bf-47de-8ae7-d5b8e8629874",
	"path": "/v1/companies/6cf86cef-a3ad-4250-93f6-1b1b68a5d10a/userss/39c7d8ec-e676-46f1-a4d3-bedecec35707",
	"method": "get",
	"request": 1529157267.0,
	"msg": "found",
	"status": "ok",
	"service": "ms-users",
	"data": {
		"id": "39c7d8ec-e676-46f1-a4d3-bedecec35707",
		"name": "jordan",
		"lastname": "gongor",
		"email": "jordang@hotmail.com",
		"role": "normal",
		"cellphone": "9999999",
		"company": "ByteCode",
		"membership_id": "18cfc500-cdc1-41e9-99a0-f1f14fb09482",
		"credit": 0,
		"company_id": "6cf86cef-a3ad-4250-93f6-1b1b68a5d10a",
		"company_name": "ByteCode"
	}
}
* @apiErrorExample {json} Error-sin_cabecera:
 * En el caso de no tener serviceid en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property serviceid"
}
* En el caso de no tener content-type en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property content-type"
}
* En el caso de no tener authorization en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property of authorization"
}
* @apiErrorExample {json} Error-serviceid:
 * En el caso de tener un serviceid incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have permits"
}
* @apiErrorExample {json} Error-content-type:
 * En el caso de tener un content-type diferente a application/json:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "you dont have the format correct"
}
* @apiErrorExample {json} Error-authorization:
 * En el caso de tener authorization incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have authorization"
}
@apiErrorExample {json} Error-token:
 *El access_token expira en cierto tiempo y si eso pasa sale este mensaje:
 *{
	"name": "HTTPError",
	"statusCode": 401,
	"status": 401,
	"message": "el Token ya expiró"
}
 */
router.get('/companies/:idCompany/membership/:iduser', validateRole.verifyNormal, validateSameRoleCompany.verifySameRoleCompany('idCompany'), (req, resp, next) => {
	console.log('***************************')
	console.log(`${obj.config.MSUSER.host}/v1/companies/${req.params.idCompany}/memberships/${req.params.iduser}`)
	console.log('***************************')
	var respuesta = requestHttp.requestGet2(`${obj.config.MSUSER.host}/v1/companies/${req.params.idCompany}/memberships/${req.params.iduser}`, obj.config.MSUSER.serviceid, obj.config.MSUSER.authorization, req.token.company_id).then(msg => {
		try{
			var body = JSON.parse(msg.body)
			resp.status(msg.code).send(body.data)
		}
		catch(err){
			console.log('Entro a catch')
		}
	}).catch(err =>{
		var body = JSON.parse(err.body)
		resp.status(err.code).send({status: err.code, data:body.data || body})
	})
})


router.get('/companies/:company_id/memberships', validateRole.verifyAdmin, validateSameRoleCompany.verifySameRoleCompany('company_id'), (req, resp) => {
	let queries = `?page=${req.query.page || 0}&per_page=${req.query.per_page || 0}`

	if (req.query !== 'undefined' && req.query.q) {
		queries += `&q=${req.query.q}`
	}
	if (req.query !== 'undefined' && req.query.sort) {
		queries += `&sort=${req.query.sort}`
	}
	console.log(`${obj.config.MSCOMPANY.host}/v1/companies/${req.params.company_id}/memberships/${queries}`)
	requestHttp.requestGet2(`${obj.config.MSCOMPANY.host}/v1/companies/${req.params.company_id}/memberships/${queries}`, obj.config.MSUSER.serviceid, obj.config.MSUSER.authorization, req.token.company_id).then(msg => {
		var body = JSON.parse(msg.body)
		resp.status(msg.code).send({status: msg.code, data:body.data.data, pagination: body.pagination})
	}).catch(err => {
		var body = JSON.parse(err.body)
		resp.status(err.code).send({status: err.code, data:body.data || body})
	})

})


router.post('/superadmin', validateRole.verifyAdmin, validateSameRoleCompany.verifySameRoleCompany(''), (req, resp) => {
	console.log('superadmin')
	var respuesta = requestHttp.requestMethod(req.method, req.body, `${obj.config.MSUSER.host}/v1/users/superadmin`, obj.config.MSUSER.serviceid, obj.config.MSUSER.authorization, '').then(msg => {
		var body = JSON.parse(msg.body)
		resp.status(msg.code).send({status: msg.code, data:body.data, pagination: body.pagination})
	}).catch(err => {
		var body = JSON.parse(err.body)
		resp.status(err.code).send({status: err.code, data:body.data || body})
	})
})

module.exports = router;