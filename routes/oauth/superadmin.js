var express = require('express');
var HTTPError = require('node-http-error');
var router = express.Router();
var request = require('request');
var requestify = require('requestify');
const obj = require('config-yml');
var requestHttp = require('../../utilities/requestHttp')




/**
 * @api {post} http://206.81.14.204:3000/auth/token Iniciar Sesion
 * @apiName PostToken
 * @apiGroup Login
 * @apiDescription Nos permite iniciar sesion.
 * @apiParam {string} content-type del tipo application/json que se envia por la cabecera
 * @apiParam {string} grant_type tipo de acceso: 'password'
 * @apiParam {string} email Correo de la usuario.
 * @apiParam {string} password Contraseña del usuario.
 * @apiSuccessExample Success-Response:
 * {
	"response_id": "e70efa7e-6e10-4bea-8e87-7bd20efbc013",
	"path": "/v1/oauth/token",
	"method": "post",
	"request": 1529720833.0,
	"msg": "token created",
	"status": "ok",
	"service": "ms-auth",
	"data": {
		"access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzUxMiJ9.eyJjb21wYW55X2lkIjoiNmNmODZjZWYtYTNhZC00MjUwLTkzZjYtMWIxYjY4YTVkMTBhIiwibWVtYmVyc2hpcF9pZCI6IjJmNWQzMmExLWY0NGUtNDhiMC1hYTU0LWZhZDcyMmFmZWM4MSIsInVzZXJfaWQiOiI2Y2MyZGQwNi04ZDZmLTRmYjktOTJhNy0wMGU1YjI4ZjZkZGYiLCJyb2xlIjoibm9ybWFsIiwiY29tcGFueV9uYW1lIjoiQnl0ZUNvZGUiLCJleHAiOjE1Mjk3MjE3MzMsImlhdCI6MTUyOTcyMDgzMywiYXVkIjoic21hcnRzZW5kIiwiaXNzIjoiIn0.CkVmX-ABTpUAd9KW6TbUCs5Qg0UvNOM1B7pcCM19FiO_81t_5V_PuPvQ5l1e0jldTRpmRCa74pjQOx2NeYhO2yO9QH6vFRWkaFBxV4_JyRbuO5GzuzUMkMi5PLMdjO-pXr0A4e5ie_gSUoefMRuTFDRgY0kiuLqdogxj2r-8uIMl3spceI0jaGg9Hd6JvBMcA4Es1E2zQ4zjIMt4GCqY262zJNw39g0PWDiBtiNFivAfb3HQvPUgv0DPd8lb9h4l7DSsyZtMqGkNd59t9t2Rk4dFeW65OkMevNPMp3UJ4X5QgIgJBJyXHRRPMMqzIVbfr9O8_RSMBpJilfX9VT9NghC2Fu--t86nyK8OcjTzPEb0fBdfUejr6Jv7_Me7mPCkRo-s3rFI_x_nwOTlzjOB49V70QrjlEwUQh5uCubM0kwqzmBUFsHoZv_b0jeEnaLsnT4VjhKTo6NKNjFq8-MzT8_QIoFSl5EULpsYd2i8vKpW4Q49pfiSNr_nXF0bp6xKcKxPd2iUjmGmjBFKadBcqO4fjwcKAY1Jd5oFml3TlHuLMjhgnW8PSHouTvzJN9YA0MV-JruCvJ-RHNAv4OXP3U8-cmIJQ3lfL6-zaq_71PLi1pZz4mQ50NLgOwaekBbNEm0wxgHLcjHbn91SjZNdidYz65zKIiG2pIZ3am2Q94A",
		"refresh_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzUxMiJ9.eyJtZW1iZXJzaGlwX2lkIjoiMmY1ZDMyYTEtZjQ0ZS00OGIwLWFhNTQtZmFkNzIyYWZlYzgxIiwiZXhwIjoxNTI5ODkzNjMzLCJpYXQiOjE1Mjk3MjA4MzMsImF1ZCI6InNtYXJ0c2VuZCIsImlzcyI6IiJ9.AmeK0eQxGh_q3kwoCnRmA6_rESSn4o8s2amHc5FlIld7mL3PtmUsB12HS3Yr3F1tTWcSp1TukvzFTogxjLEkx1WOurND8D9CJ0M52qhbvu2dmgpSDipK6c79YnK7NRfDrf2KWtT5MPKzitMDr4v4CKbaI8mZEfh0v6iPmP76Wt81GPgJpeB0_xyWxNxH3sKzEUSMiBxGA4CGz7m2mZcumApiduhgOz3Uehqybl5R94zs4b3ifFs-9XdwzXWXJjeXVvwrs1tFuafGt3AqEKXk9Wic-RnG8UoZc-R2c9wTUQUmVAB8Aucmmp_n39PJUGlL4GCJcLQJHEqOAYmHRm8I7xJ7Y5ss7zkvnuVdP339I7rFzjLdh5-Gj34o7sfN3QpOpxw9CPvPf2RXyIqAwn7Iy5FdDzYSrWF3cV9PXVlOKBEkjBT7KybFM4OAhgSMlJnJC3dD3neT0lj5o2wSwXDG7lyS4_XzzDmmWquHlIRR6b-Vhuk8RmUuU0rGHERecP04Tq6GQr0VV-ejHSBM9wpZPE5mjZNMiQTcuM30aR1-ScmWKGbLKq5N4lFb9jvv2vHjeSecSTaIOcsifvtZvwPO3eBj6w9jxk4Rq1DTjKSgeRBc2TyDmndeYDQhbjHhCq3TaSv0qnZOSGtrmhKaei7Hw4ts2pSlYJvrsrq5-bi5-oQ",
		"membership_id": "2f5d32a1-f44e-48b0-aa54-fad722afec81",
		"company_id": "6cf86cef-a3ad-4250-93f6-1b1b68a5d10a",
		"user_id": "6cc2dd06-8d6f-4fb9-92a7-00e5b28f6ddf",
		"token_type": "JWT TOKEN",
		"issued_at": 1529720833,
		"expires_in": 1529721733,
		"refresh_token_expires_in": 1529893633,
		"company_name": "ByteCode",
		"role": "normal",
		"email": "shotokan@hotmail.com"
	}
}
* @apiErrorExample {json} Error-sin_cabecera:
 * En el caso de no tener serviceid en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property serviceid"
}
* En el caso de no tener content-type en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property content-type"
}
* @apiErrorExample {json} Error-serviceid:
 * En el caso de tener un serviceid incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have permits"
}
* @apiErrorExample {json} Error-content-type:
 * En el caso de tener un content-type diferente a application/json:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "you dont have the format correct"
}

* @apiErrorExample {json} Error-credenciales:
	Si el usuario ingresa credenciales incorrectas.
	{
	"code": 401,
	"headers": {
		"server": "gunicorn/19.8.1",
		"date": "Wed, 04 Jul 2018 15:52:17 GMT",
		"connection": "close",
		"content-length": "245",
		"content-type": "application/json; charset=UTF-8"
	},
	"body": "{\"response_id\": \"5b30b357-6cab-4c8b-a1bc-b249bd93cd61\", \"path\": \"/v1/oauth/token\", \"method\": \"post\", \"request\": 1530719537.0, \"msg\": \"authentication failed\", \"status\": \"error\", \"service\": \"ms-auth\", \"data\": {\"error\": \"Credenciales incorrectas\"}}"
}
* @apiErrorExample {json} Error-grant_type:
si el grant_type no es password, se manda el siguiente mensaje:
{
	"code": 401,
	"headers": {
		"server": "gunicorn/19.8.1",
		"date": "Wed, 04 Jul 2018 15:52:43 GMT",
		"connection": "close",
		"content-length": "242",
		"content-type": "application/json; charset=UTF-8"
	},
	"body": "{\"response_id\": \"b2e1c1df-8380-4ba4-91ac-92822bc677be\", \"path\": \"/v1/oauth/token\", \"method\": \"post\", \"request\": 1530719563.0, \"msg\": \"authentication failed\", \"status\": \"error\", \"service\": \"ms-auth\", \"data\": {\"error\": \"grant_type incorrecto\"}}"
}
 */
router.post('/auth/token', (req, resp) => {
	console.log("entro")
	requestHttp.requestMethod(req.method, req.body, `${obj.config.MSAUTH.host}/v1/superadmins/oauth/token`, obj.config.MSAUTH.serviceid, obj.config.MSAUTH.authorization, '').then(res => {
		
		var body = JSON.parse(res.body)
		resp.status(res.code).send({status: res.code, data:body.data })
	}, err => {
		var body = JSON.parse(err.body)
		resp.status(err.code).send({status: err.code, data:body.data })
	})
})

/**
 * @api {post} http://206.81.14.204:3000/auth/refresh refrescar el token
 * @apiName PostRefresh
 * @apiGroup Login
 * @apiDescription Nos permite refrescar el token de inicio de sesion.
 * @apiParam {string} content-type del tipo application/json que se envia por la cabecera
 * @apiParam {string} authorization token que se envia por la cabecera y es: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJ2YWx1ZSJ9.FG-8UppwHaFp1LgRYQQeS6EDQF7_6-bMFegNucHjmWg
 * @apiParam {string} grant_type tipo de acceso: 'refresh_token'.
 * @apiParam {string} refresh_token  token.
 * @apiSuccessExample Success-Response:
 * {
	"response_id": "6e8e6f37-5a31-4532-831f-43172e8466a7",
	"path": "/v1/oauth/token/refresh",
	"method": "post",
	"request": 1529721319.0,
	"msg": "token created",
	"status": "ok",
	"service": "ms-auth",
	"data": {
		"access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzUxMiJ9.eyJjb21wYW55X2lkIjoiNmNmODZjZWYtYTNhZC00MjUwLTkzZjYtMWIxYjY4YTVkMTBhIiwibWVtYmVyc2hpcF9pZCI6IjJmNWQzMmExLWY0NGUtNDhiMC1hYTU0LWZhZDcyMmFmZWM4MSIsInVzZXJfaWQiOiI2Y2MyZGQwNi04ZDZmLTRmYjktOTJhNy0wMGU1YjI4ZjZkZGYiLCJyb2xlIjoibm9ybWFsIiwiY29tcGFueV9uYW1lIjoiQnl0ZUNvZGUiLCJleHAiOjE1Mjk3MjIyMTksImlhdCI6MTUyOTcyMTMxOSwiYXVkIjoic21hcnRzZW5kIiwiaXNzIjoiIn0.e7dRgpSWYfWaQgXee1I_WYBiEizuzOOA2M2LFyd0Z9PNhnLLqEhRXT7Pu-QG6_ixyBFnsmKixRfIGx6ZRBJOOb97v4QnyTh78uY_j4FKSQ_ph-pS_lMcydXMPjBJ5RWKTbts_uIX860Ersu8XACAwCq3mNyVWK8IcY7aVFbfkgfkmNqUGoN_Dk_QVQvXMfwZheoyKVXjkjtCK3FIIpJVzfxX1XpkjN3KnnP3h5p6cBUZWigNnp_OKwhveBEcluuH-rGhEhrIHeSx7LSEBcnSRcxBOO4X5Otf2-UD4Pu8714kP7Fz1uqz0Wr7bIrp0jYEX8RJ7__GFxB-sIFVc5TXuj1quuWhtwd1yUsZnv-o3TQTgP8N7yldH6puFrQ7-bwl61HBhQJuPsPIo2XSGM9rblvRCuNOzuNKiWkH-AMeT6HrvtBCO7TmjyUO_Kk0vCCoZR-McGM9ap0Owf1MtfLcUXGkmxLkETPeRKpevX4fB29z3_76ZrvkIQ9PBYL3GSOZTDwUQpjR3JVpOiz93f01TO80LAuHwlpZ_CTgjB4k_4-YbLoB1TxXd6oNNUZn3QI5P9xk2roAk932MCxKPLhH9I-V8fERj6_Ijbjv7KQgzZjA5M-oeq59CJHSXhzZo9sOVkQcN_1IDxlZrQIcIIOF2uLdfJF6JrVskCUqE-2kX6M",
		"refresh_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzUxMiJ9.eyJtZW1iZXJzaGlwX2lkIjoiMmY1ZDMyYTEtZjQ0ZS00OGIwLWFhNTQtZmFkNzIyYWZlYzgxIiwiZXhwIjoxNTI5ODk0MTE5LCJpYXQiOjE1Mjk3MjEzMTksImF1ZCI6InNtYXJ0c2VuZCIsImlzcyI6IiJ9.Vq52EqNtV7EgosgjsShlz5vlPMNN2Xf-yGVz8XfaAUy8ywBiPiAgoEp9Q86WkrpfARPczjUKNKZSwmBNb0OfJmQw8kADT5678JylIc_57LMbP_urqR0coxpZ8UQgw8sNP8z5TA51Vfi97LD2B3QbdZIOnlnQcvlvd9JuMr6enB5UExGEnFK6L41LBdaJ91-vjWTYvyWSUXamlkQU-d7lCqZI_vSxuhDPR6fY1TJ3VakGziYBtoP_e31tbkV7_C67aWcEIXb3h-u4munLn2RjkTZ7umLYWosxvcTjgshWMAqcGNlYimhJAeJnDDCcosRbLJgCzmAliPocSPlZhIqRclq3DcsimnxsXD8gPAniDUEWm7vAyIvDdMqD7PrqfiJW1XTARMi4YbYEXUfEpmr-60TMaMn8798LJ3W-Q4pymbfsbBVYO0z6iH7_PRIWzGSjlHFUjSjIj70p9Yo7XSMdKxU3rb3FJC4jANJZLlpOi4wU60z0YecbRtMhGOqTUnUuKdULU45fPq5WQ9tUAl3ZxqaOtUsJHYZoUrPRWtIoRGNVzKEWEHZlu9ZDkfRwmudPP9PW1wyX6M52FMoFApoBTqruxWvKNHeT9CcrzEVRpoefRzPmoLaosajlQAvy0kbPt_bdj7UaOvIYZAVdxrxVc2JsYyoF7pFTjxkK28x4Gho",
		"membership_id": "2f5d32a1-f44e-48b0-aa54-fad722afec81",
		"company_id": "6cf86cef-a3ad-4250-93f6-1b1b68a5d10a",
		"user_id": "6cc2dd06-8d6f-4fb9-92a7-00e5b28f6ddf",
		"token_type": "JWT TOKEN",
		"issued_at": 1529721319,
		"expires_in": 1529722219,
		"refresh_token_expires_in": 1529894119,
		"company_name": "ByteCode",
		"role": "normal",
		"email": "shotokan@hotmail.com"
	}
}
* @apiErrorExample {json} Error-sin_cabecera:
 * En el caso de no tener serviceid en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property serviceid"
}
* En el caso de no tener content-type en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property content-type"
}
* En el caso de no tener authorization en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property of authorization"
}
* @apiErrorExample {json} Error-serviceid:
 * En el caso de tener un serviceid incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have permits"
}
* @apiErrorExample {json} Error-content-type:
 * En el caso de tener un content-type diferente a application/json:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "you dont have the format correct"
}
* @apiErrorExample {json} Error-authorization:
 * En el caso de tener authorization incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have authorization"
}
* @apiErrorExample {json} Error-grant_type:
	si el grant_type no es refresh_token, se manda el mensaje de error:
	{
	"code": 401,
	"headers": {
		"server": "gunicorn/19.8.1",
		"date": "Wed, 04 Jul 2018 15:55:13 GMT",
		"connection": "close",
		"content-length": "242",
		"content-type": "application/json; charset=UTF-8"
	},
	"body": "{\"response_id\": \"2b719f96-b6b0-4da1-939d-d3779b22e4a5\", \"path\": \"/v1/oauth/token\", \"method\": \"post\", \"request\": 1530719713.0, \"msg\": \"authentication failed\", \"status\": \"error\", \"service\": \"ms-auth\", \"data\": {\"error\": \"grant_type incorrecto\"}}"
}

+ @apiErrorExample {json} Error-token:
	Si el token ya se refrescó o ya expiró manda el siguiente mensaje:
	{
	"code": 401,
	"headers": {
		"server": "gunicorn/19.8.1",
		"date": "Wed, 04 Jul 2018 16:16:27 GMT",
		"connection": "close",
		"content-length": "301",
		"content-type": "application/json; charset=UTF-8"
	},
	"body": "{\"response_id\": \"dff62d3c-8e40-4a03-9f31-24aabfd40886\", \"path\": \"/v1/oauth/token/refresh\", \"method\": \"post\", \"request\": 1530720987.0, \"msg\": \"authentication failed\", \"status\": \"error\", \"service\": \"ms-auth\", \"data\": {\"error\": \"No se puede actualizar el token. Puede que ya no exista o haya expirado.\"}}"
}
 */
router.post('/auth/refresh', (req, resp) => {
	var respuesta = requestHttp.requestMethod(req.method, req.body, `${obj.config.MSAUTH.host}/v1/superadmins/oauth/token/refresh`, obj.config.MSAUTH.serviceid, obj.config.MSAUTH.authorization, '').then(msg => {
		var body = JSON.parse(msg.body)
		resp.status(msg.code).send({data:body.data })
	}, err => {
		var body = JSON.parse(err.body)
		resp.status(err.code).send({data:body.data })
	})
})



module.exports = router;
