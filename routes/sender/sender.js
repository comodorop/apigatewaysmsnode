var express = require('express');
var router = express.Router();
var aws = require('../../aws')
var multipart = require("connect-multiparty");
var multipartMiddleware = multipart();
var objUtilerias = require('../../utilities/images')
var fs = require("fs");
var moment = require("moment")
var validateRole = require('../../middlewares/roleValidation')




/**
 * @api {post}http://206.81.14.204:3000/sender/upload Agregar una categoria
 * @apiName PostUpload
 * @apiGroup Sender
 * @apiDescription Nos permite subir un archivo.
 * @apiParam {string} content-type Del tipo application/json que se envia por la cabecera.
 * @apiParam {string} authorization Token que se envia por la cabecera y es: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJ2YWx1ZSJ9.FG-8UppwHaFp1LgRYQQeS6EDQF7_6-bMFegNucHjmWg
 * @apiParam {uuid} company_id id de la compañia y se envia por el body.
 * @apiParam {uuid} user_id id del usuario y se envia por el body.
 * @apiParam {image} photo archivo que se subirá y se envia por el body.
 * @apiParam {string} message Es un mensaje y se envia por el body.
 * 
 * @apiSuccessExample Success-Response:
 * {
	"status": "200",
	"msg": "New register",
	"data": []
   }
   * @apiErrorExample {json} Error-sin_cabecera:
 * En el caso de no tener serviceid en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property serviceid"
}
* En el caso de no tener content-type en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property content-type"
}
* En el caso de no tener authorization en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property of authorization"
}
* @apiErrorExample {json} Error-serviceid:
 * En el caso de tener un serviceid incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have permits"
}
* @apiErrorExample {json} Error-content-type:
 * En el caso de tener un content-type diferente a application/json:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "you dont have the format correct"
}
* @apiErrorExample {json} Error-authorization:
 * En el caso de tener authorization incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have authorization"
}
@apiErrorExample {json} Error-token:
 *El access_token expira en cierto tiempo y si eso pasa sale este mensaje:
 *{
	"name": "HTTPError",
	"statusCode": 401,
	"status": 401,
	"message": "el Token ya expiró"
}
 */
router.post('/upload', validateRole.verifyNormal, multipartMiddleware, function (req, res, next) {
    var params = req.body;
    var rutaFinal = "./files/"
    var file = req.files.file
    var infoFile = req.files.file.name.split(".")
    var extension = infoFile[1]
    if(params.send_date === 'Invalid date'){
        params.send_date = moment().format('YYYY-MM-DDThh:mm');
    }
    if (extension === 'xlsx' || extension === 'xls') {
        var path = file.path
        fs.exists(rutaFinal, function (exists) {
            if (exists) {
                var archivo = objUtilerias.getFirstFileName(file.name)
                rutaFinal += archivo
            }
            params.photo = archivo
            fs.createReadStream(path).pipe(fs.createWriteStream(rutaFinal), function () {
                fs.unlink(path, function () {
                    if (error) {
                        throw error
                    } else {
                        console.log("creo el archivo correctamente")
                    }
                });
            });
            var objData = {
                "dir": rutaFinal,
                "name": archivo
            }
            aws.upload(objData).then(msg => {
                var objRabbit = {
                    company_id: params.company_id,
                    user_id: params.user_id,
                    file_name: archivo,
                    type: extension,
                    send_date: params.send_date,
                    priority: "now",
                    message: params.message,
                    original_name: file.name,
                    title: params.title
                };
                console.log(objRabbit)
                channelWrapper.publish("sms_events", "files", objRabbit, {
                    headers: {
                        x_event_name: "excel"
                    },
                    persistent: true
                }).then(function () {
                    res.send(msg)
                }).catch(err => {
                    channelWrapper.close();
                    connection.close();
                });
            }, err => {
                res.send(err)
            })
        });
    } else {
        res.status(400).send({status:400, msg:"Se requiere un formato adecuado xlsx o xls"})

    }
})



router.post('/sendContacts', validateRole.verifyNormal, (req, res) => {
    var params = req.body

    
    console.log(params)
    channelWrapper.publish("sms_events_contacts", "contacts", params, {
        headers: {
            x_event_name: "sms-contacts"
        }, persistent: true
    }).then(() => {
        res.status(200).send({
            status: 200,
            msg: "New Register"
        })
    }).catch(err => {
        channelWrapper.close()
    })
})






module.exports = router;