var express = require('express');
var router = express.Router();
var request = require('request');
var requestify = require('requestify');
const obj = require('config-yml');
var requestHttp = require('../../utilities/requestHttp')
var validateRole = require('../../middlewares/roleValidation')
var validateSameRoleCompany = require('../../middlewares/sameCompany')

var id = ''
/**
 * @api {post}http://206.81.14.204:3000/companys/company Crear una compañia.
 * @apiName PostCompany
 * @apiGroup Company
 * @apiDescription Estos son los campos que se necesitan son para crear una compañia exitosamente
 * @apiParam {string} content-type Del tipo application/json que se envia por la cabecera.
 * @apiParam {string} authorization Que inicia con Bearer seguido de un token que se envia por la cabecera.
 * @apiParam {string} name Nombre de la compañia y se envia por el body.
 * @apiParam {string} rfc rfc de la compañia y se envia por el body.
 * @apiParam {string} email_contact Correo de contacto de la compañia y se envia por el body.
 * @apiParam {string} phone El numero teléfonico de la compañia y se envia por el body.
 * @apiParam {int} credit Credito de la compañia y se envia por el body.
 * @apiSuccessExample Success-Response:
 * {
	"response_id": "d268ddfa-b54b-4a9b-b86c-2b0048fec0f5",
	"path": "/v1/companies",
	"method": "post",
	"request": 1529057590.0,
	"msg": "created",
	"status": 201,
	"service": "ms-users",
	"data": {
		"id": "5dadb518-c2e8-4caf-8c79-afbc2a057176",
		"name": "Byt3Code_2",
		"rfc": "asdasdas",
		"email_contact": "isc_86@hotmail.com",
		"credits": 1000,
		"phone": "9999999"
	}
}
* @apiErrorExample {json} Error-sin_cabecera:
 * En el caso de no tener serviceid en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property serviceid"
}
* En el caso de no tener content-type en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property content-type"
}
* En el caso de no tener authorization en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property of authorization"
}
* @apiErrorExample {json} Error-serviceid:
 * En el caso de tener un serviceid incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have permits"
}
* @apiErrorExample {json} Error-content-type:
 * En el caso de tener un content-type diferente a application/json:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "you dont have the format correct"
}
* @apiErrorExample {json} Error-authorization:
 * En el caso de tener authorization incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have authorization"
}
@apiErrorExample {json} Error-token:
 *El access_token expira en cierto tiempo y si eso pasa sale este mensaje:
 *{
	"name": "HTTPError",
	"statusCode": 401,
	"status": 401,
	"message": "el Token ya expiró"
}
 */
router.post('/company', validateRole.verifySuperAdmin, (req, resp) => {
	console.log(`${obj.config.MSCOMPANY.host}/v1/companies`)
	var respuesta = requestHttp.requestMethod(req.method, req.body, `${obj.config.MSCOMPANY.host}/v1/companies`, obj.config.MSCOMPANY.serviceid, obj.config.MSCOMPANY.authorization, '').then(msg => {
		var body = JSON.parse(msg.body)
		resp.status(msg.code).send({status: msg.code, data:body.data })
	}, err => {
		var body = JSON.parse(err.body)
		resp.status(err.code).send({status: err.code, data:body.data || body})
	})
})
/**
 * @api {get}http://206.81.14.204:3000/companys/company/:id Mostrar informacion de la compañia
 * @apiName GetCompany
 * @apiGroup Company
 * @apiDescription Esta api muestra informacion de la compañia solicitando el siguiente parametro.
 * @apiParam {string} content-type Del tipo application/json que se envia por la cabecera.
 * @apiParam {string} authorization Que inicia con Bearer seguido de un token que se envia por la cabecera.
 * @apiParam {uuid} id Identificador de la compañia y se envia por la url.
 * @apiSuccesExample Success-Response:
 * {
	"response_id": "48d4e434-fb0d-4dc6-b34e-410ea76910bb",
	"path": "/v1/companies/5dadb518-c2e8-4caf-8c79-afbc2a057176",
	"method": "get",
	"request": 1529124141.0,
	"msg": "entity",
	"status": "ok",
	"service": "ms-users",
	"data": {
		"id": "5dadb518-c2e8-4caf-8c79-afbc2a057176",
		"name": "Byt3Code_2",
		"rfc": "asdasdas",
		"email_contact": "isc_86@hotmail.com",
		"credits": 1000,
		"phone": "9999999"
	}
}
* @apiErrorExample {json} Error-sin_cabecera:
 * En el caso de no tener serviceid en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property serviceid"
}
* En el caso de no tener content-type en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property content-type"
}
* En el caso de no tener authorization en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property of authorization"
}
* @apiErrorExample {json} Error-serviceid:
 * En el caso de tener un serviceid incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have permits"
}
* @apiErrorExample {json} Error-content-type:
 * En el caso de tener un content-type diferente a application/json:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "you dont have the format correct"
}
* @apiErrorExample {json} Error-authorization:
 * En el caso de tener authorization incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have authorization"
}
* @apiErrorExample {json} Error-id:
 * Si el id de la compañia no es correcta:
 * {
	"code": 500,
	"headers": {
		"connection": "close",
		"content-type": "text/html",
		"content-length": "141"
	},
	"body": "<html>\n  <head>\n    <title>Internal Server Error</title>\n  </head>\n  <body>\n    <h1><p>Internal Server Error</p></h1>\n    \n  </body>\n</html>\n"
}
@apiErrorExample {json} Error-token:
 *El access_token expira en cierto tiempo y si eso pasa sale este mensaje:
 *{
	"name": "HTTPError",
	"statusCode": 401,
	"status": 401,
	"message": "el Token ya expiró"
}
 */
router.get('/company/:id', validateRole.verifyAdmin, validateSameRoleCompany.verifySameRoleCompany('id'), (req, resp) => {
	let respuesta = requestHttp.requestGet2(`${obj.config.MSCOMPANY.host}/v1/companies/${req.params.id}`, obj.config.MSCOMPANY.serviceid, obj.config.MSCOMPANY.authorization, '').then(msg => {
		var body = JSON.parse(msg.body)
		resp.status(msg.code).send({status: msg.code, data:body.data })
	}, err => {
		var body = JSON.parse(err.body)
		resp.status(err.code).send({status: err.code, data:body.data || body})
	})
})

/**
 * @api {delete}http://206.81.14.204:3000/companys/company/:id Borrar compañia
 * @apiName DeleteCompany
 * @apiGroup Company
 * @apiDescription Esta api es para borrar una compañia con el siguiente parámetro.
 * @apiParam {string} content-type Del tipo application/json que se envia por la cabecera.
 * @apiParam {string} authorization Que inicia con Bearer seguido de un token que se envia por la cabecera.
 * @apiParam {uuid} id Identificador de la compañia y se envia por la url.
 * @apiSuccesExample Success-Response:
 * * @apiErrorExample {json} Error-sin_cabecera:
 * En el caso de no tener serviceid en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property serviceid"
}
* En el caso de no tener content-type en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property content-type"
}
* En el caso de no tener authorization en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property of authorization"
}
* @apiErrorExample {json} Error-serviceid:
 * En el caso de tener un serviceid incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have permits"
}
* @apiErrorExample {json} Error-content-type:
 * En el caso de tener un content-type diferente a application/json:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "you dont have the format correct"
}
* @apiErrorExample {json} Error-authorization:
 * En el caso de tener authorization incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have authorization"
}
@apiErrorExample {json} Error-token:
 *El access_token expira en cierto tiempo y si eso pasa sale este mensaje:
 *{
	"name": "HTTPError",
	"statusCode": 401,
	"status": 401,
	"message": "el Token ya expiró"
}
 */
router.delete('/company/:id', validateRole.verifySuperAdmin, validateSameRoleCompany.verifySameRoleCompany('id'), (req, resp) => {
	
	id = req.params.id
	console.log(`${obj.config.MSCOMPANY.host}/v1/companies/${id}`)
	var respuesta = requestHttp.requestMethod(req.method, req.body, `${obj.config.MSCOMPANY.host}/v1/companies/${id}`, obj.config.MSCOMPANY.serviceid, obj.config.MSCOMPANY.authorization, '').then(msg => {
		resp.status(msg.code).send({})
	}).catch(err => {
		var body = JSON.parse(err.body)
		resp.status(err.code).send({status: err.code, data:body.data || body })
	})
})

/**
 * @api {put}http://206.81.14.204:3000/companys/company Actualizar compañia
 * @apiName PutCompany
 * @apiGroup Company
 * @apiDescription Esta api requiere de los siguientes parámetros:
 * @apiParam {string} content-type Del tipo application/json que se envia por la cabecera.
 * @apiParam {string} authorization Que inicia con Bearer seguido de un token que se envia por la cabecera.
 * @apiParam {uuid} company_id Identificador de la compañia y se envia por el body.
 * @apiParam {string} name Nombre de la compañia y se envia por el body. 
 * @apiParam {string} rfc rfc de la compañia y se envia por el body. 
 * @apiParam {string} email_contact Correo de contacto de la compañia y se envia por el body. 
 * @apiParam {int} credits Credito de la compañia y se envia por el body. 
 * @apiSuccesExample Success-Response:
 * * @apiErrorExample {json} Error-sin_cabecera:
 * En el caso de no tener serviceid en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property serviceid"
}
* En el caso de no tener content-type en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property content-type"
}
* En el caso de no tener authorization en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property of authorization"
}
* @apiErrorExample {json} Error-serviceid:
 * En el caso de tener un serviceid incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have permits"
}
* @apiErrorExample {json} Error-content-type:
 * En el caso de tener un content-type diferente a application/json:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "you dont have the format correct"
}
* @apiErrorExample {json} Error-authorization:
 * En el caso de tener authorization incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have authorization"
}
@apiErrorExample {json} Error-token:
 *El access_token expira en cierto tiempo y si eso pasa sale este mensaje:
 *{
	"name": "HTTPError",
	"statusCode": 401,
	"status": 401,
	"message": "el Token ya expiró"
}
 */
router.put('/:id', validateRole.verifyAdmin, validateSameRoleCompany.verifySameRoleCompany('id'), (req, resp) => {
	console.log(`${obj.config.MSCOMPANY.host}/v1/companies/${req.params.id}`)
	var respuesta = requestHttp.requestMethod(req.method, req.body, `${obj.config.MSCOMPANY.host}/v1/companies/${req.params.id}`, obj.config.MSCOMPANY.serviceid, obj.config.MSCOMPANY.authorization, req.token.company_id || '').then(msg => {
		console.log(msg)
		var body = JSON.parse(msg.body)
		resp.status(msg.code).send({status: msg.code, data:body.data })
	}, err => {
		var body = JSON.parse(err.body)
		resp.status(err.code).send({status: err.code, data:body.data || body })
	})
})

/**
 * @api {post}http://206.81.14.204:3000/v1/companies/:id/categories Agregar una categoria
 * @apiName PostCategory
 * @apiGroup Category
 * @apiDescription Esta api nos permite insertar una categoria por compania.
 * @apiParam {string} content-type Del tipo application/json que se envia por la cabecera.
 * @apiParam {string} authorization Token que se envia por la cabecera y es: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJ2YWx1ZSJ9.FG-8UppwHaFp1LgRYQQeS6EDQF7_6-bMFegNucHjmWg
 * @apiParam {string} Nombre Nombre de la categoria y se envia por el body.
 * @apiParam {string} Description Descripción de la categoria y se envia por el body.
 * @apiSuccessExample Success-Response:
 * 
	{
    "response_id": "5d4da980-798c-47b4-9c0c-16522b20d04b",
    "path": "/v1/companies/6cf86cef-a3ad-4250-93f6-1b1b68a5d10a/categories",
    "method": "post",
    "request": 1533317173,
    "msg": "create new category",
    "api-version": "v1",
    "status": "ok",
    "service": "MS-SMS",
    "data": {
        "company_id": "6cf86cef-a3ad-4250-93f6-1b1b68a5d10a",
        "id": "486fb107-63d8-4125-b2ef-2191098dac28",
        "description": "",
        "name": "otras"
    }
}

}
   * @apiErrorExample {json} Error-sin_cabecera:
 * En el caso de no tener serviceid en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property serviceid"
}
* En el caso de no tener content-type en la cabecera:PostCategory
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property content-type"
}
* En el caso de no tener authorization en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property of authorization"
}
* @apiErrorExample {json} Error-serviceid:
 * En el caso de tener un serviceid incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have permits"
}
* @apiErrorExample {json} Error-content-type:
 * En el caso de tener un content-type diferente a application/json:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "you dont have the format correct"
}
* @apiErrorExample {json} Error-authorization:
 * En el caso de tener authorization incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have authorization"
}
@apiErrorExample {json} Error-token:
 *El access_token expira en cierto tiempo y si eso pasa sale este mensaje:
 *{
	"name": "HTTPError",
	"statusCode": 401,
	"status": 401,
	"message": "el Token ya expiró"
}
 */
router.post('/:id/categories', validateRole.verifyNormal, validateSameRoleCompany.verifySameRoleCompany('id'), (req, resp) => {
	console.log(`${obj.config.MSCATEGORY.host}/v1/companies/${id}/categories`)
	var id = req.params.id
	requestHttp.requestMethod(req.method, req.body, `${obj.config.MSCATEGORY.host}/v1/companies/${id}/categories`, obj.config.MSCATEGORY.serviceid, obj.config.MSCATEGORY.authorization, req.token.company_id).then(msg => {
		resp.send(msg)
	}, err => {
		resp.send(err)
	})
	// marca
})


router.post('/companies/:company_id/users', validateRole.verifyAdmin, validateSameRoleCompany.verifySameRoleCompany('company_id'), (req, resp) => {
	console.log("Esta es la api")
	console.log(`${obj.config.MSCOMPANY.host}/v1/companies/${req.params.company_id}/users`)
	requestHttp.requestMethod(req.method, req.body, `${obj.config.MSCOMPANY.host}/v1/companies/${req.params.company_id}/users`, obj.config.MSCATEGORY.serviceid, obj.config.MSCATEGORY.authorization, req.token.company_id).then(msg => {
		resp.send(msg)
	}, err => {
		resp.send(err)
	})

})

router.post('/wizard', validateRole.verifySuperAdmin, (req, resp) => {
	requestHttp.requestMethod(req.method, req.body, `${obj.config.MSCOMPANY.host}/v1/companies/wizard`, obj.config.MSCATEGORY.serviceid, obj.config.MSCATEGORY.authorization, req.token.company_id || '').then(msg => {
		var body = JSON.parse(msg.body)
		resp.status(msg.code).send({status: msg.code, data:body.data || body})
	}, err => {
		var body = JSON.parse(err.body)
		resp.status(err.code).send({status: err.code, data:body.data || body})
	})

})

router.get('/:uuidCompany/categories', validateRole.verifyNormal, validateSameRoleCompany.verifySameRoleCompany('uuidCompany'), (req, resp) => {
	let queries = `?page=${req.query.page || 0}&per_page=${req.query.per_page || 0}`
	if (req.query !== 'undefined' && req.query.q) {
		queries += `&q=${req.query.q}`
	}
	if (req.query !== 'undefined' && req.query.sort) {
		queries += `&sort=${req.query.sort}`
	}
	requestHttp.requestGet2(`${obj.config.MSCATEGORY.host}/v1/companies/${req.params.uuidCompany}/categories/${queries}`, obj.config.MSCATEGORY.serviceid, obj.config.MSCATEGORY.authorization, req.token.company_id).then(msg => {
        var body = JSON.parse(msg.body)
		resp.status(msg.code).send({status: msg.code, data:body.data, pagination: body.pagination})
	}, err => {
		var body = JSON.parse(err.body)
		resp.status(err.code).send({status: err.code, data:body.data || body})
	})
})



router.get('/:company_id/credits', validateRole.verifyAdmin, validateSameRoleCompany.verifySameRoleCompany('company_id'), (req, resp) => {
	requestHttp.requestGet(`${obj.config.MSCOMPANY.host}/v1/companies/${req.params.company_id}/credits`, obj.config.MSCATEGORY.serviceid, obj.config.MSCATEGORY.authorization, req.token.company_id).then(msg => {
		var body = JSON.parse(msg)
		resp.status(200).send({ data: body.data })
	}, err => {
		resp.send(err)
	})
})


router.get('/', validateRole.verifyNormal, validateSameRoleCompany.verifySameRoleCompany('uuidCompany'), (req, resp) => {
	let queries = `?page=${req.query.page || 0}&per_page=${req.query.per_page || 0}`
	if (req.query !== 'undefined' && req.query.q) {
		queries += `&q=${req.query.q}`
	}
	if (req.query !== 'undefined' && req.query.sort) {
		queries += `&sort=${req.query.sort}`
	}
	requestHttp.requestGet2(`${obj.config.MSUSER.host}/v1/companies/${queries}`, obj.config.MSCOMPANY.serviceid, obj.config.MSCATEGORY.authorization, '').then(msg => {
        var body = JSON.parse(msg.body)
		resp.status(msg.code).send({status: msg.code, data:body.data, pagination: body.pagination})
	}, err => {
		var body = JSON.parse(err.body)
		 resp.status(err.code).send({status: err.code, data:body.data || body})
	})
})

router.patch('/:id/credits', validateRole.verifySuperAdmin, (req, resp) => {
	var respuesta = requestHttp.requestPatch(`${obj.config.MSCOMPANY.host}/v1/companies/${req.params.id}/credits`, req.body, obj.config.MSCOMPANY.serviceid, obj.config.MSCOMPANY.authorization, '').then(res => {
		 resp.status(res.code).send({})
	 }).catch(err => {
		 let body = {}
		 if (typeof err.body !== 'undefined' && err.body !== '') {
			body = JSON.parse(err.body)
		 }
		resp.status(err.code).send({status: err.code, data:body.data || {} })
	 })
})

router.get('/:company_id/sms-groups', (req, resp)=> {
	let queries = `?page=${req.query.page || 0}&per_page=${req.query.per_page || 0}`
	if (req.query !== 'undefined' && req.query.q) {
		queries += `&q=${req.query.q}`
	}
	if (req.query !== 'undefined' && req.query.sort) {
		queries += `&sort=${req.query.sort}`
	}
	requestHttp.requestGet2(`${obj.config.MSCATEGORY.host}/v1/companies/${req.params.company_id}/sms-groups/${queries}`, obj.config.MSCOMPANY.serviceid, obj.config.MSCATEGORY.authorization, '').then(msg => {
        var body = JSON.parse(msg.body)
		resp.status(msg.code).send({status: msg.code, data:body.data, pagination: body.pagination})
	}, err => {
		var body = JSON.parse(err.body)
		 resp.status(err.code).send({status: err.code, data:body.data || body})
	})
})

router.get('/:company_id/sms-groups/:general_sms_id/sms', (req, resp)=> {
	let queries = `?page=${req.query.page || 0}&per_page=${req.query.per_page || 0}`
	if (req.query !== 'undefined' && req.query.q) {
		queries += `&q=${req.query.q}`
	}
	if (req.query !== 'undefined' && req.query.sort) {
		queries += `&sort=${req.query.sort}`
	}
	requestHttp.requestGet2(`${obj.config.MSCATEGORY.host}/v1/companies/${req.params.company_id}/sms-groups/${req.params.general_sms_id}/sms/${queries}`, obj.config.MSCOMPANY.serviceid, obj.config.MSCATEGORY.authorization, '').then(msg => {
        var body = JSON.parse(msg.body)
		resp.status(msg.code).send({status: msg.code, data:body.data, pagination: body.pagination})
	}, err => {
		var body = JSON.parse(err.body)
		 resp.status(err.code).send({status: err.code, data:body.data || body})
	})
})






module.exports = router;