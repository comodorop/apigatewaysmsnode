var express = require('express');
var router = express.Router();
var request = require('request');
var requestify = require('requestify');
const obj = require('config-yml');
var requestHttp = require('../../utilities/requestHttp')
var validateRole = require('../../middlewares/roleValidation')
var validateSameRoleCompany = require('../../middlewares/sameCompany')

router.get('/:uuidContact/categories', validateRole.verifyNormal, (req, resp) => {
	let queries = `?page=${req.query.page || 0}&per_page=${req.query.per_page || 0}`
	if (req.query !== 'undefined' && req.query.q) {
		queries += `&q=${req.query.q}`
	}
	if (req.query !== 'undefined' && req.query.sort) {
		queries += `&sort=${req.query.sort}`
	}
	requestHttp.requestGet2(`${obj.config.MSCATEGORY.host}/v1/contacts/${req.params.uuidContact}/categories/${queries}`, obj.config.MSCATEGORY.serviceid, obj.config.MSCATEGORY.authorization, req.token.company_id).then(msg => {
		console.log(msg.body)
        var body = JSON.parse(msg.body)
		resp.status(msg.code).send({status: msg.code, data:body.data, pagination: body.pagination})
	}).catch(err => {
		var body = JSON.parse(err.body)
		resp.status(err.code).send({status: err.code, data:body.data || body })
	})
})

router.get('/:uuidContact', (req, resp) => {
	let url = `${obj.config.MSCOMPANY.host}/v1/contacts/${req.params.uuidContact}`
	var respuesta = requestHttp.requestGet2(`${obj.config.MSCATEGORY.host}/v1/contacts/${req.params.uuidContact}`, obj.config.MSCATEGORY.serviceid, obj.config.MSCATEGORY.authorization, req.token.company_id).then(msg => {
		var body = JSON.parse(msg.body)
		resp.status(msg.code).send(body.data)
	}).catch(err =>{
		var body = JSON.parse(err.body)
		resp.status(err.code).send({status: err.code, data:body.data || body })
	})
})


router.put('/:uuidContact', (req, resp) => {
	let url = `${obj.config.MSCOMPANY.host}/v1/contacts/${req.params.uuidContact}`
	var respuesta = requestHttp.requestMethod(req.method, req.body, `${obj.config.MSCATEGORY.host}/v1/contacts/${req.params.uuidContact}`, obj.config.MSCATEGORY.serviceid, obj.config.MSCATEGORY.authorization, req.token.company_id).then(msg => {
		// var body = JSON.parse(msg)
		resp.status(msg.code).send({})
	}).catch(err =>{
		console.log(err)
		var body = JSON.parse(err.body)
		resp.status(err.code).send({status: err.code, data:body.data || body })
	})
})




module.exports = router;