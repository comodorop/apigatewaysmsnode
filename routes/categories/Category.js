var express = require('express');
var router = express.Router();
var request = require('request');
var requestify = require('requestify');
const obj = require('config-yml');
var requestHttp = require('../../utilities/requestHttp')
var validateRole = require('../../middlewares/roleValidation')
var id = ''
var iduser = ''
var idcompany = ''
/**git
 * @api {get}http://206.81.14.204:3000/v1/categories/:id mostrar categorias
 * @apiName GetCategory
 * @apiGroup Category
 * @apiDescription Nos permite mostrar informacion de las categorias. No requiere parámetros.
 * @apiParam {string} content-type Del tipo application/json que se envia por la cabecera.
 * @apiParam {string} authorization Token que se envia por la cabecera y es: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJ2YWx1ZSJ9.FG-8UppwHaFp1LgRYQQeS6EDQF7_6-bMFegNucHjmWg
 * @apiSuccessExample Success-Response:
 *{
    "response_id": "80c41c46-67df-413b-97a2-fa9e8041f742",
    "path": "/v1/categories/ba3f8c72-8f1b-49db-95e5-5a6b4f880455",
    "method": "get",
    "request": 1533317841,
    "msg": "category",
    "api-version": "v1",
    "status": "ok",
    "service": "MS-SMS",
    "data": {
        "company_id": "6cf86cef-a3ad-4250-93f6-1b1b68a5d10a",
        "name": "test1",
        "id": "ba3f8c72-8f1b-49db-95e5-5a6b4f880455",
        "description": "esto es un catalogo"
    }
}
    * @apiErrorExample {json} Error-sin_cabecera:
 * En el caso de no tener serviceid en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property serviceid"
}
* En el caso de no tener content-type en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property content-type"
}
* En el caso de no tener authorization en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property of authorization"
}
* @apiErrorExample {json} Error-serviceid:
 * En el caso de tener un serviceid incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have permits"
}
* @apiErrorExample {json} Error-content-type:
 * En el caso de tener un content-type diferente a application/json:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "you dont have the format correct"
}
* @apiErrorExample {json} Error-authorization:
 * En el caso de tener authorization incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have authorization"
}
@apiErrorExample {json} Error-token:
 *El access_token expira en cierto tiempo y si eso pasa sale este mensaje:
 *{
	"name": "HTTPError",
	"statusCode": 401,
	"status": 401,
	"message": "el Token ya expiró"
}
 */
router.get('/:id', validateRole.verifyNormal, (req, resp) => {
	id = req.params.id
	console.log(id)
	var respuesta = requestHttp.requestGet(`${obj.config.MSCATEGORY.host}/v1/categories/${id}`, obj.config.MSCATEGORY.serviceid, obj.config.MSCATEGORY.authorization, req.token.company_id).then(msg => {
		resp.send(msg)
	}, err => {
		resp.send(err)
	})
})



/**
 * @api {put}http://206.81.14.204:3000/v1/categories/:id Actualizar Categoria
 * @apiName PutCategory
 * @apiGroup Category
 * @apiDescription Esta api nos permite actualizar información de categoria.
 * @apiParam {string} content-type Del tipo application/json que se envia por la cabecera.
 * @apiParam {string} authorization Token que se envia por la cabecera y es: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJ2YWx1ZSJ9.FG-8UppwHaFp1LgRYQQeS6EDQF7_6-bMFegNucHjmWg
 * @apiParam {uuid} uuid uuid de la categoria y se envia por el body.
 * @apiParam {string} nombre nombre de la categoria y se envia por el body.
 * @apiParam {string} description Descripcion de la categoria y se envia por el body.
 * @apiSuccessExample Success-Response: 
 *  {
    "response_id": "fc0ac9b8-a86c-47db-9d06-73af3746c016",
    "path": "/v1/categories/486fb107-63d8-4125-b2ef-2191098dac28",
    "method": "put",
    "request": 1533317667,
    "msg": "update category",
    "api-version": "v1",
    "status": "ok",
    "service": "MS-SMS",
    "data": {
        "company_id": "6cf86cef-a3ad-4250-93f6-1b1b68a5d10a",
        "name": "otras",
        "id": "486fb107-63d8-4125-b2ef-2191098dac28",
        "description": ""
    }
}
   * @apiErrorExample {json} Error-sin_cabecera:
 * En el caso de no tener serviceid en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property serviceid"
}
* En el caso de no tener content-type en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property content-type"
}
* En el caso de no tener authorization en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property of authorization"
}
* @apiErrorExample {json} Error-serviceid:
 * En el caso de tener un serviceid incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have permits"
}
* @apiErrorExample {json} Error-content-type:
 * En el caso de tener un content-type diferente a application/json:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "you dont have the format correct"
}
* @apiErrorExample {json} Error-authorization:
 * En el caso de tener authorization incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have authorization"
}
@apiErrorExample {json} Error-token:
 *El access_token expira en cierto tiempo y si eso pasa sale este mensaje:
 *{
	"name": "HTTPError",
	"statusCode": 401,
	"status": 401,
	"message": "el Token ya expiró"
}
 */
//pendientehttp://206.81.14.204:8002/categories/486fb107-63d8-4125-b2ef-2191098dac28
router.put('/:id', validateRole.verifyNormal, (req, resp) => {
    console.log(`${obj.config.MSCATEGORY.host}/v1/categories/${req.params.id}`)
	var respuesta = requestHttp.requestMethod(req.method, req.body, `${obj.config.MSCATEGORY.host}/v1/categories/${req.body.id}`, obj.config.MSCATEGORY.serviceid, obj.config.MSCATEGORY.authorization, req.token.company_id).then(msg => {
		resp.send(msg)
	}, err => {
		resp.send(err)
	})
})

/**
 * @api {delete}http://206.81.204:3000/v1/categories/:id Eliminar una Categoria
 * @apiName DeleteCategory
 * @apiGroup Category
 * @apiDescription Nos permite elimiinar una categoria.
 * @apiParam {string} content-type Del tipo application/json que se envia por la cabecera.
 * @apiParam {string} authorization Token que se envia por la cabecera y es: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJ2YWx1ZSJ9.FG-8UppwHaFp1LgRYQQeS6EDQF7_6-bMFegNucHjmWg
 * @apiParam {uuid} category_id id de categoria que se envia por el url
 * @apiSuccessExample Success-Response:
 * regresa un status 204
   * @apiErrorExample {json} Error-sin_cabecera:
 * En el caso de no tener serviceid en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property serviceid"
}
* En el caso de no tener content-type en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property content-type"
}
* En el caso de no tener authorization en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property of authorization"
}
* @apiErrorExample {json} Error-serviceid:
 * En el caso de tener un serviceid incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have permits"
}
* @apiErrorExample {json} Error-content-type:
 * En el caso de tener un content-type diferente a application/json:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "you dont have the format correct"
}
* @apiErrorExample {json} Error-authorization:
 * En el caso de tener authorization incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have authorization"
}
 */
router.delete('/:id', validateRole.verifyNormal, (req, resp) => {
	id = req.params.id
	var respuesta = requestHttp.requestMethod(req.method, id, `${obj.config.MSCATEGORY.host}/v1/categories/${id}`, obj.config.MSCATEGORY.serviceid, obj.config.MSCATEGORY.authorization, req.token.company_id).then(msg => {
		resp.send(msg)
	}, err => {
		resp.send(err)
	})
})







/**
 * @api {post}http://206.81.14.204:3000/categories/contact/:id agregar contacto
 * @apiName PostContact
 * @apiGroup Contact
 * @apiDescription Nos permite agregar un contacto.
 * @apiParam {string} content-type Del tipo application/json que se envia por la cabecera.
 * @apiParam {string} authorization Token que se envia por la cabecera y es: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJ2YWx1ZSJ9.FG-8UppwHaFp1LgRYQQeS6EDQF7_6-bMFegNucHjmWg
 * @apiParam {uuid} uuidCompany ID de la compañia y se envia por el body.
 * @apiParam {string} first_name Nombre(s) y se envia por el body.
 * @apiParam {string} last_name Apellido y se envia por el body.
 * @apiParam {string} phone Telefono de contacto y se envia por el body.
 * @apiParam {string} email Email de contacto y se envia por el body.
 * @apiParam {string} observations observaciones del contacto y se envia por el body.
 * @apiSuccessExample Success-Response:
 *{
    "response_id": "fc0ac9b8-a86c-47db-9d06-73af3746c016",
    "path": "/v1/categories/486fb107-63d8-4125-b2ef-2191098dac28",
    "method": "put",
    "request": 1533317667,
    "msg": "update category",
    "api-version": "v1",
    "status": "ok",
    "service": "MS-SMS",
    "data": {
        "company_id": "6cf86cef-a3ad-4250-93f6-1b1b68a5d10a",
        "name": "otras",
        "id": "486fb107-63d8-4125-b2ef-2191098dac28",
        "description": ""
    }
}
   * @apiErrorExample {json} Error-sin_cabecera:
 * En el caso de no tener serviceid en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property serviceid"
}
* En el caso de no tener content-type en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property content-type"
}
* En el caso de no tener authorization en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property of authorization"
}
* @apiErrorExample {json} Error-serviceid:
 * En el caso de tener un serviceid incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have permits"
}
* @apiErrorExample {json} Error-content-type:
 * En el caso de tener un content-type diferente a application/json:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "you dont have the format correct"
}
* @apiErrorExample {json} Error-authorization:
 * En el caso de tener authorization incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have authorization"
}
@apiErrorExample {json} Error-token:
 *El access_token expira en cierto tiempo y si eso pasa sale este mensaje:
 *{
	"name": "HTTPError",
	"statusCode": 401,
	"status": 401,
	"message": "el Token ya expiró"
}
 */
router.post('/companies/:idcompany/contacts', validateRole.verifyNormal, (req, resp) => {
	id = req.params.idcompany
	console.log(req)
	var respuesta = requestHttp.requestMethod(req.method, req.body, `${obj.config.MSCATEGORY.host}/v1/companies/${id}/contacts`, obj.config.MSCATEGORY.serviceid, obj.config.MSCATEGORY.authorization, req.token.company_id).then(msg => {
		resp.send(msg)
    }, err => {
        resp.send(err)
    })
})
/**
 * @apihttp://206.81.14.204:8002/categories/486fb107-63d8-4125-b2ef-2191098dac28

/**
 * @api {get}http://206.81.14.204:3000/v1/categories/companies/idcompany/contacts Mostrar Contactos de una compania
 * @apiName GetContact
 * @apiGroup Contact
 * @apiDescription Nos permite mostrar informacion de los contactos de una compañia.
 * @apiParam {string} content-type Del tipo application/json que se envia por la cabecera.
 * @apiParam {string} authorization Token que se envia por la cabecera y es: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJ2YWx1ZSJ9.FG-8UppwHaFp1LgRYQQeS6EDQF7_6-bMFegNucHjmWg
 * @apiParam {uuid} idcompany ID de la compañia que se envia por la url.
 * @apiSuccessExample Success-Response:
 * {
    "response_id": "d89a8624-e074-4440-8012-490d068f3bdb",
    "path": "/v1/companies/6cf86cef-a3ad-4250-93f6-1b1b68a5d10a/contacts",
    "method": "get",
    "request": 1533316269,
    "msg": "contact list",
    "api-version": "v1",
    "status": "ok",
    "service": "MS-SMS",
    "data": [
        {
            "observations": "Nothing to say",
            "company_id": "6cf86cef-a3ad-4250-93f6-1b1b68a5d10a",
            "email": "isc_86@gmail.com",
            "last_name": "Sabido",
            "phone": "9992193269",
            "id": "9ad20734-dfe8-4521-821b-02848b2c4496",
            "first_name": "Ivan Israel"
        },
        {
            "observations": "Nothing to say",
            "company_id": "6cf86cef-a3ad-4250-93f6-1b1b68a5d10a",
            "email": "isc_@gmail.com",
            "last_name": "Sabido",
            "phone": "9992199888",
            "id": "1f6bc260-f810-4b5b-9332-6afc16394804",
            "first_name": "Israel"
        },
        {
            "observations": "Nothing to say",
            "company_id": "6cf86cef-a3ad-4250-93f6-1b1b68a5d10a",
            "email": "elian_2014@gmail.com",
            "last_name": "Sabido",
            "phone": "9992199888",
            "id": "d0a50d3c-aed3-4c46-8397-9584537487b5",
            "first_name": "Elian"
        },
        {
            "observations": "Nothing to say",
            "company_id": "6cf86cef-a3ad-4250-93f6-1b1b68a5d10a",
            "email": "sarah_2012@gmail.com",
            "last_name": "Sabido",
            "phone": "9992199288",
            "id": "154d6eb2-c88b-4fe7-b2ba-edea515fb142",
            "first_name": "Sarah"
        }
    ]
}
    * @apiErrorExample {json} Error-sin_cabecera:
 * En el caso de no tener serviceid en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property serviceid"
}
* En el caso de no tener content-type en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property content-type"
}
* En el caso de no tener authorization en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property of authorization"
}
* @apiErrorExample {json} Error-serviceid:
 * En el caso de tener un serviceid incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have permits"
}
* @apiErrorExample {json} Error-content-type:
 * En el caso de tener un content-type diferente a application/json:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "you dont have the format correct"
}
* @apiErrorExample {json} Error-authorization:
 * En el caso de tener authorization incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have authorization"
}
@apiErrorExample {json} Error-token:
 *El access_token expira en cierto tiempo y si eso pasa sale este mensaje:
 *{
	"name": "HTTPError",
	"statusCode": 401,
	"status": 401,
	"message": "el Token ya expiró"
}
 */
router.get('/companies/:idcompany/contacts', validateRole.verifyNormal, validateRole.verifyNormal, (req, resp) => {
	let queries = `?page=${req.query.page || 0}&per_page=${req.query.per_page || 0}`

	if (req.query !== 'undefined' && req.query.q) {
		queries += `&q=${req.query.q}`
	}
	if (req.query !== 'undefined' && req.query.sort) {
		queries += `&sort=${req.query.sort}`
	}
	if (req.query !== 'undefined' && req.query.nin_category) {
		queries += `&nin_category=${req.query.nin_category}`
	}
	id = req.params.idcompany
	var respuesta = requestHttp.requestGet2(`${obj.config.MSCATEGORY.host}/v1/companies/${id}/contacts/${queries}`, obj.config.MSCATEGORY.serviceid, obj.config.MSCATEGORY.authorization, req.token.company_id).then(msg => {
		console.log(msg.body)
        var body = JSON.parse(msg.body)
        console.log('-------')
        console.log(body)
		resp.status(msg.code).send({status: msg.code, data:body.data, pagination: body.pagination})
	}, err => {
		var body = JSON.parse(err.body)
		resp.status(err.code).send({status: err.code, data:body.data || body })
	})
})
/**
 * @api {get}http://206.81.14.204:3000/v1/categories/contacts/idcontact mostrar un contacto
 * @apiName GetContact
 * @apiGroup Contact
 * @apiDescription Nos permite mostrar informacion del contacto de una compania.
 * @apiParam {string} content-type Del tipo application/json que se envia por la cabecera.
 * @apiParam {string} authorization Token que se envia por la cabecera y es: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJ2YWx1ZSJ9.FG-8UppwHaFp1LgRYQQeS6EDQF7_6-bMFegNucHjmWg
 * @apiParam {uuid} idcontact ID de la compañia que se envia por la url.
 * @apiSuccessExample Success-Response:
 * {
	"status": "200",
	"msg": "Registers",
	"data": [
		{
			"uuid": "1d0bfb49-ecb2-43c2-85be-943920aa9532",
			"first_name": "jordan",
			"last_name": "gongora",
			"phone": "123456789",
			"email": "prueba@gmail.com",
			"observations": "",
			"uuidCompany": "68a59fbc-ede5-4167-bde5-c3b7b985968e"
        }
      ]
    }
    * @apiErrorExample {json} Error-sin_cabecera:
 * En el caso de no tener serviceid en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property serviceid"
}
* En el caso de no tener content-type en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property content-type"
}
* En el caso de no tener authorization en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property of authorization"
}
* @apiErrorExample {json} Error-serviceid:
 * En el caso de tener un serviceid incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have permits"
}
* @apiErrorExample {json} Error-content-type:
 * En el caso de tener un content-type diferente a application/json:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "you dont have the format correct"
}
* @apiErrorExample {json} Error-authorization:
 * En el caso de tener authorization incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have authorization"
}
@apiErrorExample {json} Error-token:
 *El access_token expira en cierto tiempo y si eso pasa sale este mensaje:
 *{
	"name": "HTTPError",
	"statusCode": 401,
	"status": 401,
	"message": "el Token ya expiró"
}
 */
router.get('/contacts/:idcontact', validateRole.verifyNormal, (req, resp) => {
	id = req.params.idcontact
	var respuesta = requestHttp.requestGet(`${obj.config.MSCATEGORY.host}/v1/contacts/${id}`, obj.config.MSCATEGORY.serviceid, obj.config.MSCATEGORY.authorization, req.token.company_id).then(msg => {
		resp.send(msg)
	}, err => {
		resp.send(err)
	})
})

/**
 * @api {put}http://206.81.14.204:3000/v1/categories/contacts/idcontact Actualizar contacto
 * @apiName PutContact
 * @apiGroup Contact
 * @apiDescription Nos permite Actualizar informacion de contacto.
 * @apiParam {string} content-type Del tipo application/json que se envia por la cabecera.
 * @apiParam {string} authorization Token que se envia por la cabecera y es: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJ2YWx1ZSJ9.FG-8UppwHaFp1LgRYQQeS6EDQF7_6-bMFegNucHjmWg
 * @apiParam {uuid} idcontact id del contacto y se envia por la url.
 * @apiParam {string} first_name Nombre(s) y se envia por el body.
 * @apiParam {string} last_name Apellido y se envia por el body.
 * @apiParam {string} phone Telefono de contacto y se envia por el body.
 * @apiParam {string} email Email de contacto y se envia por el body.
 * @apiParam {string} observations observaciones del contacto y se envia por el body.
 * @apiSuccessExample Success-Response:
 * No body returned response
   * @apiErrorExample {json} Error-sin_cabecera:
 * En el caso de no tener serviceid en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property serviceid"
}
* En el caso de no tener content-type en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property content-type"
}
* En el caso de no tener authorization en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property of authorization"
}
* @apiErrorExample {json} Error-serviceid:
 * En el caso de tener un serviceid incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have permits"
}
* @apiErrorExample {json} Error-content-type:
 * En el caso de tener un content-type diferente a application/json:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "you dont have the format correct"
}
* @apiErrorExample {json} Error-authorization:
 * En el caso de tener authorization incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have authorization"
}
 */
router.put('/contacts/:idcontact', validateRole.verifyNormal, (req, resp) => {
	id = req.params.idcontact
	var respuesta = requestHttp.requestMethod(req.method, req.body, `${obj.config.MSCATEGORY.host}/v1/contacts/${id}`, obj.config.MSCATEGORY.serviceid, obj.config.MSCATEGORY.authorization, req.token.company_id).then(msg => {
		resp.send(msg)
	}, err => {
		resp.send(err)
	})
})

/**
 * @api {delete}http://206.81.204:3000/v1/categories/contacts/idcontact Eliminar un contacto
 * @apiName DeleteContact
 * @apiGroup Contact
 * @apiDescription Nos permite Actualizar informacion de contacto.
 * @apiParam {string} content-type Del tipo application/json que se envia por la cabecera.
 * @apiParam {string} authorization Token que se envia por la cabecera y es: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJ2YWx1ZSJ9.FG-8UppwHaFp1LgRYQQeS6EDQF7_6-bMFegNucHjmWg
 * @apiParam {uuid} idcontact id de contacto que se envia por el url
 * * @apiSuccessExample Success-Response:
 * No body returned response
   * @apiErrorExample {json} Error-sin_cabecera:
 * En el caso de no tener serviceid en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property serviceid"
}
* En el caso de no tener content-type en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property content-type"
}
* En el caso de no tener authorization en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property of authorization"
}
* @apiErrorExample {json} Error-serviceid:
 * En el caso de tener un serviceid incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have permits"
}
* @apiErrorExample {json} Error-content-type:
 * En el caso de tener un content-type diferente a application/json:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "you dont have the format correct"
}
* @apiErrorExample {json} Error-authorization:
 * En el caso de tener authorization incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have authorization"
}
 */
router.delete('/contacts/:idcontact', validateRole.verifyNormal, (req, resp) => {
	id = req.params.idcontact
	console.log(id)
	var respuesta = requestHttp.requestMethod(req.method, id, `${obj.config.MSCATEGORY.host}/v1/contacts/${id}`, obj.config.MSCATEGORY.serviceid, obj.config.MSCATEGORY.authorization, req.token.company_id).then(msg => {
		resp.send(msg)
	}, err => {
		resp.send(err)
	})
})

/**
 * @api {get}http://206.81.14.204:3000/categories/idcategory/contacts Mostrar lista de grupos
 * @apiName GetGroup
 * @apiGroup Group
 * @apiDescription Nos permite mostrar informacion del listgroup.
 * @apiParam {string} content-type Del tipo application/json que se envia por la cabecera.
 * @apiParam {string} authorization Token que se envia por la cabecera y es: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJ2YWx1ZSJ9.FG-8UppwHaFp1LgRYQQeS6EDQF7_6-bMFegNucHjmWg
 * @apiParam {uuid} idcategory ID de la compañia que se envia por la url.
 * @apiSuccessExample Success-Response:
 * {
	"response_id": "9d6e45bd-8881-4cc2-8525-60fb7a27e149",
	"path": "/v1/categories/46919a29-a93e-429f-af56-412e1e4d37fb/contacts",
	"method": "get",
	"request": 1533359498.0,
	"msg": "list contacts in category",
	"api-version": "v1",
	"status": "ok",
	"service": "MS-SMS",
	"data": [
		{
			"observations": "Nothing to say",
			"first_name": "Israel",
			"last_name": "Sabido",
			"id": "1f6bc260-f810-4b5b-9332-6afc16394804",
			"email": "isc_@gmail.com",
			"company_id": "6cf86cef-a3ad-4250-93f6-1b1b68a5d10a",
			"phone": "9992199888"
		},
		{
			"observations": "Nothing to say",
			"first_name": "Elian",
			"last_name": "Sabido",
			"id": "d0a50d3c-aed3-4c46-8397-9584537487b5",
			"email": "elian_2014@gmail.com",
			"company_id": "6cf86cef-a3ad-4250-93f6-1b1b68a5d10a",
			"phone": "9992199888"
		},
		{
			"observations": "Nothing to say",
			"first_name": "Sarah",
			"last_name": "Sabido",
			"id": "154d6eb2-c88b-4fe7-b2ba-edea515fb142",
			"email": "sarah_2012@gmail.com",
			"company_id": "6cf86cef-a3ad-4250-93f6-1b1b68a5d10a",
			"phone": "9992199288"
		},
		{
			"observations": "Nothing to say",
			"first_name": "Ivan Israel",
			"last_name": "Sabido",
			"id": "9ad20734-dfe8-4521-821b-02848b2c4496",
			"email": "isc_86@gmail.com",
			"company_id": "6cf86cef-a3ad-4250-93f6-1b1b68a5d10a",
			"phone": "9992193269"
		}
	]
}
    * @apiErrorExample {json} Error-sin_cabecera:
 * En el caso de no tener serviceid en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property serviceid"
}
* En el caso de no tener content-type en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property content-type"
}
* En el caso de no tener authorization en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property of authorization"
}
* @apiErrorExample {json} Error-serviceid:
 * En el caso de tener un serviceid incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have permits"
}
* @apiErrorExample {json} Error-content-type:
 * En el caso de tener un content-type diferente a application/json:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "you dont have the format correct"
}
* @apiErrorExample {json} Error-authorization:
 * En el caso de tener authorization incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have authorization"
}
@apiErrorExample {json} Error-token:
 *El access_token expira en cierto tiempo y si eso pasa sale este mensaje:
 *{
	"name": "HTTPError",
	"statusCode": 401,
	"status": 401,
	"message": "el Token ya expiró"
}
 */
router.get('/:idcategory/contacts', validateRole.verifyNormal, (req, resp) => {
	let queries = `?page=${req.query.page || 0}&per_page=${req.query.per_page || 0}`

	if (req.query !== 'undefined' && req.query.q) {
		queries += `&q=${req.query.q}`
	}
	if (req.query !== 'undefined' && req.query.sort) {
		queries += `&sort=${req.query.sort}`
	}
	id = req.params.idcategory
	var respuesta = requestHttp.requestGet2(`${obj.config.MSCATEGORY.host}/v1/categories/${id}/contacts/${queries}`, obj.config.MSCATEGORY.serviceid, obj.config.MSCATEGORY.authorization, req.token.company_id).then(msg => {
		var body = JSON.parse(msg.body)
		resp.status(msg.code).send({status: msg.code, data:body.data, pagination: body.pagination})
    }, err => {
        var body = JSON.parse(err.body)
		resp.status(err.code).send({status: err.code, data:body.data || body})
    })
})

/**
 * @api {post} http://206.81.14.204:8003/v1/categories/idcategory/contacts Agregar un grupo
 * @apiName PostGroup
 * @apiGroup Group
 * @apiDescription Nos permite agregar un grupo.
 * @apiParam {string} content-type Del tipo application/json que se envia por la cabecera.
 * @apiParam {string} authorization Token que se envia por la cabecera y es: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJ2YWx1ZSJ9.FG-8UppwHaFp1LgRYQQeS6EDQF7_6-bMFegNucHjmWg
 * @apiParam {uuid} contacts ID de contactos que se asignan a una categoria y se envia por el body.
 * @apiParam {uuid} idcategory ID de la categoria y se envia por la url.
 * @apiSuccessExample Success-Response:
 * {
    "response_id": "0dcc7ecd-d0f2-4f31-a860-9fd27d83e4c5",
    "path": "/v1/categories/46919a29-a93e-429f-af56-412e1e4d37fb/contacts",
    "method": "post",
    "request": 1533316512,
    "msg": "create new category",
    "api-version": "v1",
    "status": "ok",
    "service": "MS-SMS",
    "data": {
        "contacts": [
            "154d6eb2-c88b-4fe7-b2ba-edea515fb142",
            "1f6bc260-f810-4b5b-9332-6afc16394804",
            "d0a50d3c-aed3-4c46-8397-9584537487b5",
            "9ad20734-dfe8-4521-821b-02848b2c4496",
            "9ad20734-dfe8-4521-821b-02848b2c4496"
        ]
    }
}
   * @apiErrorExample {json} Error-sin_cabecera:
 * En el caso de no tener serviceid en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property serviceid"
}
* En el caso de no tener content-type en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property content-type"
}
* En el caso de no tener authorization en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property of authorization"
}
* @apiErrorExample {json} Error-serviceid:
 * En el caso de tener un serviceid incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have permits"
}
* @apiErrorExample {json} Error-content-type:
 * En el caso de tener un content-type diferente a application/json:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "you dont have the format correct"
}
* @apiErrorExample {json} Error-authorization:
 * En el caso de tener authorization incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have authorization"
}
@apiErrorExample {json} Error-token:
 *El access_token expira en cierto tiempo y si eso pasa sale este mensaje:
 *{
	"name": "HTTPError",
	"statusCode": 401,
	"status": 401,
	"message": "el Token ya expiró"
}
 */
router.post('/:idcategory/contacts', validateRole.verifyNormal, (req, resp) => {
	id = req.params.idcategory
	var respuesta = requestHttp.requestMethod(req.method, req.body, `${obj.config.MSCATEGORY.host}/v1/categories/${id}/contacts`, obj.config.MSCATEGORY.serviceid, obj.config.MSCATEGORY.authorization, req.token.company_id).then(msg => {
		resp.send(msg)
	}, err => {
		resp.send(err)
	})
})

/**
 * @api {post} http://206.81.14.204:8003/v1/categories/idcategory/contacts Agregar un grupo
 * @apiName DeleteGroup
 * @apiGroup Group
 * @apiDescription Nos permite eliminar un grupo.
 * @apiParam {string} content-type Del tipo application/json que se envia por la cabecera.
 * @apiParam {string} authorization Token que se envia por la cabecera y es: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJ2YWx1ZSJ9.FG-8UppwHaFp1LgRYQQeS6EDQF7_6-bMFegNucHjmWg
 * @apiParam {uuid} idcategory ID de la categoria y se envia por la url.
 * @apiSuccessExample Success-Response:
 * No body returned for response
   * @apiErrorExample {json} Error-sin_cabecera:
 * En el caso de no tener serviceid en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property serviceid"
}
* En el caso de no tener content-type en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property content-type"
}
* En el caso de no tener authorization en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property of authorization"
}
* @apiErrorExample {json} Error-serviceid:
 * En el caso de tener un serviceid incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have permits"
}
* @apiErrorExample {json} Error-content-type:
 * En el caso de tener un content-type diferente a application/json:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "you dont have the format correct"
}
* @apiErrorExample {json} Error-authorization:
 * En el caso de tener authorization incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have authorization"
}
@apiErrorExample {json} Error-token:
 *El access_token expira en cierto tiempo y si eso pasa sale este mensaje:
 *{
	"name": "HTTPError",
	"statusCode": 401,
	"status": 401,
	"message": "el Token ya expiró"
}
 */
router.delete('/:idcategory/contacts', validateRole.verifyNormal, (req, resp) => {
	id = req.params.idcategory
	var respuesta = requestHttp.requestMethod(req.method, req.body, `${obj.config.MSCATEGORY.host}/v1/categories/${id}/contacts`, obj.config.MSCATEGORY.serviceid, obj.config.MSCATEGORY.authorization, req.token.company_id).then(msg => {
		resp.send(msg)
	}, err => {
		resp.send(err)
	})
})

/**
 * @api {put} http://206.81.14.204:8003/categories/listgroup Actualizar grupo
 * @apiName PutListgroup
 * @apiGroup Listgroup
 * @apiDescription Nos permite actualizar un grupo.
 * @apiParam {string} content-type Del tipo application/json que se envia por la cabecera.
 * @apiParam {string} authorization Token que se envia por la cabecera y es: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJ2YWx1ZSJ9.FG-8UppwHaFp1LgRYQQeS6EDQF7_6-bMFegNucHjmWg
 * @apiParam {uuid} uuid ID del grupo y se envia por el body.
 * @apiParam {uuid} uuid_contact ID del contacto y se envia por el body.
 * @apiParam {uuid} uuid_category ID de la categoria y se envia por el body.
 * @apiSuccessExample Success-Response:
 * {
	"status": "200",
	"msg": "Register affect",
	"data": []
   }
   * @apiErrorExample {json} Error-sin_cabecera:
 * En el caso de no tener serviceid en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property serviceid"
}
* En el caso de no tener content-type en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property content-type"
}
* En el caso de no tener authorization en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property of authorization"
}
* @apiErrorExample {json} Error-serviceid:
 * En el caso de tener un serviceid incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have permits"
}
* @apiErrorExample {json} Error-content-type:
 * En el caso de tener un content-type diferente a application/json:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "you dont have the format correct"
}
* @apiErrorExample {json} Error-authorization:
 * En el caso de tener authorization incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have authorization"
}
* @apiErrorExample {json} Error-token:
 *El access_token expira en cierto tiempo y si eso pasa sale este mensaje:
 *{
	"name": "HTTPError",
	"statusCode": 401,
	"status": 401,
	"message": "el Token ya expiró"
}
 */
router.put('/listgroup', validateRole.verifyNormal, (req, resp) => {
	var respuesta = requestHttp.requestMethod(req.method, req.body, `${obj.config.MSCATEGORY.host}/listGroup`, obj.config.MSCATEGORY.serviceid, obj.config.MSCATEGORY.authorization, req.token.company_id).then(msg => {
		resp.send(msg)
	}, err => {
		resp.send(err)
	})
})

/**
 * @api {post} http://206.81.14.204:8003/v1/categories/companies/idcompany/sms Obtener sms
 * @apiGroup Sms
 * @apiDescription Se obtienen los sms registrados o enviados por una compañia.
 * @apiParam {string} content-type Del tipo application/json que se envia por la cabecera.
 * @apiParam {string} authorization Token que se envia por la cabecera y es: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJ2YWx1ZSJ9.FG-8UppwHaFp1LgRYQQeS6EDQF7_6-bMFegNucHjmWg
 * @apiParam {uuid} idcompany ID de la categoria y se envia por la url.
 * @apiSuccessExample Success-Response:
 * {
    "response_id": "e107a984-6d61-4660-bafa-2de98c7d9991",
    "path": "/v1/companies/2f5d32a1-f44e-48b0-aa54-fad722afec81/sms",
    "method": "get",
    "request": 1533318209,
    "msg": "list",
    "api-version": "v1",
    "status": "ok",
    "service": "MS-SMS",
    "data": []
}
   * @apiErrorExample {json} Error-sin_cabecera:
 * En el caso de no tener serviceid en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property serviceid"
}
* En el caso de no tener content-type en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property content-type"
}
* En el caso de no tener authorization en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property of authorization"
}
* @apiErrorExample {json} Error-serviceid:
 * En el caso de tener un serviceid incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have permits"
}
* @apiErrorExample {json} Error-content-type:
 * En el caso de tener un content-type diferente a application/json:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "you dont have the format correct"
}
* @apiErrorExample {json} Error-authorization:
 * En el caso de tener authorization incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have authorization"
}
@apiErrorExample {json} Error-token:
 *El access_token expira en cierto tiempo y si eso pasa sale este mensaje:
 *{
	"name": "HTTPError",
	"statusCode": 401,
	"status": 401,
	"message": "el Token ya expiró"
}
 */
router.get('/companies/:idcompany/sms', validateRole.verifySuperAdmin, (req, resp) => {
	id = req.params.idcompany
	var respuesta = requestHttp.requestGet(`${obj.config.MSCATEGORY.host}/v1/companies/${id}/sms`, obj.config.MSCATEGORY.serviceid, obj.config.MSCATEGORY.authorization, req.token.company_id).then(msg => {
		resp.send(msg)
	}, err => {
		resp.send(err)
	})
})



module.exports = router;
