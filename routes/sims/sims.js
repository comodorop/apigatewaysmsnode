var express = require('express');
var router = express.Router();
var request = require('request');
var requestify = require('requestify');
const obj = require('config-yml');
var requestHttp = require('../../utilities/requestHttp')
var validateRole = require('../../middlewares/roleValidation')
var id = ''
var iduser = ''
var idcompany = ''


/**git
 * @api {get}http://206.81.14.204:3000/v1/sims/ Obtiene los sims con su status
 * @apiName GetCategory
 * @apiGroup Category
 * @apiDescription Nos permite obtener los sims en la bd, con sus datos y status.
 * @apiParam {string} content-type Del tipo application/json que se envia por la cabecera.
 * @apiParam {string} authorization Token que se envia por la cabecera y es: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJ2YWx1ZSJ9.FG-8UppwHaFp1LgRYQQeS6EDQF7_6-bMFegNucHjmWg
 * @apiSuccessExample Success-Response:
 *{
    "response_id": "5da3c5bc-e430-45e7-ac37-b83192def5cb",
    "path": "/v1/sims",
    "method": "get",
    "request": 1536184536,
    "msg": "list",
    "api-version": "v1",
    "status": "ok",
    "service": "MS-SMS",
    "data": [
        {
            "updated_at": "2018-09-05 21:32:31.353525+00:00",
            "fails": 5,
            "last_fail": "2018-09-05 21:32:31.353525+00:00",
            "modem_status_url": "http://empresariandotegoip.ddns.net/default/en_US/send_status.xml",
            "message": "28",
            "modem_id": "72f1e66c-1fc0-430a-97cd-202aa426da7a",
            "modem_url": "http://empresariandotegoip.ddns.net/default/en_US/send.html",
            "modem_model": "GOIP32",
            "status": "blocked",
            "id": "49f2f8e5-0158-42e3-a762-ccf8db2f5491",
            "port": 23
        }
    ]
}
    * @apiErrorExample {json} Error-sin_cabecera:
 * En el caso de no tener serviceid en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property serviceid"
}
* En el caso de no tener content-type en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property content-type"
}
* En el caso de no tener authorization en la cabecera:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have the property of authorization"
}
* @apiErrorExample {json} Error-serviceid:
 * En el caso de tener un serviceid incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have permits"
}
* @apiErrorExample {json} Error-content-type:
 * En el caso de tener un content-type diferente a application/json:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "you dont have the format correct"
}
* @apiErrorExample {json} Error-authorization:
 * En el caso de tener authorization incorrecto:
 * {
	"name": "HTTPError",
	"statusCode": 412,
	"status": 412,
	"message": "You dont have authorization"
}
@apiErrorExample {json} Error-token:
 *El access_token expira en cierto tiempo y si eso pasa sale este mensaje:
 *{
	"name": "HTTPError",
	"statusCode": 401,
	"status": 401,
	"message": "el Token ya expiró"
}
 */
router.get('/', validateRole.verifySuperAdmin, (req, resp) => {
	let queries = `?page=${req.query.page || 0}&per_page=${req.query.per_page || 0}`

	if (req.query !== 'undefined' && req.query.q) {
		queries += `&q=${req.query.q}`
	}
	if (req.query !== 'undefined' && req.query.sort) {
		queries += `&sort=${req.query.sort}`
	}
	console.log(`${obj.config.MSCATEGORY.host}/v1/sims/${queries}`)
	var respuesta = requestHttp.requestGet2(`${obj.config.MSCATEGORY.host}/v1/sims/${queries}`, obj.config.MSCATEGORY.serviceid, obj.config.MSCATEGORY.authorization, '').then(res => {
	   console.log(res.body)
        var body = JSON.parse(res.body)

		resp.status(res.code).send({status: res.code, data:body.data, pagination: body.pagination})
	}, err => {
		var body = JSON.parse(err.body)
		resp.status(err.code).send({status: err.code, data:body.data || body})
	})
})

router.patch('/:id', validateRole.verifySuperAdmin, (req, resp) => {
	var respuesta = requestHttp.requestPatch(`${obj.config.MSCATEGORY.host}/v1/sims/${req.params.id}`, req.body, obj.config.MSCATEGORY.serviceid, obj.config.MSCATEGORY.authorization, '').then(res => {
		console.log(res.body)
		 var body = JSON.parse(res.body)
		 
		 resp.status(res.code).send({status: res.code, data:body.data, pagination: body.pagination})
	 }, err => {
		 var body = JSON.parse(err.body)
		 resp.status(err.code).send({status: err.code, data:body.data })
	 })
})
module.exports = router