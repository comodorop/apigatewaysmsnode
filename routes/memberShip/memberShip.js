var express = require('express');
var HTTPError = require('node-http-error');
var router = express.Router();
var request = require('request');
var requestify = require('requestify');
const obj = require('config-yml');
var requestHttp = require('../../utilities/requestHttp')
var validateRole = require('../../middlewares/roleValidation')
var validateSameRoleMembership = require('../../middlewares/sameMembership')


router.patch('/:membership_id/credits', validateRole.verifyAdmin, (req, resp) => {
	requestHttp.requestMethod(req.method, req.body, `${obj.config.MSCOMPANY.host}/v1/memberships/${req.params.membership_id}/credits`, obj.config.MSCOMPANY.serviceid, obj.config.MSCOMPANY.authorization, req.token.company_id).then(res => {
		console.log(res.body)
		var body = JSON.parse(res.body)
		resp.status(res.code).send({ status: res.code, data: body.data })
	}, err => {
		var body = JSON.parse(err.body)
		resp.status(err.code).send({ status: err.code, data: body.data || body })
	})
})

router.delete('/:membership_id/credits', validateRole.verifyAdmin, (req, resp) => {
	requestHttp.requestMethod(req.method, req.body, `${obj.config.MSCOMPANY.host}/v1/memberships/${req.params.membership_id}/credits`, obj.config.MSCOMPANY.serviceid, obj.config.MSCOMPANY.authorization, req.token.company_id).then(res => {
		var body = JSON.parse(res.body)
		resp.status(res.code).send({ status: res.code, data: body.data })
	}, err => {
		var body = JSON.parse(err.body)
		resp.status(err.code).send({ status: err.code, data: body.data })
	})
})


router.get('/:membership_id/credits', validateRole.verifyNormal, validateSameRoleMembership.verifySameRoleMembership('membership_id'), (req, resp) => {
	console.log(`${obj.config.MSCOMPANY.host}/v1/memberships/${req.params.membership_id}/credit`)
	requestHttp.requestGet2(`${obj.config.MSCOMPANY.host}/v1/memberships/${req.params.membership_id}/credits`, obj.config.MSCOMPANY.serviceid, obj.config.MSCOMPANY.authorization, req.token.company_id).then(res => {
		var body = JSON.parse(res.body)
		resp.status(res.code).send({ status: res.code, data: body.data })
	}, err => {
		var body = JSON.parse(err.body)
		resp.status(err.code).send({ status: err.code, data: body.data })
	})
})

router.get('/:membership_id/notifications', (req, resp) => {
	let queries = `?page=${req.query.page || 0}&per_page=${req.query.per_page || 0}`
	if (req.query !== 'undefined' && req.query.q) {
		queries += `&q=${req.query.q}`
	}
	if (req.query !== 'undefined' && req.query.sort) {
		queries += `&sort=${req.query.sort}`
	}
	requestHttp.requestGet2(`${obj.config.SMS.host}/v1/memberships/${req.params.membership_id}/notifications/${queries}`, obj.config.MSCOMPANY.serviceid, obj.config.MSCOMPANY.authorization, req.token.company_id).then(res => {
		var body = JSON.parse(res.body)
		resp.status(res.code).send({ status: res.code, data: body.data })
	}, err => {
		var body = JSON.parse(err.body)
		resp.status(err.code).send({ status: err.code, data: body.data })
	})
})

module.exports = router;