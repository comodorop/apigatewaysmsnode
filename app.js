const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cors = require('cors');
const auth = require('./routes/oauth/auth')
const helmet = require('helmet')
const authentication = require('./middlewares/authentication')
const validateCompany = require('./middlewares/middlewareCompany')
const validateAuth= require('./middlewares/middlewareAuth')
const validateUser= require('./middlewares/middlewareUser')
const validateCategory = require('./middlewares/middlewareCategory')
const validateAccessToken = require('./middlewares/middlewareAccessToken')
const validateSender = require('./middlewares/middlewareSender')


const app = express();
app.use(cors())
// secure express app
app.use(helmet({
    dnsPrefetchControl: false,
    ieNoOpen: false
}))
app.use(authentication.authentification)
app.use(validateAccessToken.verifyAccessToken)

const objAuth = require('./routes/oauth/auth')
const objAuthSuperadmin = require('./routes/oauth/superadmin')
const objUser = require('./routes/users/user')
const objComp = require('./routes/companies/company')
const objCat = require('./routes/categories/Category')
const objSender = require('./routes/sender/sender')
const objMemberShip = require('./routes/memberShip/memberShip')
const objContacts = require('./routes/contacts/contacts')
const objSims= require('./routes/sims/sims')
const objSuperadmins= require('./routes/superadmins')

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))
app.use('/auth', objAuth)
app.use('/superadmins', objAuthSuperadmin)
app.use('/v1/users',  objUser)
app.use('/v1/companies', objComp)
app.use('/v1/categories', objCat)
app.use('/sender', objSender)
app.use('/v1/memberShip', objMemberShip)
app.use('/v1/contacts', objContacts)
app.use('/v1/sims', objSims)
app.use('/v1/superadmins', objSuperadmins)

app.use(function(req, res, next) {
    return res.status(404).send({ message: 'Ruta '+req.url+' No encontrada' })
})


module.exports = app;
