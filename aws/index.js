var fs = require('fs');
var AWS = require('aws-sdk');
var config = require('../utilities/config')
var messages = require('../utilities/messages')

var s3 = new AWS.S3({
    accessKeyId: config.aws.access_key_ID,
    secretAccessKey: config.aws.secret_access_key
})

function upload(file) {
    console.log(file)
    var promise = new Promise((resolve, reject) => {
        if (!file.name) {
            console.log("no encontro el archivo")
            reject(messages.createMessage(400, "PutObjectError"))
        } else {
            fs.readFile(file.dir, (err, data) => {
                if (err) {
                    reject(messages.createMessage(409, "ReadFileError"))
                }
                var paramsPutObject = {
                    Bucket: 'sms-irecom',
                    Key: file.name,
                    Body: data
                }
                s3.putObject(paramsPutObject, (err, data) => {
                    if (err) {
                        console.log(err)
                        reject(messages.createMessage(400, "PutObjectError"))
                    } else {
                        resolve(messages.createMessage(201, "SuccessfulUpload", data))
                        console.log(data)
                    }
                })
            })
        }
    })
    return promise
}

module.exports = {
    upload
}