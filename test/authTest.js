var chai = require('chai');
var chaiHttp = require('chai-http');
const expect = require('chai').expect;
const obj = require('config-yml');
const config = require('../utilities/config');
chai.use(chaiHttp);

var url = `${config.URL_PROD}/auth`
var agent = chai.request.agent(url)
var datos = ''

describe('Login', () => {
    it('Debe iniciar Sesion', (done) => {
        agent
            .post('/token')
            .set({
                "Content-Type": 'application/json',
                serviceid: obj.config.MSAUTH.serviceid
            })
            .send({
                grant_type: "password",
                email: "jordan.glizama@gmail.com",
                password: "123456789"
            })
            .end((err, res) => {
                //console.log(res.text)
                expect(res).to.have.status(200)
                done();
                return datos = JSON.parse(res.text)
            });
    });
    it('Debe refrescar el token', (done) => {
        agent
            .post('/refresh')
            .set({
                "Content-Type": 'application/json',
                serviceid: obj.config.MSAUTH.serviceid
            })
            .send({
                grant_type: 'refresh_token',
                refresh_token: datos.data.refresh_token
            })
            .end((err, res) => {

                var datosApiRefresh = JSON.parse(res.text)
                //console.log(datosApiRefresh)
                expect(res).to.have.status(200)
                done()
            })
    })
});


module.exports = {

}
