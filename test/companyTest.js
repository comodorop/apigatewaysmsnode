var chai = require('chai');
var chaiHttp = require('chai-http');
const expect = require('chai').expect;
const obj = require('config-yml');
const config = require('../utilities/config');
chai.use(chaiHttp);

var url = `${config.URL_PROD}/auth`
var agent = chai.request.agent(url)
var response = ''
var id = ''
describe('Compañias', () => {
    it('Debe iniciar Sesion', (done) => {

        agent
            .post('/token')
            .set({
                "Content-Type": 'application/json',
                serviceid: obj.config.MSAUTH.serviceid
            })
            .send({
                grant_type: "password",
                email: "jordan.glizama@gmail.com",
                password: "123456789"
            })
            .end((err, res) => {
                //console.log(res.text)
                expect(res).to.have.status(200)
                done();
                url = `${config.URL_PROD}/company`
                agent = chai.request.agent(url)
                return datos = JSON.parse(res.text)
            })
    })
    it('debe crear compañia', (done) => {
        agent
            .post('/createCompany')
            .set({
                authorization: obj.config.MSCOMPANY.authorization,
                "Content-Type": 'application/json',
                serviceid: obj.config.MSCOMPANY.serviceid,
                access_token: datos.data.access_token
            })
            .send({
                name: "Ceviches",
                rfc: "golsjas",
                email_contact: "jordan.glizama@gmail.com",
                phone: " 9992673372",
                credit: 1000
            })
            .end((err, res) => {
                response = JSON.parse(res.text)
                console.log(response)
                expect(response).to.have.status(201)
                done()
                return id = response.data.id

            })
    })
    it('Debe actualizar informacion de compañia', (done) => {
        agent
            .put('/updateCompany')
            .set({
                authorization: obj.config.MSCOMPANY.authorization,
                "Content-Type": 'application/json',
                serviceid: obj.config.MSCOMPANY.serviceid,
                access_token: datos.data.access_token
            })
            .send({
                company_id: "03a02a6a-0169-4f45-8be6-c416536eeb50",
                data: {
                    name: "ceviches_v2",
                    rfc: "aqweq",
                    email_contact: "jordan.glizama@gmail.com",
                    phone: "9999999",
                    credit: 1000
                }
            })
            .end((err, res) => {
                if (err) {
                    throw err
                } else {
                    response = JSON.parse(res.text)
                    //console.log(response)
                    expect(response).to.have.status('ok')
                    done()
                }
            })
    })
    it('Debe mostrar información de la compañia', (done) => {
        agent
            .get('/infoCompany')
            .set({
                authorization: obj.config.MSCOMPANY.authorization,
                "Content-Type": 'application/json',
                serviceid: obj.config.MSCOMPANY.serviceid,
                access_token: datos.data.access_token
            })
            .send({ id: id })
            .end((err, res) => {
                //response = JSON.parse(res.text)
                expect(response).to.have.status('ok')
                done()
            })
    })
    it('Debe eliminar una compaia', (done) => {
        agent
            .delete('/deleteCompany')
            .set({
                authorization: obj.config.MSCOMPANY.authorization,
                "Content-Type": 'application/json',
                serviceid: obj.config.MSCOMPANY.serviceid,
                access_token: datos.data.access_token
            })
            .send({ id: id })
            .end((err, res) => {
                if (res.text) {
                    response = JSON.parse(res.text)
                }
                if (response.code == 500) {
                    expect(response.code).to.have.equal(500)
                    done()
                    console.log('       ya no existe el registro')
                }
                else {
                    expect(res).to.have.status(200)
                    done()
                }
            })

    })
})