var chai = require('chai');
var chaiHttp = require('chai-http');
const expect = require('chai').expect;
const obj = require('config-yml');
const config = require('../utilities/config');
chai.use(chaiHttp);

var url = `${config.URL_PROD}/auth`
var agent = chai.request.agent(url)

describe('Usuarios', () => {
    it('Debe iniciar Sesion', (done) => {
        agent
            .post('/token')
            .set({
                "Content-Type": 'application/json',
                serviceid: obj.config.MSAUTH.serviceid
            })
            .send({
                grant_type: "password",
                email: "jordan.glizama@gmail.com",
                password: "123456789"
            })
            .end((err, res) => {
                //console.log(res.text)
                expect(res).to.have.status(200)
                done();
                url = `${config.URL_PROD}/user`
                agent = chai.request.agent(url)
                return datos = JSON.parse(res.text)
            });
    });
    it('Debe mostrar roles', (done) => {
        agent
            .get('/roles')
            .set({
                authorization: obj.config.MSUSER.authorization,
                "Content-Type": 'application/json',
                serviceid: obj.config.MSUSER.serviceid,
                access_token: datos.data.access_token
            })
            .end((err, res) => {
                //console.log(res.text)
                expect(res).to.have.status(200)
                done()
            })
    })
    it('Debe actualizar el rol', (done) => {
        agent
            .put('/roles')
            .set({
                authorization: obj.config.MSUSER.authorization,
                "Content-Type": 'application/json',
                serviceid: obj.config.MSUSER.serviceid,
                access_token: datos.data.access_token
            })
            .send({
                role_id: '27166b61-d386-418d-ae23-5050455ab5e8',
                membership_id: '2f5d32a1-f44e-48b0-aa54-fad722afec81'
            })
            .end((err, res) => {
                expect(res).to.have.status(200)
                done()
            })
    })
    it('Debe crear un usuario', (done) => {
        agent
            .post('/createUser')
            .set({
                authorization: obj.config.MSUSER.authorization,
                "Content-Type": 'application/json',
                serviceid: obj.config.MSUSER.serviceid,
                access_token: datos.data.access_token
            })
            .send({
                "name": "jordan",
                "lastname": "gongora",
                "password": "123456789",
                "email": "esteban@hotmail.com",
                "cellphone": "9999999",
                "role_id": "7712e1eb-bf99-4ab1-8ca2-4d5918139d63",
                "company_id": "6cf86cef-a3ad-4250-93f6-1b1b68a5d10a"
            })
            .end((err, res) => {
                //console.log(res.text)
                var code = JSON.parse(res.text)
                if (code.status) {
                    expect(code).to.have.status('ok')
                    done()
                } else {
                    expect(code.code).to.equal(400)
                    done()
                    console.log('       el email ya existe')
                }

            })
    })
    it('Debe actualizar un usuario', (done) => {
        agent
            .put('/updateUser')
            .set({
                authorization: obj.config.MSUSER.authorization,
                "Content-Type": 'application/json',
                serviceid: obj.config.MSUSER.serviceid,
                access_token: datos.data.access_token
            })
            .send({

                "company_id": "6cf86cef-a3ad-4250-93f6-1b1b68a5d10a",
                "id": "df6f4fd1-f648-4684-87c7-7b9de9b0d700",
                "data": {
                    "name": "jordan",
                    "lastname": "gongora",
                    "password": "123456789",
                    "email": "jordan.glizama@gmail.com",
                    "cellphone": "9992673372",
                    "role_id": "27166b61-d386-418d-ae23-5050455ab5e8"
                }

            })
            .end((err, res) => {
                code = JSON.parse(res.text)
                expect(code).to.have.status('ok')
                done()
                //console.log(       code.status)
            })
    })
    it('Debe actualizar roles', (done) => {
        agent
            .put('/roles')
            .set({
                authorization: obj.config.MSUSER.authorization,
                "Content-Type": 'application/json',
                serviceid: obj.config.MSUSER.serviceid,
                access_token: datos.data.access_token
            })
            .send({
                role_id: "27166b61-d386-418d-ae23-5050455ab5e8",
                membership_id: "66b29d93-66bf-4eab-879a-d7f757971ae7"
            })
            .end((err, res) => {
                if (err) {
                    throw err
                }
                else {
                    expect(res).to.have.status(200)
                    done()
                }
            })
    })
    it('Debe cambiar Contraseña', (done) => {
        agent
            .patch('/changePass')
            .set({
                authorization: obj.config.MSUSER.authorization,
                "Content-Type": 'application/json',
                serviceid: obj.config.MSUSER.serviceid,
                access_token: datos.data.access_token
            })
            .send({
                company_id: "6cf86cef-a3ad-4250-93f6-1b1b68a5d10a",
                id: "df6f4fd1-f648-4684-87c7-7b9de9b0d700",
                data: {
                    password: "12345678"
                }
            })
            .end((err, res) => {
                if (err) {
                    throw err
                } else {
                    expect(res).to.have.status(200)
                    done()
                }
            })
    })
    it('Debe mostrar información del usuario', (done) => {
        agent
            .get('/infoUser')
            .set({
                authorization: obj.config.MSUSER.authorization,
                "Content-Type": 'application/json',
                serviceid: obj.config.MSUSER.serviceid,
                access_token: datos.data.access_token
            })
            .send({
                company_id: "6cf86cef-a3ad-4250-93f6-1b1b68a5d10a",
                id: "df6f4fd1-f648-4684-87c7-7b9de9b0d700",
            })
            .end((err, res) => {
                if (err) {
                    throw err
                } else {
                    code = JSON.parse(res.text)
                    expect(code).to.have.status('ok')
                    done()
                }
            })
    })
    it('Debe mostrar las membresias de una compañia', (done) => {
        agent
            .get('/memberships')
            .set({
                authorization: obj.config.MSUSER.authorization,
                "Content-Type": 'application/json',
                serviceid: obj.config.MSUSER.serviceid,
                access_token: datos.data.access_token
            })
            .send({
                company_id: "6cf86cef-a3ad-4250-93f6-1b1b68a5d10a"
            })
            .end((err, res) => {
                if (err) {
                    throw err
                } else {
                    code = JSON.parse(res.text)
                    //console.log(code)
                    expect(code).to.have.status(200)
                    done()

                }
            })
    })
});
