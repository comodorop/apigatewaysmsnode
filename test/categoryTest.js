var chai = require('chai');
var chaiHttp = require('chai-http');
const expect = require('chai').expect;
const obj = require('config-yml');
const config = require('../utilities/config');
chai.use(chaiHttp);

var url = `${config.URL_DEV}/auth`
var agent = chai.request.agent(url)
var response = ''
var id = ''
describe('Categorias', () => {
    it('Debe iniciar Sesion', (done) => {

        agent
            .post('/token')
            .set({
                "Content-Type": 'application/json',
                serviceid: obj.config.MSAUTH.serviceid
            })
            .send({
                grant_type: "password",
                email: "jordan.glizama@gmail.com",
                password: "123456789"
            })
            .end((err, res) => {
                //console.log(res.text)
                expect(res).to.have.status(200)
                done();
                url = `${config.URL_DEV}/v1/categories`
                agent = chai.request.agent(url)
                return datos = JSON.parse(res.text)
            })
    })
    it('Debe registrar una categoria', (done) => {
        agent
            .post('/category')
            .set({
                authorization: obj.config.MSCATEGORY.authorization,
                "Content-Type": 'application/json',
                serviceid: obj.config.MSCATEGORY.serviceid,
                access_token: datos.data.access_token
            })
            .send({
                nombre: "uniTest",
                description: "prueba unitaria"
            })
            .end((err, res) => {
                if (err) {
                    throw err
                } else {
                    //console.log(res.text)
                    response = JSON.parse(res.text)
                    console.log(response)
                    expect(response).to.have.status(200)
                    done()
                }
            })
    })
    it('Debe regresar todas las categorias', (done) => {
        agent
            .get('/category')
            .set({
                authorization: obj.config.MSCATEGORY.authorization,
                "Content-Type": 'application/json',
                serviceid: obj.config.MSCATEGORY.serviceid,
                access_token: datos.data.access_token
            })
            .end((err, res) => {
                if (err) {
                    throw err
                } else {
                    expect(res).to.have.status(200)
                    done()
                }
            })
    })
    it('Debe actualizar una categoria', (done) => {
        agent
            .put('/category')
            .set({
                authorization: obj.config.MSCATEGORY.authorization,
                "Content-Type": 'application/json',
                serviceid: obj.config.MSCATEGORY.serviceid,
                access_token: datos.data.access_token
            })
            .send({
                uuid: "d472496a-9084-43f3-8a6f-e7bd5fdf5177",
                nombre: "prueba unitaria editada",
                description: "editada"
            })
            .end((err, res) => {
                if (err) {
                    throw err
                } else {
                    response = JSON.parse(res.text)
                    expect(response).to.have.status(200)
                    done()
                }
            })
    })
    it('Debe registrar un contacto', (done) => {
        agent
            .post('/companies/68a59fbc-ede5-4167-bde5-c3b7b985968e/contacts')
            .set({
                authorization: obj.config.MSCATEGORY.authorization,
                "Content-Type": 'application/json',
                serviceid: obj.config.MSCATEGORY.serviceid,
                access_token: datos.data.access_token
            })
            .send({
                "idcompany": "68a59fbc-ede5-4167-bde5-c3b7b985968e",
                "first_name": "pancho",
                "last_name": "lopez",
                "phone": "9992673372",
                "email": "prueba123@gmail.com",
                "observations": "asasdasddasd"
            })
            .end((err, res) => {
                if (err) {
                    throw err
                } else {
                    //console.log(res.text)
                    response = JSON.parse(res.text)
                    expect(response).to.have.status('ok')
                    done()
                }
            })
    })
    it('Debe regresar todos los contactos de una compañia', (done) => {
        agent
            .get('/companies/6cf86cef-a3ad-4250-93f6-1b1b68a5d10a/contacts')
            .set({
                authorization: obj.config.MSCATEGORY.authorization,
                "Content-Type": 'application/json',
                serviceid: obj.config.MSCATEGORY.serviceid,
                access_token: datos.data.access_token
            })
            .end((err, res) => {
                if (err) {
                    throw err
                } else {
                    response = JSON.parse(res.text)
                    expect(response).to.have.status('ok')
                    done()
                }
            })
    })
    it('Debe regresar un contacto de una compañia', (done) => {
        agent
            .get('/contacts/9ad20734-dfe8-4521-821b-02848b2c4496')
            .set({
                authorization: obj.config.MSCATEGORY.authorization,
                "Content-Type": 'application/json',
                serviceid: obj.config.MSCATEGORY.serviceid,
                access_token: datos.data.access_token
            })
            .end((err, res) => {
                if (err) {
                    throw err
                } else {
                    response = JSON.parse(res.text)
                    expect(response).to.have.status('ok')
                    done()
                }
            })
    })
    it('Debe actualizar un contacto', (done) => {
        agent
            .put('/contacts/9ad20734-dfe8-4521-821b-02848b2c4496')
            .set({
                authorization: obj.config.MSCATEGORY.authorization,
                "Content-Type": 'application/json',
                serviceid: obj.config.MSCATEGORY.serviceid,
                access_token: datos.data.access_token
            })
            .send({
                "first_name": "Ivan Israel",
                "last_name": "Sabido",
                "phone": "9992193269",
                "email": "isc_86@gmail.com",
                "observations": "Nothing to say"
            })
            .end((err, res) => {
                if (err) {
                    throw err
                } else {
                    //console.log(res.text)
                    expect(res).to.have.status(200)
                    done()
                }
            })
    })
    it('Debe eliminar un contacto', (done) => {
        agent
            .delete('/contacts/e0a27e3f-3970-4258-8bd0-330a299b8d55')
            .set({
                authorization: obj.config.MSCATEGORY.authorization,
                "Content-Type": 'application/json',
                serviceid: obj.config.MSCATEGORY.serviceid,
                access_token: datos.data.access_token
            })
            .end((err, res) => {
                if (err) {
                    throw err
                } else {
                    //console.log(res.text)
                    expect(res).to.have.status(200)
                    done()
                }
            })
    })
    it('Debe registrar un grupo de contactos con una categoria', (done) => {
        agent
            .post('/46919a29-a93e-429f-af56-412e1e4d37fb/contacts')
            .set({
                authorization: obj.config.MSCATEGORY.authorization,
                "Content-Type": 'application/json',
                serviceid: obj.config.MSCATEGORY.serviceid,
                access_token: datos.data.access_token
            })
            .send({
                contacts: [
                    "154d6eb2-c88b-4fe7-b2ba-edea515fb142",
                    "1f6bc260-f810-4b5b-9332-6afc16394804",
                    "d0a50d3c-aed3-4c46-8397-9584537487b5",
                    "9ad20734-dfe8-4521-821b-02848b2c4496",
                    "9ad20734-dfe8-4521-821b-02848b2c4496"
                ]
            })
            .end((err, res) => {
                if (err) {
                    throw err
                } else {
                    //console.log(res.text)
                    response = JSON.parse(res.text)
                    expect(response).to.have.status('ok')
                    done()
                }
            })
    })
    it('Debe regresar contactos con una categoria', (done) => {
        agent
            .get('/46919a29-a93e-429f-af56-412e1e4d37fb/contacts')
            .set({
                authorization: obj.config.MSCATEGORY.authorization,
                "Content-Type": 'application/json',
                serviceid: obj.config.MSCATEGORY.serviceid,
                access_token: datos.data.access_token
            })
            .end((err, res) => {
                if (err) {
                    throw err
                } else {
                    response = JSON.parse(res.text)
                    expect(response).to.have.status('ok')
                    done()
                }
            })
    })
    it('Debe eliminar un grupo', (done) => {
        agent
            .delete('/46919a29-a93e-429f-af56-412e1e4d37fb/contacts')
            .set({
                authorization: obj.config.MSCATEGORY.authorization,
                "Content-Type": 'application/json',
                serviceid: obj.config.MSCATEGORY.serviceid,
                access_token: datos.data.access_token
            })
            .end((err, res) => {
                if (err) {
                    throw err
                } else {
                    //console.log(res.text)
                    //response = JSON.parse(res.text)
                    expect(res).to.have.status(200)
                    done()
                }
            })
    })
    it('Debe obtener sms', (done) => {
        agent
            .get('/companies/2f5d32a1-f44e-48b0-aa54-fad722afec81/sms')
            .set({
                authorization: obj.config.MSCATEGORY.authorization,
                "Content-Type": 'application/json',
                serviceid: obj.config.MSCATEGORY.serviceid,
                access_token: datos.data.access_token
            })
            .end((err, res) => {
                if (err) {
                    throw err
                } else {
                    //console.log(res.text)
                    response = JSON.parse(res.text)
                    expect(response).to.have.status('ok')
                    done()
                }
            })
    })
    /* it('Debe actualizar una lista de grupos', (done) => {
         agent
             .post('/listgroup')
             .set({
                 authorization: obj.config.MSCATEGORY.authorization,
                 "Content-Type": 'application/json',
                 serviceid: obj.config.MSCATEGORY.serviceid,
                 access_token: datos.data.access_token
             })
             .send({
                 "uuid": "1d0bfb49-ecb2-43c2-85be-943920aa9532",
                 uuid_contact: "218d3429-230f-4a56-b60c-4da539dfec30",
                 uuid_category: "41fef325-4f55-416f-a79f-256fad007c73"
             })
             .end((err, res) => {
                 if (err) {
                     throw err
                 } else {
                     //console.log(res.text)
                     response = JSON.parse(res.text)
                     expect(response).to.have.status(200)
                     done()
                 }
             })
     })*/
})