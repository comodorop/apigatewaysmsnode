

// Create a new connection manager


// Ask the connection manager for a ChannelWrapper.  Specify a setup function to run every time we reconnect
// to the broker.


// channelWrapper.sendToQueue('rxQueueName', { hello: 'world' })
//     .then(function () {
//         return console.log("Message was sent!  Hooray!");
//     }).catch(function (err) {
//         return console.log("Message was rejected...  Boo!");
//     });

// const amqp = require('..');
// const { wait } = require('../lib/helpers');
var amqp = require('amqp-connection-manager');


const QUEUE_NAME = 'testProbando'
const EXCHANGE_NAME = 'test';

// Create a connetion manager
const connection = amqp.connect(['amqp://localhost'], {json: true});
connection.on('connect', () => console.log('Connected!'));
connection.on('disconnect', params => console.log('Disconnected.', params.err.stack));

// Create a channel wrapper
const channelWrapper = connection.createChannel({
    json: true,
    setup: function (channel) {
        return Promise.all([
          channel.assertExchange(EXCHANGE_NAME,'topic'),
          channel.prefetch(1),
          channel.assertQueue(QUEUE_NAME, {durable: true}),
          channel.bindQueue(QUEUE_NAME, EXCHANGE_NAME, "create"),
          // channel.bindQueue("file_queues", "sms_events", '#'),
        //   channel.bindQueue("testProbando", "sms_envents", '#'),
          
          // channel.consume("file_queues", onMessage)
      ]);
    }
});

// Send messages until someone hits CTRL-C or something goes wrong...
function sendMessage() {
    channelWrapper.publish(EXCHANGE_NAME, "create", {time: Date.now()}, { contentType: 'application/json', persistent: true })
    .then(function() {
        console.log("Message sent");
    }).catch(err=>{
        console.log("marco un error")
    })
    .then()
    .catch(err => {
        console.log("Message was rejected:", err.stack);
        channelWrapper.close();
        connection.close();
    });
};

console.log("Sending messages...");
sendMessage();