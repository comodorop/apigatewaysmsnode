var jwt = require('jsonwebtoken')
var config = require('../../utilities/config')
var msg = require('../../utilities/messages')
var moment = require('moment')
var fs = require("fs")
var mensaje = ''
function decodeToken(token) {
    var promise = new Promise((resolve, reject) => {
        try {
            pubkey = fs.readFileSync('utilities/pubkey.pem')
            let decode = jwt.verify(token, pubkey, { audience: 'smartsend' })
            resolve(decode);
        }
        catch (err) {
            mensaje = msg.createMessage(500, "El token es invalido");
            console.log('El token expiró')
            reject(mensaje);
        }
    })
    return promise
}

function validateToken(token) {
    var promise = new Promise((resolve, reject) => {
        decodeToken(token)
            .then(decode => {
                if (decode.exp <= moment().unix()) {
                    reject(mensaje);
                }
                else {
                    resolve(decode)
                }
            })
            .catch(err => {
                reject(mensaje)
            })
    })
    return promise
}
module.exports = {
    decodeToken,
    validateToken
}